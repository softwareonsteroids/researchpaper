/**
 *  Copyright 2017 Okusanya Oluwadamilola
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
 
/**
 *  Producer class
 *  Produces messages to be replicated by the raft node
 *  For every message, it receives an acknowledgement message
 */
 
import java.io.PrintWriter;
import java.io.StringWriter; 
import java.io.BufferedReader;
import java.io.BufferedWriter; 
import java.io.IOException;
import org.zeromq.ZMQ;
import java.util.UUID;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Map;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Collections;
import java.util.ArrayList;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
  
public class Consumer extends Thread{
    
    private final static String HOSTNAME = System.getenv("HOSTNAME");
    private ArrayList<NodeDetails> clusterRaftNodes;
    public HashMap<String, ZMQ.Socket> negotationSockets;
    public NodeDetails details;
    private int nextNodeToTry;   
    private SystemState.ClusterState currentClusterState;   // this is mainly for shutdown and startup
    private static String PUBLICNODENAME = "Cluster-" + HOSTNAME;
    private static String APPLOGFILENAME;
    public String leaderId = "";
    
    /** 
     * Sockets that belongs to the thread/node 
     *
     * - A socket that connects to management server. 
     *      - For sending/receiving messages to the management server.
     *      - Socket is async.  
     * - A socket that connects to management server.
     *      - For receiving heartbeat messages
     * - A socket that connects to the management server.
     *      - For sending receipt of the heartbeat messages
     * - A socket that connects to the raft nodes.
     *      - For sending messages to the raft node
     *      - Socket is async
     */
     
    private ZMQ.Socket manServerSocket,
        heartBeatSubSocket,
        heartBeatRepSocket,
        shutDownSocket;        
    
    private ZMQ.Context context = ZMQ.context(1);
    private ZMQ.Poller poller;
    
    // Utils object
    public final Utils utils = new Utils();
    
    // Logger
    public final static Logger logger = Logger.getLogger(Consumer.class.getName());
    public static FileHandler handler;
    
    // Timer object 
    public Timer consumerTimer;
    
    Consumer(){
        this.currentClusterState = SystemState.ClusterState.DOWN;
        this.details = new NodeDetails.NodeDetailsBuilder(false)
                            .setPublicNodeName(PUBLICNODENAME)
                            .setPrivateNodeName("customer")
                            .build();
        this.poller = new ZMQ.Poller(3);
        this.nextNodeToTry = 0;
        this.clusterRaftNodes = new ArrayList<>();
        this.negotationSockets = new HashMap<>();
        this.setUpSockets();
    }
    
    public static void startLogging(){
        try{
            APPLOGFILENAME = "logs/" + PUBLICNODENAME + ".log";
            handler = new FileHandler(APPLOGFILENAME, true);
        } catch ( SecurityException | IOException e ){
            e.printStackTrace();
        }
        
        System.out.println("===============================================");
        String strToWrite = "This is " + PUBLICNODENAME + "'s log";
        System.out.println(strToWrite);
        System.out.println("===============================================");
        handler.setFormatter(new SimpleFormatter());
        logger.setUseParentHandlers(false);
        logger.addHandler(handler);
        logger.setLevel(Level.INFO);
    }
    
    public void setUpSockets(){
        this.manServerSocket = this.context.socket(ZMQ.DEALER);
        this.manServerSocket.setIdentity(this.details.getPublicNodeName().getBytes());
        this.manServerSocket.connect(Constants.MANAGEMENT_SERVER_COMMAND_PORT);
        
        this.heartBeatSubSocket = this.context.socket(ZMQ.SUB);
        this.heartBeatSubSocket.connect(Constants.MANAGEMENT_SERVER_SUB_PORT);
        this.heartBeatSubSocket.subscribe("".getBytes());
        
        this.heartBeatRepSocket = this.context.socket(ZMQ.REQ);
        this.heartBeatRepSocket.connect(Constants.MANAGEMENT_SERVER_HEARTBEAT_PORT);
        
        this.shutDownSocket = this.context.socket(ZMQ.REQ);
        this.shutDownSocket.connect(Constants.MANAGEMENT_SERVER_SHUTDOWN_PORT);
		
        this.poller.register(this.manServerSocket, ZMQ.Poller.POLLIN);
        this.poller.register(this.heartBeatSubSocket, ZMQ.Poller.POLLIN);
    }
    
    private void tearDownSockets(){
        this.manServerSocket.close();
        this.heartBeatRepSocket.close();
        this.heartBeatSubSocket.close();
        this.tearDownNegotationSockets();
        this.context.term();
    }
    
    private void printPeers(){
        String strToLog = "Current total num of raftNodes: " + this.clusterRaftNodes.size(); 
        this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
        this.printState("Nodes that are part of this cluster: ", "INFO", this.details.getPrivateNodeName());
        for (NodeDetails details: this.clusterRaftNodes){
            this.printState(details.toString(), "INFO", this.details.getPrivateNodeName());
        }
    }
    
    private void setUpNegotiationSockets(){
        for (NodeDetails item: this.clusterRaftNodes){
            if (!item.getPrivateNodeName().equalsIgnoreCase(this.details.getPrivateNodeName())){
                ZMQ.Socket socket = this.context.socket(ZMQ.DEALER);
                String socketname = UUID.randomUUID().toString();
                String logStr = "This is the dealer socket identity=" +
                    socketname + " on this node that is bound to the router socket of node=(" +
                    item.getPrivateNodeName() + ")";
                this.printState(logStr, "INFO", this.details.getPrivateNodeName());
                socket.setIdentity(socketname.getBytes());
                socket.connect(item.getIPAddress());
                this.negotationSockets.put(item.getPrivateNodeName(), socket);
            }
        }
    }
    
    private void printState(String message, String type, String privateName){
        String strToWrite = "[STATE at " + privateName +  "]: " + message;
        System.out.println(strToWrite);
        switch(type){
            case "INFO":
                logger.log(Level.INFO, strToWrite);
                break;
            case "WARNING":
                logger.log(Level.WARNING, strToWrite);
                break;
            default:
                System.out.println("Cannot log!!!!");
                break;
        }
    }
        
    private void stopProducingMessages(){
        if (consumerTimer != null){
            consumerTimer.cancel();   // only removes future tasks
            consumerTimer.purge(); // removes currently running tasks
            consumerTimer = null;
        }
    }
    
    public void sendGetMessageToServer(){
        ZMQ.Socket socket = negotationSockets.get(leaderId);
        String strToWrite = "Sent GET message to raftnode leader on this socket=" + 
            new String(socket.getIdentity());
        Message message = createMessageToSend(MessageType.Command.RAFT,
            MessageType.Raft.CLIENTGET, MessageType.Command.GET_SAVEDENTITY);
        message.setNodeIDFrom(new String(socket.getIdentity()));
        socket.send(utils.convertToBytes(message), 0);
        printState(strToWrite, "INFO", details.getPrivateNodeName());
        //logger.log(Level.INFO, strToWrite);
    }
    
    private void startProducingMessages(){
        if (consumerTimer == null){
            TimerTask task = new TimerTask(){
                @Override
                public void run(){
                    sendGetMessageToServer();
                }
            };
            consumerTimer = new Timer();
            consumerTimer.scheduleAtFixedRate(task, 0, 6000);
        }
    }
    	
    public Message createMessageToSend(MessageType.Command messageType, 
        MessageType.Raft raftype, Object ... args){
        Message messageToSend = new Message(messageType);
        if (raftype != null){
            messageToSend.setPayloadType(raftype);
        }
        if (args != null){
            for (Object arg: args){
                messageToSend.addPayloadPart(arg);
            }
        }
        return messageToSend;
    }
    
    private void tearDownNegotationSockets(){
        for (ZMQ.Socket socket: this.negotationSockets.values()){
            socket.close();
        }
    }
    
    private void checkForNodesToTry(String start){
        NodeDetails node;
        ZMQ.Socket socket;
        String str;
        if (start.isEmpty()) {
            // Unregister that socket
            // Work backwards
            int lastnodeTried = ( ( (this.nextNodeToTry - 1) % this.clusterRaftNodes.size() )
                + this.clusterRaftNodes.size() ) % this.clusterRaftNodes.size();
            str = "Index tried: " + lastnodeTried + " of " + this.clusterRaftNodes.size();
            this.printState(str, "INFO", this.details.getPrivateNodeName());
            node = this.clusterRaftNodes.get(lastnodeTried);
            socket = this.negotationSockets.get(node.getPrivateNodeName());
            this.poller.unregister(socket);
        }
            
        str = "Current index to try: " + this.nextNodeToTry + " of " + this.clusterRaftNodes.size();
        this.printState(str, "INFO", this.details.getPrivateNodeName());
        node = this.clusterRaftNodes.get(this.nextNodeToTry);
        socket = this.negotationSockets.get(node.getPrivateNodeName());
        this.poller.register(socket, ZMQ.Poller.POLLIN);
        this.nextNodeToTry = (this.nextNodeToTry + 1) % this.clusterRaftNodes.size();
        
        str = "Next index to try: " + this.nextNodeToTry + " of " + this.clusterRaftNodes.size();
        this.printState(str, "INFO", this.details.getPrivateNodeName());        
                
        Message message = this.createMessageToSend(MessageType.Command.RAFT,
                MessageType.Raft.CLIENTGET, MessageType.Command.GETLEADERID);
        
        message.setNodeIDFrom(new String(socket.getIdentity()));
        
        if (socket != null){
                socket.send(this.utils.convertToBytes(message),0);
        }
    }
    
    @Override
    public void run(){
        try{
            while(!Thread.currentThread().isInterrupted()){
                try{
                    Message message;
                    String strMessage, strToLog;
                    MessageType.Command type;
                    ZMQ.Socket socket;
                    
                    this.poller.poll();
                    
                    if (this.poller.pollin(0)){
                        message = (Message) this.utils.convertFromBytes(this.manServerSocket.recv(0));
                        type = message.getMessageType();
                        switch(type){
                            case START:
                                if (this.currentClusterState == SystemState.ClusterState.STARTED){
                                    this.currentClusterState = SystemState.ClusterState.RUNNING;
                                    this.printState("Changing ClusterNodeState from STARTED to RUNNING", 
                                        "INFO", 
                                        this.details.getPrivateNodeName()
                                    );
                                    this.clusterRaftNodes = (ArrayList<NodeDetails>) message.getPayloadPart();
                                    this.printState("Received this clusterOfNodes", 
                                        "INFO", 
                                        this.details.getPrivateNodeName()
                                    );
                                    this.printPeers();
                                    this.setUpNegotiationSockets();
                                    this.checkForNodesToTry("START");
                                    //thrd.start();
                                    //this.innerThreadSocket.send(this.utils.convertToBytes(new Message(type)), 0);
                                    break;
                                } else {
                                    strToLog = "Received START when node is already started";
                                    this.printState(strToLog, "WARNING", this.details.getPrivateNodeName());
                                }
                                break;
                            case QUIT:
                            // Quitting 
                                this.shutDownSocket.send(
                                    this.utils.convertToBytes(
                                        this.createMessageToSend(MessageType.Command.HEARTBEAT, 
                                            null, PUBLICNODENAME)
                                    ), 0
                                );
                                this.printState( 
                                    "Sent QUIT message ON [Node Shutdown Endpoint] TO (Management Server)", 
                                    "INFO", 
                                    this.details.getPrivateNodeName()
                                );
                                message = (Message) this.utils.convertFromBytes(this.shutDownSocket.recv(0));
                                strToLog = "MESSAGE RECEIVED: (" + message.getMessageType() + ") ON [Node Shutdown Endpoint] FROM (Management Server)";
                                this.printState(
                                    strToLog, 
                                    "INFO", 
                                    this.details.getPrivateNodeName()
                                );
                                if ("GOODBYE".equalsIgnoreCase(message.getMessageType().name())){
                                    Thread.currentThread().interrupt();
                                }
                                break;
                            default:
                                this.printState("This command is not recognised", 
                                    "WARNING",
                                    this.details.getPrivateNodeName()
                                );
                                break;
                        }
                        strToLog = "PROCESSING END: (Command Type: " + type.name() + ")";
                        this.printState(strToLog, 
                            "INFO",
                            this.details.getPrivateNodeName()
                        );
                    }
                    
                    if (this.poller.pollin(1)){
                        message = (Message) this.utils.convertFromBytes(this.heartBeatSubSocket.recv(0));
                        strToLog = "MESSAGE RECEIVED: (" + (String) message.getPayloadPart() + ") ON [Subs Socket] FROM (Management server)";
                        this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                        
                        // Communicating with the management server
                        
                        this.heartBeatRepSocket.send(
                            this.utils.convertToBytes(
                                this.createMessageToSend(MessageType.Command.HEARTBEAT, 
                                    null, PUBLICNODENAME)
                            ), 0
                        );
                        this.printState("MESSAGE SENT: (" + PUBLICNODENAME + ") ON [Node Rep Endpoint] TO (Management Server)", 
                                "INFO", this.details.getPrivateNodeName());
                        message = (Message) this.utils.convertFromBytes(this.heartBeatRepSocket.recv(0));
                        strMessage = (String) message.getPayloadPart();
                        strToLog = "MESSAGE RECEIVED: (" + strMessage + ") ON [Node Rep Endpoint] FROM (Management Server)";
                        this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                        
                        if ("I have seen you".equalsIgnoreCase(strMessage) && this.currentClusterState == SystemState.ClusterState.DOWN){
                            this.manServerSocket.send(
                                this.utils.convertToBytes(
                                    this.createMessageToSend(MessageType.Command.HEARTBEAT,
                                        null, this.details)
                                ),0
                            );
                            strToLog = "MESSAGE SENT: (" + 
                                    this.details + 
                                    ") ON [Node Public Endpoint] TO (Management Server)";
                            this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                            this.currentClusterState = SystemState.ClusterState.STARTED;
                            this.printState("Changing ClusterState from DOWN to STARTED", 
                                "INFO", this.details.getPrivateNodeName());
                        } else if ("I have seen you".equalsIgnoreCase(strMessage) && this.currentClusterState == SystemState.ClusterState.STARTED && this.clusterRaftNodes.isEmpty() == true){
                            // This is on the rare chance the managmenet server receives its heartbeat 
                            // before the management server sends the entire cluster of raftnodes to the 
                            // raftnodes themselves
                            this.printState("Node is at ClusterState STARTED", 
                                "INFO", this.details.getPrivateNodeName());
                            this.manServerSocket.send(
                                this.utils.convertToBytes(
                                    this.createMessageToSend(MessageType.Command.HEARTBEAT,
                                        null, this.details)
                                ),0
                            );
                            strToLog = "MESSAGE SENT: (" + 
                                    this.details + 
                                    ") ON [Node Public Endpoint] TO (Management Server)";
                            this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                        }
                    }
                    
                    if (this.currentClusterState == SystemState.ClusterState.RUNNING){
                        if (this.poller.pollin(2)){
                            socket = this.poller.getSocket(2);
                            message = (Message) this.utils.convertFromBytes(socket.recv(0));
                            type = message.getMessageType();
                            strToLog = "Received a message of type=[" + type.name() + "]";
                            this.printState(
                                strToLog,
                                "INFO",
                                this.details.getPrivateNodeName()
                            );
                            switch(type){
                                case GETLEADERID_REP:
                                    MessageType.Command responsetype = (MessageType.Command) message.getPayloadPart();
                                    switch (responsetype){
                                        case ACK:
                                            this.leaderId = (String) message.getPayloadPart();
                                            strToLog = "Found the leader=" + this.leaderId + " Connecting now...";
                                            this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                                            this.startProducingMessages();
                                            break;
                                        case NACK:
                                            if (this.leaderId == ""){
                                                this.printState("Did not find leader.Retrying..", "INFO", this.details.getPrivateNodeName());
                                                this.checkForNodesToTry("");
                                            } else {
                                                strToLog = "Received NACK when leader is known.Disregarding..";
                                                this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                                            } 
                                            break;
                                    }
                                    break;
                                case RAFT:
                                    MessageType.Raft raftype = message.getPayloadType();
                                    switch(raftype){
                                        case CLIENTGET_REP:
                                            String checkAddress = (String)message.getPayloadPart();
                                            if (checkAddress.isEmpty()){
                                                this.leaderId = "";
                                                this.stopProducingMessages();
                                                this.checkForNodesToTry("");
                                            } else {
                                                MessageType.Command restype = (MessageType.Command)message.getPayloadPart();
                                                strToLog = "Received raftype=" + raftype + ", subtype=" + restype;
                                                this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                                                StringBuilder sb = new StringBuilder();
                                                switch (restype){
                                                    case ACK:
                                                        sb.append("Received (");
                                                        sb.append(String.valueOf((int) message.getPayloadPart()));
                                                        sb.append(") from raftnode [");
                                                        sb.append(checkAddress);
                                                        sb.append("]");
                                                        System.out.println(sb.toString());
                                                        this.printState(sb.toString(), 
                                                            "INFO",
                                                            this.details.getPrivateNodeName()
                                                        );
                                                        break;
                                                    case NACK:
                                                        sb.append("Received (");
                                                        if (message.isEmpty()){
                                                            sb.append("null) from ratnode[");
                                                        } else {
                                                            sb.append(String.valueOf((int) message.getPayloadPart()));
                                                            sb.append(") from raftnode [");
                                                        }
                                                        sb.append(checkAddress);
                                                        sb.append("]");
                                                        System.out.println(sb.toString());
                                                        this.printState(sb.toString(), 
                                                            "INFO",
                                                            this.details.getPrivateNodeName()
                                                        );
                                                        break;
                                                }
                                            }
                                            break;
                                        default:
                                            this.printState("This command is not recognised", 
                                                "WARNING",
                                                this.details.getPrivateNodeName()
                                            );
                                            break;
                                    }
                                    break;
                                default:
                                    this.printState("This command is not recognised", 
                                        "WARNING",
                                        this.details.getPrivateNodeName()
                                    );
                                    break;
                            }
                            strToLog = "Processing end of message of type=( " + type.name() + ")";
                            this.printState(strToLog, 
                                "INFO",
                                this.details.getPrivateNodeName()
                            );
                        }
                    }
                    Thread.sleep(2000);
                } catch (InterruptedException ie){
                    ie.printStackTrace();
                }
            }
                
            if (Thread.currentThread().isInterrupted()){
                return;
            }
            
        } catch (Exception ie){
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ie.printStackTrace(pw);
            System.out.println(sw.toString());
        }
        
        this.tearDownSockets();
    }
    
    public static void main(String [] args){
        startLogging();
        // String type = System.getenv("TYPE");
        // int nodeNum = Integer.parseInt(System.getenv("NODENUM"));
        // String strRT = "TYPE: " + type + " NODENUM: " + nodeNum; 
        // System.out.println(strRT);
        Consumer consumer = new Consumer();
        consumer.start();
    }
}