/**
 *  Copyright 2017 Okusanya Oluwadamilola
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
 
import java.io.Serializable;

/**
 *  LogEntry object for the raft substrate of the 
 *  FittChart application
 */
 
public class RaftLogEntry implements Serializable{
    
    private static final long serialVersionUID=246815961865799286L;

    private int term;
    private int index;
    private Object command;
    
    RaftLogEntry(int index, int term, Object command){
        this.index = index;
        this.term = term;
        this.command = command;
    }

    public int getIndex(){
        return this.index;
    }
    
    public int getTerm(){
        return this.term;
    }
    
    public Object getCommand(){
        return this.command;
    }
    
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Term: ");
        sb.append(String.valueOf(this.term));
        sb.append(", Index:");
        sb.append(String.valueOf(this.index));
        sb.append(", Command:");
        sb.append(String.valueOf(this.command));
        return sb.toString();
    }
}
