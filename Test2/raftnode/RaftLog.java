/**
 *  Copyright 2017 Okusanya Oluwadamilola
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import java.util.ArrayList;

/**
 *  Log object for the raft substrate of the 
 *  FittChart application
 */
 
public class RaftLog{

    private ArrayList<RaftLogEntry> log;
    private int prevLastLogIndex,
                prevLastTerm,
                lastTerm,
                lastLogIndex;
                
    RaftLog(){
        this.log = new ArrayList<>();
        this.prevLastLogIndex = 0;
        this.prevLastTerm = 0;
        this.lastTerm = 0;
        this.lastLogIndex = 0;
        
        // The index of the RaftLog always starts at 1
        //this.append(0, null);
        this.log.add(new RaftLogEntry(0, 0, null));
    }
    
    private void append(int term, Object command){
        this.prevLastLogIndex = this.lastLogIndex;
        this.prevLastTerm = this.lastTerm;
        this.lastLogIndex++;
        this.lastTerm = term;
        this.log.add(new RaftLogEntry(this.lastLogIndex, this.lastTerm, command));
    }
    
    public void appendEntries(ArrayList<RaftLogEntry> entries){
        if (entries.size() != 0){
            for (RaftLogEntry entry: entries){
                this.append(entry.getTerm(), entry.getCommand());
            }
        }
    }
    
    public void deleteFrom(int start){
        for (int index = start; index < this.lastLogIndex ; index++){
            this.log.remove(index);
        }
    }
    
    public int getLastTerm(){
        return this.lastTerm;
    }
    
    public int getLastLogIndex(){
        return this.lastLogIndex;
    }
    
    public RaftLogEntry getEntryAt(int index){
        if (index <= 0){
            return this.log.get(0);
        } else {
            return this.log.get(index);
        }
    }
    
    public ArrayList<RaftLogEntry> getLog(){
        return this.log;
    }
    
    public ArrayList<RaftLogEntry> getEntriesFromToEnd(int start){
        return this.getEntriesFromRange(start, this.getLastLogIndex(), this.getLog());
    }
    
    public ArrayList<RaftLogEntry> getEntriesFromRange(int start, int end, ArrayList<RaftLogEntry> list){
        if (end == 0 || start < 0 || start == end ){
            return new ArrayList<RaftLogEntry>();
        }
        return new ArrayList<RaftLogEntry>(list.subList(start, end));
    }
    
    public RaftLogEntry getEntryAtPrevIndex(int index){
        RaftLogEntry entry;
        if (index <= 0){
            entry = this.getEntryAt(0);
        } else {
            entry = this.getEntryAt(index - 1);
        }
        return entry;
    }
    
    // find the first index of the conflicting entry
    public int findConflictingIndex(int term){
        for (RaftLogEntry entry: this.log){
            if (entry.getTerm() == term){
                return entry.getIndex();
            }
        }
        return 0;
    }
}
