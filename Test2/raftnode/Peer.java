/**
 *  Copyright 2017 Okusanya Oluwadamilola
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
 
/**
 *  Peer class
 *  Class that contains the raftnode object
 *  - This performs three functions
 *      - The Raft process that holds elections. It also contains the
        Timer process.
 *      - The file process that polls the log to write the values ot the  
        memory-mapped queues.
 */

import java.io.PrintWriter;
import java.io.StringWriter; 
import java.io.BufferedReader;
import java.io.BufferedWriter; 
import java.io.IOException;
import java.util.concurrent.ThreadLocalRandom;
import org.zeromq.ZMQ;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.Iterator;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Collections;
import java.util.ArrayList;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Peer extends Thread{
    
    private final static String HOSTNAME = System.getenv("HOSTNAME");
    private final static String PUBLICNODENAME= "Cluster-" + HOSTNAME;      // used to connect to the managment server
    private final static String PRIVATENODENAME = "Raft-" + HOSTNAME;       // used to connect to the raft nodes
    private static String APPLOGFILENAME;
    private ArrayList<NodeDetails> clusterRaftNodes;
    private HashMap<String, ZMQ.Socket> dealerSockets;
    private NodeDetails details;
    private boolean hasStartedAppendEntries = false;
    
    private SystemState.ClusterState currentClusterState;  // this is mainly for shutdown and startup
    private final int WAITCOUNT = 10;
    private int waitCount;
    private int indexOfEntrySent = 0;
    
    /**
     *  Sockets that belongs to the thread/node 
     *
     *  - A socket that connects to other peers
     *      - For sending messages to the peers.
     *      - Async
     *  - A socket that connects to other peers
     *      - For receiving messages to the peers.
     *      - Async
     *  - A socket that connects to the management server
     *      - For receiving heartbeats
     *  - A socket that connects to the management server
     *      - For sending/receiving messages to the server
     *      - Async
     *  - A socket that connects to the management server.
     *      - For sending receipt of the heartbeat messages
     *  - A socket that connects to the file thread
     *      - For sending/receiving messages to the thread
     *  - A socket that connects to the timer thread
     *      - For sending/receiving messages to the thread
     */
        
    private ZMQ.Socket manServerSocket,
        heartBeatSubSocket,
        heartBeatRepSocket,
        raftNodeRcvSocket,
        raftNodeSndSocket,
        fileThreadSocket,
        timerThreadSocket,
        shutDownSocket;

    private ZMQ.Context context = ZMQ.context(1);
    private ZMQ.Poller poller;
    
    // Utils object
    private final Utils utils = new Utils();
    
    // FileThread object
    private FileThread fileThr;

    // Timer object
    private TimerThread timerThr;
    
    // Timestamp object
    private Timestamp initialElectionTimestamp;
    
    // Logger
    public final static Logger logger = Logger.getLogger(Peer.class.getName());
    public static FileHandler handler;
    
    /**** Peer State variables start ******/
    private int curTerm,            // the lastest term that server has seen
                commitIndex,        // highest log entry known to be committed
                lastApplied;        // index of log entry applied to state machine 
                
    
    private String votedFor = "",        // the node that received a vote in the current term
                leaderAddress = "",           // the node that is the current leader 
                selfEndpoint = "";       // this node Id
        
    // for each remote peer, index of the next log entry to send to that server
    private HashMap<String, Integer> nextIndex;
    
    // for each remote peer, index of the highest log entry 
    // known to be replicated on the server
    private HashMap<String, Integer> matchIndex;
    
    // the state of the node(raft election)
    private SystemState.RaftState currentNodeState;     
    
    // the list of voters that have voted for the node during the current term
    private ArrayList<String> voters;
    
    // the log associated with the node
    private RaftLog log;
    
    // Quorum size
    private int majority;
    
    /**** Peer State variables end*****/
    
    Peer(String nodename){
        this.currentClusterState = SystemState.ClusterState.DOWN;
        this.currentNodeState = SystemState.RaftState.FOLLOWER;
        
        String address = "tcp://" + nodename + ":6500";
        this.details = new NodeDetails.NodeDetailsBuilder(false)
                            .setPublicNodeName(PUBLICNODENAME)
                            .setPrivateNodeName(PRIVATENODENAME)
                            .setIPAddress(address)
                            .build();
        System.out.println("My details are : " + this.details);
        this.log = new RaftLog();
        this.dealerSockets = new HashMap<String, ZMQ.Socket>();
        this.matchIndex = new HashMap<String, Integer>();
        this.nextIndex = new HashMap<String, Integer>();
        this.voters = new ArrayList<>();
        this.clusterRaftNodes = new ArrayList<>();
        
        this.poller = new ZMQ.Poller(4);
        
        this.setUpSockets();
    }
    
    private void setUpSockets(){
        this.fileThreadSocket = this.context.socket(ZMQ.PAIR);
        this.fileThreadSocket.bind(Constants.FILESERVER_PORT);
        
        this.timerThreadSocket = this.context.socket(ZMQ.PAIR);
        this.timerThreadSocket.bind(Constants.TIMERSERVER_PORT);
        
        this.manServerSocket = this.context.socket(ZMQ.DEALER);
        this.manServerSocket.setIdentity(this.PUBLICNODENAME.getBytes());
        this.manServerSocket.connect(Constants.MANAGEMENT_SERVER_COMMAND_PORT);
        
        this.heartBeatSubSocket = this.context.socket(ZMQ.SUB);
        this.heartBeatSubSocket.connect(Constants.MANAGEMENT_SERVER_SUB_PORT);
        this.heartBeatSubSocket.subscribe("".getBytes());
        
        this.heartBeatRepSocket = this.context.socket(ZMQ.REQ);
        this.heartBeatRepSocket.connect(Constants.MANAGEMENT_SERVER_HEARTBEAT_PORT);
        
        this.raftNodeRcvSocket = this.context.socket(ZMQ.ROUTER);
        this.raftNodeRcvSocket.bind(Constants.RAFTCLUSTER_RAFTNODE_RECV_PORT);
        
        this.shutDownSocket = this.context.socket(ZMQ.REQ);
        this.shutDownSocket.connect(Constants.MANAGEMENT_SERVER_SHUTDOWN_PORT);
        
        ////this.raftNodeSndSocket = this.context.socket(ZMQ.DEALER);
        ////this.raftNodeSndSocket.setIdentity(this.details.getPrivateNodeName().getBytes());
        
        this.poller.register(this.manServerSocket, ZMQ.Poller.POLLIN);
        this.poller.register(this.heartBeatSubSocket, ZMQ.Poller.POLLIN);
        this.poller.register(this.raftNodeRcvSocket, ZMQ.Poller.POLLIN);
        this.poller.register(this.timerThreadSocket, ZMQ.Poller.POLLIN);
        
        this.fileThr = new FileThread(this.context, HOSTNAME);
        this.timerThr = new TimerThread(this.context, 
                            HOSTNAME, this.details.getPrivateNodeName());
    }
    
    
    /** This sets up dealer sockets for each raft node in the cluster, apart from itself.
     * A router node can receive from multiple delar sockets. Initially the configuration
     * was one dealer/router on the thread. This was based on the faulty assumption that
     * the sockets could be reused. Unforuntaley this is not the case, as trying to reuse
     * the socket by closing and redefining it or by conneting and disconnecting are 
     * esssientially undefined async operations. So the only route was to go for 
     * a more static infrastructure **/
    private void setUpDealerSockets(){
        for (NodeDetails item: this.clusterRaftNodes){
            if (!item.getPrivateNodeName().equalsIgnoreCase(this.details.getPrivateNodeName())){
                ZMQ.Socket socket = this.context.socket(ZMQ.DEALER);
                String socketname = UUID.randomUUID().toString();
                String logStr = "This is the dealer socket identity=" +
                    socketname + " on this node that is bound to the router socket of node=(" +
                    item.getPrivateNodeName() + ")";
                this.printState(logStr, "INFO", this.details.getPrivateNodeName());
                socket.setIdentity(socketname.getBytes());
                socket.connect(item.getIPAddress());
                this.dealerSockets.put(item.getPrivateNodeName(), socket);
            }
        }
    }
    
    
    private void tearDownDealerSockets(){
        for (ZMQ.Socket socket: this.dealerSockets.values()){
            socket.close();
        }
    }
    
    private void tearDownSockets(){
        this.shutDownSocket.close();
        this.heartBeatRepSocket.close();
        this.heartBeatSubSocket.close();
        this.raftNodeRcvSocket.close();
        this.tearDownDealerSockets();
        this.fileThreadSocket.close();
        this.timerThreadSocket.close();
        this.manServerSocket.close();
        this.context.term();
    }
    
    private void changeStateToCandidate(){
        if (this.currentNodeState == SystemState.RaftState.FOLLOWER){
            this.curTerm++;
            Message message = this.createMessageToSend(MessageType.Command.SAVEDSTATE,
                MessageType.Raft.TERM, String.valueOf(this.curTerm));
            this.save(message);
            this.currentNodeState = SystemState.RaftState.CANDIDATE;
            this.voters.clear();
            this.voters.add(this.details.getIPAddress());
            this.votedFor = this.details.getIPAddress();
            message = this.createMessageToSend(MessageType.Command.SAVEDSTATE,
                MessageType.Raft.VOTEDFOR, this.votedFor);
            this.save(message);
            this.printState(
                "Broadcasting Request Votes", 
                "INFO",
                this.details.getPrivateNodeName()
            );
            this.broadCastRequestVotes();
        } else {
            this.printState(
                "Node is in Candidate State (Change Ignored)", 
                "INFO",
                this.details.getPrivateNodeName()
            );
        }
    }
    
    public void changeStateToFollower(int candidateTerm, String leader){
        this.printState(
            "Changing to follower", 
            "INFO",
            this.details.getPrivateNodeName()
        );
        this.currentNodeState = SystemState.RaftState.FOLLOWER;
        this.leaderAddress = leader;
        this.curTerm =  Math.max(candidateTerm, this.curTerm);
        Message message = this.createMessageToSend(MessageType.Command.SAVEDSTATE,
            MessageType.Raft.TERM, String.valueOf(this.curTerm));
        this.save(message);
        this.voters.clear();
        this.votedFor = "";
    }
    
   public void changeStateToLeader(){
        this.printState(
            "Changing from Candidate to Leader", 
            "INFO",
            this.details.getPrivateNodeName()
        );
        this.currentNodeState = SystemState.RaftState.LEADER;
        this.leaderAddress = this.details.getPrivateNodeName();
        this.voters.clear();
        this.votedFor = "";
        
        // Reinitialization of entries after election
        for (NodeDetails item: this.clusterRaftNodes){
            this.nextIndex.put(item.getPrivateNodeName(), this.log.getLastLogIndex() + 1);
            this.matchIndex.put(item.getPrivateNodeName(), 0);
        }
        
        Message message = this.createMessageToSend(MessageType.Command.SETLEADER,
            MessageType.Raft.SETLEADERID, this.leaderAddress);
    
        // Sending to managementserver
        this.manServerSocket.send(
            this.utils.convertToBytes(message), 0
        );
        String logStr = "MESSAGE SENT: (" + MessageType.Raft.SETLEADERID.name() + 
                ") ON [Node Public EndPoint] TO (Management Server)";
        this.printState(logStr, "INFO", this.details.getPrivateNodeName());
        
        // Sending to all the raft nodes in the cluster
        String myname = this.details.getPrivateNodeName();
        for (NodeDetails item: this.clusterRaftNodes){
            if (!myname.equalsIgnoreCase(item.getPrivateNodeName())){
                this.unicastMessage(item.getPrivateNodeName(), message);
            }
        }
    }
    
    private String findNameForSocketId(String id){
        for ( Map.Entry<String, ZMQ.Socket> entry: this.dealerSockets.entrySet()){
            String name = entry.getKey();
            ZMQ.Socket value = entry.getValue();
            String identity = new String(value.getIdentity());
            String logStr = "Identity of socket= " + identity;
            this.printState(logStr, "INFO", this.details.getPrivateNodeName());
            if (id.equalsIgnoreCase(identity)){
                return name;
            } 
        }
        return "";
    }
    
    public Message createMessageToSend(MessageType.Command messageType, 
            MessageType.Raft raftype, Object ... args){
        Message messageToSend = new Message(messageType);
        if (raftype != null){
            messageToSend.setPayloadType(raftype);
        }
        if (args != null){
            for (Object arg: args){
                messageToSend.addPayloadPart(arg);
            }
        }
        return messageToSend;
    }
    
    private void unicastMessage(String idToSendTo, Message message){
        String logStr = "Connecting to node=(" + idToSendTo + ")";
        this.printState(logStr, "INFO", this.details.getPrivateNodeName());
        //message.addPayloadPart(this.details.getPrivateNodeName());
        message.setNodeIDFrom(this.details.getPrivateNodeName());
        ZMQ.Socket socket = this.dealerSockets.get(idToSendTo);
        socket.send(this.utils.convertToBytes(message), 0);
        logStr = "MESSAGE SENT: type=" + message.getMessageType() + ",subtype=" + 
            message.getPayloadType() + " ON [Node Private Endpoint] TO (" + idToSendTo + ")"; 
        this.printState(logStr, "INFO", this.details.getPrivateNodeName());
    }

    
    private void broadCastRequestVotes(){
              
        // Building messages
        int term = this.log.getLastTerm();
        int logIndex = this.log.getLastLogIndex();
        Message message = new Message(MessageType.Command.RAFT);
        message.setPayloadType(MessageType.Raft.REQUESTVOTE);
        message.addPayloadPart(this.curTerm);
        message.addPayloadPart(logIndex);
        message.addPayloadPart(term);
        
        String strToWrite = "[Message Sent by " + this.PUBLICNODENAME + "]: Broadcasting request votes";
        this.printState(strToWrite, "INFO", this.details.getPrivateNodeName());
        
        for (NodeDetails item : this.clusterRaftNodes){
            if(!item.getIPAddress().equalsIgnoreCase(this.details.getIPAddress())){
                this.unicastMessage(item.getPrivateNodeName(), message); 
            }
        }
    }
        
    /** Nodes kept answering requestVotes messages even after there is a 
    ** a leaderId **/
    
    public void processRequestVote(Message message, String candidateId){
        int candidate_Term = (int) message.getPayloadPart();
        int candidate_LogIndex = (int) message.getPayloadPart();
        int candidate_LogTerm = (int) message.getPayloadPart();        
        
        String logStr = "My current term= " + this.curTerm;
        this.printState(logStr, "INFO", this.details.getPrivateNodeName());
        logStr = "Candidate's term= " + candidate_Term;
        this.printState(logStr, "INFO", this.details.getPrivateNodeName());
        
        // The candidate's term is greater than the node's term
        if (this.curTerm < candidate_Term){
            this.printState("[RequestVote]: Changing to Follower from Candidate", 
                    "INFO", this.details.getPrivateNodeName());
            // force the node to go to follower state even if it is follower state
            this.changeStateToFollower(candidate_Term, "");         
        }
        
        message = new Message(MessageType.Command.RAFT);
        message.setPayloadType(MessageType.Raft.REQUESTVOTE_REP);
        message.addPayloadPart(this.curTerm);
        
        if (this.curTerm > candidate_Term || isCandidateLogUptodate(candidate_LogIndex, 
            candidate_LogTerm) == false){
            message.addPayloadPart(MessageType.Command.NACK);
            
            if (this.curTerm > candidate_Term){
                this.printState(
                    "[RequestVote]: Candidate Term too small", 
                    "WARNING",
                    this.details.getPrivateNodeName()
                );
            } else {
                this.printState(
                    "[RequestVote]: Candidate Logs not up to date", 
                    "WARNING",
                    this.details.getPrivateNodeName()
                );
            }
        } else if (isCandidateLogUptodate(candidate_LogIndex, candidate_LogTerm)){
            // The candidate node that is receiving its own request vote starts here
            // The node grants the vote 
            
            this.printState(
                "This candidate's term is equal to the received term and the logs are up to date", 
                "INFO",
                this.details.getPrivateNodeName()
            );
            message.addPayloadPart(MessageType.Command.ACK);
            
            // The node saves the candidate's votedFor 
            if (this.votedFor == "" || !this.votedFor.equalsIgnoreCase(candidateId)){ 
                if (this.votedFor == ""){
                    this.printState(
                        "Voted for is empty", 
                        "INFO",
                        this.details.getPrivateNodeName()
                    );
                } else {
                    this.printState(
                        "Changing votedFor", 
                        "INFO",
                        this.details.getPrivateNodeName()
                    );
                }
                this.votedFor = candidateId;
                Message messageToSave = this.createMessageToSend(MessageType.Command.SAVEDSTATE, 
                    MessageType.Raft.VOTEDFOR, this.votedFor);
                this.save(messageToSave);
            }
        }
        
        this.unicastMessage(candidateId, message);
    }
    
    public void processRequestVoteRep(Message message, String followerId){
        if (this.isACandidate() == false){
            this.printState(
                "Received RequestVote rep and am not a candidate in this term, Disregarding", 
                "WARNING",
                this.details.getPrivateNodeName()
            );
            return;
        }
        
        int followerTerm = (int) message.getPayloadPart();
        MessageType.Command type = (MessageType.Command) message.getPayloadPart();
         
        if (this.curTerm > followerTerm){
            String str = "Request Vote from " + followerId + " ignored, stale term";
            this.printState(
                str, 
                "WARNING",
                this.details.getPrivateNodeName()
            );
        } else if (this.curTerm < followerTerm){
            String str = "Request Vote from " + followerId + " denied";
            this.printState(
                str, 
                "WARNING",
                this.details.getPrivateNodeName()
            );
            this.changeStateToFollower(followerTerm, "");
        } else {
            if (type == MessageType.Command.ACK){
                this.voters.add(followerId);
                // this works if voters.size >= majoirty
                if (this.getNumberOfVoters() >= this.getMajority()){
                    this.changeStateToLeader();
                }
            } else if (type == MessageType.Command.NACK){
                String str = "Request Vote from " + followerId + " denied";
                this.printState(
                    str, 
                    "WARNING",
                    this.details.getPrivateNodeName()
                );
            }
        }
        
    }
    
    public void processAppendEntries(Message message, String leaderId){
        if (this.isAFollower()){
            if (this.waitCount > 0){
                this.waitCount = 0;
            }
        }
        int remoteTerm = (int) message.getPayloadPart();
        int leader_prevLogIndex = (int) message.getPayloadPart();
        int leader_prevTerm = (int) message.getPayloadPart();
        int leader_commitIndex = (int) message.getPayloadPart();
        ArrayList<RaftLogEntry> entries = (ArrayList<RaftLogEntry>) message.getPayloadPart();
        
        
        message = new Message(MessageType.Command.RAFT);
        message.setPayloadType(MessageType.Raft.APPENDENTRIES_REP);
        message.addPayloadPart(this.curTerm);
        
        if (this.curTerm == remoteTerm){
            if (this.isALeader() || this.isACandidate()){
                // Candidate 
                // just in case an election just happened before this node received this AppendEntries
                // Leader
                // just in case if a better leader is found
                if (this.isALeader()){
                    this.printState("[AppendEntries]: A better leader is found", 
                        "SEVERE", this.details.getPrivateNodeName());
                    this.changeStateToFollower(remoteTerm, "");
                    return;
                } else if (this.isACandidate()){
                    this.printState("[AppendEntries]: A leader is found", 
                        "INFO", 
                        this.details.getPrivateNodeName());
                    this.changeStateToFollower(remoteTerm, leaderId);
                }
            }
            
            if (this.leaderAddress == ""){
                this.leaderAddress = leaderId;
            }
            
            RaftLogEntry prev = this.log.getEntryAt(leader_prevLogIndex);

            int prevLogIndex = prev.getIndex();
            int prevEntryTerm = prev.getTerm();
                
            if (prevEntryTerm == leader_prevTerm){
                // check if the log is the same, if so send back a Heartbeat response
                
                int lastLogIndex = this.log.getLastLogIndex();
                if ( entries.isEmpty()){
                    this.printState("[AppendEntries]: Entries are empty. Found heartbeat", "INFO",
                        this.details.getPrivateNodeName());
                    message.addPayloadPart(MessageType.Command.HEARTBEAT);
                } else {
                    String str = "[AppendEntries]: Found a new entry," + "Previous entry_index=" + 
                            leader_prevLogIndex+ " Previous entry_term=" + leader_prevTerm;
                    this.printState(str, "INFO", this.details.getPrivateNodeName());
                    for (RaftLogEntry entry: entries){
                        str = "[AppendEntries]: Adding new entry: " + entry;
                        this.printState(str, "INFO", this.details.getPrivateNodeName());
                    }
                    this.log.appendEntries(entries);
                    message.addPayloadPart(MessageType.Command.ACK);
                    message.addPayloadPart(lastLogIndex);
                }
                
                this.unicastMessage(leaderId, message);
                
                if (leader_commitIndex > this.commitIndex){
                    this.commitIndex = Math.min(leader_commitIndex, lastLogIndex);
                }
                
                // this.applyCommitEntries();
            } else {
                    // Catch if prevEntryTerm != leader_prevTerm or there is no previous item
                    // in the log for the leader_prevLogIndex
                String str = "[AppendEntries]: Found a conflicting index," + "Local entry_term=" 
                                + prevEntryTerm + " Actual leader entry_term=" + leader_prevTerm;
                this.printState(str, "WARNING", this.details.getPrivateNodeName());
                int conflictingIndex = this.log.findConflictingIndex(leader_prevTerm);
                this.log.deleteFrom(conflictingIndex);
                message.addPayloadPart(MessageType.Command.NACK);
                message.addPayloadPart(conflictingIndex);
                this.unicastMessage(leaderId, message);
            }
        } else {
            if (this.curTerm > remoteTerm){
                this.printState("[AppendEntries] Current Term > Remote Term: State Request", "SEVERE",
                    this.details.getPrivateNodeName());
            } else if (this.curTerm < remoteTerm){
                this.printState("[AppendEntries] Current Term < Remote Term: Current Server Term Outdated", "SEVERE",
                    this.details.getPrivateNodeName());
                this.changeStateToFollower(remoteTerm, leaderId);
            }
            message.addPayloadPart(MessageType.Command.NACK);
            this.unicastMessage(leaderId, message);
        }
    }
    
    public void processAppendEntriesRep(Message message, String followerId){
        if (this.isALeader() == false){
            return;
        }
        
        int followerTerm = (int) message.getPayloadPart();
        MessageType.Command responsetype = (MessageType.Command) message.getPayloadPart();
        
        String strToWrite = "";
        if (this.curTerm > followerTerm){
            strToWrite = "[AppendEntriesRep]: Stale Request from " + followerId; 
            this.printState(strToWrite, "WARNING", this.details.getPrivateNodeName());
        } else if (this.curTerm < followerTerm){
            this.printState("[AppendEntriesRep]: Server outdated, will switch to follower", "WARNING",
                this.details.getPrivateNodeName());
            this.changeStateToFollower(followerTerm, "");
        } else {
            switch(responsetype){
                case ACK:
                    int followerLogIndex = (int) message.getPayloadPart();
                    this.setMatchIndexFor(followerId, followerLogIndex);
                    int new_next_index = Math.min(followerLogIndex + 1,
                                            this.log.getLastLogIndex() + 1);
                    this.setNextIndexFor(followerId, new_next_index);
                    
                    // If there exists a N > commitIndex, a majority of matchIndex[i] >= N,
                    // and log[N].term == curTerm, set commitIndex = N
                    
                    ArrayList<Integer> all_match_values = 
                        new ArrayList<Integer>(this.matchIndex.values());
                    all_match_values.add(this.log.getLastLogIndex());
                    Collections.sort(all_match_values, Collections.reverseOrder());
                    int assigned_majority_index = (int) (all_match_values.size() / 2);
                    int committedIndex = all_match_values.get(assigned_majority_index);
                    int committedTerm = this.log.getEntryAt(committedIndex).getTerm();
                    
                    if (committedTerm == this.curTerm){
                        int newCommittedIndex = Math.max(committedIndex, this.commitIndex);
                        this.commitIndex = newCommittedIndex;
                        this.printState("[AppendEntriesRep]: Committing Entries", "INFO",
                            this.details.getPrivateNodeName());
                        // this.applyCommittedLogEntries;
                        // send write responses
                    }
                    break;
                case NACK:
                    if (message.getPayloadPart() != null){
                        int conflictingIndex = (int) message.getPayloadPart();
                        int newNextIndex = Math.max(1, conflictingIndex);
                        this.printState("[AppendEntriesRep]: Set new index for conflicting value", 
                            "SEVERE", this.details.getPrivateNodeName());
                        this.setNextIndexFor(followerId, newNextIndex);
                    }
                    break;
                case HEARTBEAT:
                    this.printState("[AppendEntries]: Received heartbeat from " + followerId, 
                        "INFO", this.details.getPrivateNodeName());
                    break;
            }
        }
    }
    
    private int getNextIndexFor(String id){
        return this.nextIndex.get(id);
    }
    
    private void setNextIndexFor(String id, int index){
        this.nextIndex.put(id, index);
    }
    
    private int getMatchIndex(String id){
        return this.matchIndex.get(id);
    }
    
    private void setMatchIndexFor(String id, int index){
        this.matchIndex.put(id, Math.max(this.matchIndex.get(id), index));
    }
    
    public void broadCastAppendEntries(){
    
        for (NodeDetails item: this.clusterRaftNodes){
            if (!item.getPrivateNodeName().equalsIgnoreCase(this.details.getPrivateNodeName())){
                int nextIndex = this.getNextIndexFor(item.getPrivateNodeName());
                int entryLastLogIndex = this.log.getEntryAtPrevIndex(nextIndex).getIndex();
                int entryLastLogTerm = this.log.getEntryAtPrevIndex(nextIndex).getTerm();
                ArrayList<RaftLogEntry> entries = this.log.getEntriesFromToEnd(nextIndex);
                Message messageToSend = this.createMessageToSend(MessageType.Command.RAFT,
                    MessageType.Raft.APPENDENTRIES, this.curTerm, entryLastLogIndex, 
                    entryLastLogTerm, this.commitIndex, entries);
                this.unicastMessage(item.getPrivateNodeName(), messageToSend);
            }
        }
    }
    
    private void sendMessageToExternalClient(Message message, String clientId){
        this.raftNodeRcvSocket.send(clientId, ZMQ.SNDMORE);
        this.raftNodeRcvSocket.send(this.utils.convertToBytes(message), 0);
    }
	
    private void processClientPost(Message message, String clientId){
        if (this.isALeader()){
            int counter = (int) message.getPayloadPart();
            String str = "Message payload from producer=" + counter;
            this.printState(str, "INFO", this.details.getPrivateNodeName());
            RaftLogEntry logEntry = new RaftLogEntry(this.log.getLastLogIndex(), this.curTerm, counter);
            ArrayList<RaftLogEntry> entries = new ArrayList<>();
            entries.add(logEntry);
            this.printState("Appending new message payload to my log", "INFO", this.details.getPrivateNodeName());
            this.log.appendEntries(entries);
            message = this.createMessageToSend(MessageType.Command.RAFT, MessageType.Raft.CLIENTPOST_REP, this.details.getPrivateNodeName());
            //message.addPayloadPart(this.details.getPrivateNodeName());
        } else {
            this.printState("Node is not a leader. Sending a NO-OP message for CLIENTPOST.", "INFO",
            this.details.getPrivateNodeName());
            message = this.createMessageToSend(MessageType.Command.RAFT, MessageType.Raft.CLIENTPOST_REP, "");
        }
        
        this.sendMessageToExternalClient(message, clientId);
    }
	
    private void processClientGet(Message message, String clientId){
        MessageType.Command payloadType = (MessageType.Command) message.getPayloadPart();
        
        switch (payloadType){
            case GETLEADERID:
                if (this.isLeaderKnown()){
                    this.printState("Leader is known. Giving node the address..", "INFO",
                        this.details.getPrivateNodeName());
                    message = this.createMessageToSend(MessageType.Command.GETLEADERID_REP,
                            null,  MessageType.Command.ACK, this.leaderAddress);
                } else {
                    this.printState("Leader is not known. Giving node no address..", "INFO",
                        this.details.getPrivateNodeName());
                    message = this.createMessageToSend(MessageType.Command.GETLEADERID_REP,
                            null, MessageType.Command.NACK);
                }
                break;
            case GET_SAVEDENTITY:
                // This should actually only work after an applyCommitEntries command(bit)
                if (this.isALeader()){
                    if (this.indexOfEntrySent < this.commitIndex){
                        this.indexOfEntrySent++;
                        RaftLogEntry savedEntry = this.log.getEntryAt(indexOfEntrySent);
                        Object command = savedEntry.getCommand();
                        String strToLog = "Sent message=" + String.valueOf(command) +  " to " + clientId;
                        message = this.createMessageToSend(MessageType.Command.RAFT, 
                            MessageType.Raft.CLIENTGET_REP, this.details.getPrivateNodeName(),
                            MessageType.Command.ACK, command);
                        this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                    } else {
                        this.printState("There is nothing new in the log!!! Sending old entries.", "INFO", 
                                this.details.getPrivateNodeName());
                        RaftLogEntry savedEntry = this.log.getEntryAt(indexOfEntrySent);
                        Object command = savedEntry.getCommand();
                        if (indexOfEntrySent == 0){
                            message = this.createMessageToSend(MessageType.Command.RAFT, 
                                MessageType.Raft.CLIENTGET_REP, this.details.getPrivateNodeName(),
                                MessageType.Command.NACK);
                        } else {
                            message = this.createMessageToSend(MessageType.Command.RAFT, 
                                MessageType.Raft.CLIENTGET_REP, this.details.getPrivateNodeName(),
                                MessageType.Command.NACK, command);
                        }
                    }
                } else {
                    this.printState("Sending a NO-OP message for CLIENTPOST", "INFO",
                    this.details.getPrivateNodeName());
                    message = this.createMessageToSend(MessageType.Command.RAFT, MessageType.Raft.CLIENTPOST_REP, "");
                }
                break;
        }
        this.sendMessageToExternalClient(message, clientId);
    }
    
    private boolean isCandidateLogUptodate(int candidate_LogIndex, int candidate_LogTerm){
        /**
         * Raft determines which of the two logs is more up to date by comparing 
         * the index and term of the last entries in the logs.
         * 
         * If the logs have last entries with different terms, then the log with
         * the later term is more up-to-date. If the logs end with the same term,
         * then whichever log is longer is more up-to-date.
         */
         int lastIndex = this.log.getLastLogIndex();
         int lastTerm = this.log.getLastTerm();
         
         if (lastTerm > candidate_LogTerm){
            return false;
         } else if (lastTerm < candidate_LogTerm){
            return true;
         } else {
            return lastIndex <= candidate_LogIndex;
         }
    }
    
    private void save(Message message){
        String strToWrite = "Saving " + message.getPayloadType().name() + " to disk";
        this.printState(strToWrite, "INFO", this.details.getPrivateNodeName());
        this.fileThreadSocket.send(this.utils.convertToBytes(message), 0);
        String response = new String(this.fileThreadSocket.recv(0));
        while ("FAIL".equalsIgnoreCase(response)){
            this.fileThreadSocket.send(this.utils.convertToBytes(message), 0);
            response = new String(this.fileThreadSocket.recv(0));
        }
    }
    
    private boolean isLeaderKnown(){
        return this.leaderAddress != ""; 
    }
    
    public int getMajority(){
        this.majority = (this.clusterRaftNodes.size() / 2) + 1;    // majority is above 50%
        return this.majority;
    }
    
    public SystemState.RaftState getRaftNodeState(){
        return this.currentNodeState;
    }
    
    public boolean isALeader(){
        return this.currentNodeState == SystemState.RaftState.LEADER;
    }
    
    public boolean isAFollower(){
        return this.currentNodeState == SystemState.RaftState.FOLLOWER;
    }
    
    public boolean isACandidate(){
        return this.currentNodeState== SystemState.RaftState.CANDIDATE;
    }
    
    public String getLeaderId(){
        return this.leaderAddress;
    }
    
    private int getNumberOfVoters(){
        return this.voters.size();
    }
    
    /** Logging methods **/
    private void printState(String message, String type, String appname){
        String strToWrite = "[STATE at (" + appname +  ")]: " + message;
        System.out.println(strToWrite);
        switch(type){
            case "INFO":
                logger.log(Level.INFO, strToWrite);
                break;
            case "WARNING":
                logger.log(Level.WARNING, strToWrite);
                break;
            case "SEVERE":
                logger.log(Level.SEVERE, strToWrite);
                break;
            default:
                System.out.println("Cannot log!!!!");
                break;
        }
    }
    
    private void printElectionTimeouts(){
        String logStr = "Time at last election attempt=" + this.initialElectionTimestamp.getTime() + "ms";
        this.printState(logStr, "INFO", this.details.getPrivateNodeName());
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        long diff = ts.getTime() - this.initialElectionTimestamp.getTime(); 
        logStr = "Time between last and current election attempt=" + diff   + "ms";
        this.printState(logStr, "INFO", this.details.getPrivateNodeName());
        this.initialElectionTimestamp = ts;
    }
    
    private void printTimeReceived(Message message, MessageType.Raft subtype){
        Timestamp stamp = new Timestamp(System.currentTimeMillis());
        Timestamp messageCreatedTime = message.getTimeCreated();
        long diff = stamp.getTime() - messageCreatedTime.getTime();
        MessageType.Command maintype = message.getMessageType();
        String strToLog = "";
        if (subtype != null){
            strToLog = "Time taken for message of payloadtype " + subtype + " to arrive here is: "
                    + diff + "ms";
        } else {
            strToLog = "Time taken for message of " + maintype + " to arrive here is: "
                    + diff + "ms";
        }
        this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
    }
    
     private void processEject(String nodeId){
        Iterator<NodeDetails> it = this.clusterRaftNodes.iterator();
        while(it.hasNext()){
            NodeDetails item = it.next();
            if (item.getPublicNodeName().equalsIgnoreCase(nodeId)){
                String strToLog = "Found node to eject=" + item.getPrivateNodeName();
                this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                ZMQ.Socket socket = this.dealerSockets.get(item.getPrivateNodeName());
                socket.close();
                this.dealerSockets.remove(item.getPrivateNodeName());
                this.printState("Removing socket", "INFO", this.details.getPrivateNodeName());
                it.remove();
            }
        }
        this.printPeers();
    }
    
    
    private void printPeers(){
        String strToLog = "Current total num of raftNodes: " + this.clusterRaftNodes.size(); 
        this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
        this.printState("Nodes that are part of this cluster: ", "INFO", this.details.getPrivateNodeName());
        for (NodeDetails details: this.clusterRaftNodes){
            this.printState(details.toString(), "INFO", this.details.getPrivateNodeName());
        }
    }
    
    public static void startLogging(){
        try{
            APPLOGFILENAME = "logs/ClusterNode_" + HOSTNAME + ".log";
            handler = new FileHandler(APPLOGFILENAME, true);
        } catch ( SecurityException | IOException e ){
            e.printStackTrace();
        }
        
        System.out.println("===============================================");
        String strToWrite = "This is " + PRIVATENODENAME + " or " + "ClusterNode_" + HOSTNAME + "'s log";
        System.out.println(strToWrite);
        System.out.println("===============================================");
        handler.setFormatter(new SimpleFormatter());
        logger.setUseParentHandlers(false);
        logger.addHandler(handler);
        logger.setLevel(Level.INFO);
    }
    
    /** Logging methods **/
    
    @Override
    public void run(){
        Thread fileProcess = new Thread(this.fileThr);
        Thread threadProcess =  new Thread(this.timerThr);
        
        try{
            while (!Thread.currentThread().isInterrupted()){
                Message message;
                String strMessage, strToLog;
                
                MessageType.Command type;
                
                this.poller.poll();
                
                // the management socket
                if (this.poller.pollin(0)){
                    message = (Message) this.utils.convertFromBytes(this.manServerSocket.recv(0));
                    type = message.getMessageType();
                    switch(type){
                        case START:
                            if (this.currentClusterState == SystemState.ClusterState.STARTED){
                                this.clusterRaftNodes = (ArrayList<NodeDetails>) message.getPayloadPart();
                                this.setUpDealerSockets();
                                this.printPeers();
                                threadProcess.start();
                                fileProcess.start();
                                this.printState(
                                    "Starting First Election TimeOut", 
                                    "INFO",
                                    this.details.getPrivateNodeName()
                                );
                                this.initialElectionTimestamp = new Timestamp(System.currentTimeMillis());
                                this.timerThreadSocket.send(
                                    this.utils.convertToBytes(
                                        new Message(MessageType.Command.TIME_OUT)
                                    ),
                                    0
                                );
                                this.currentClusterState = SystemState.ClusterState.RUNNING;
                                this.printState(
                                    "Changing ClusterNodeState from STARTED to UP",
                                    "INFO",
                                    this.details.getPrivateNodeName()
                                );
                            } else {
                                this.printState(
                                    "This node is already started",
                                    "WARNING",
                                    this.details.getPrivateNodeName()
                                );
                            }
                            break;
                        case ADDSERVER:
                            NodeDetails dets = (NodeDetails) message.getPayloadPart();
                            this.clusterRaftNodes.add(dets);
                            this.printPeers();
                            ZMQ.Socket newSocket = this.context.socket(ZMQ.DEALER);
                            String newSocketname = UUID.randomUUID().toString();
                            strToLog = "This is the dealer socket identity=" +
                                newSocketname + " on this node that is bound to the router socket of node=(" +
                                dets.getPrivateNodeName() + ")";
                            this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                            newSocket.setIdentity(newSocketname.getBytes());
                            newSocket.connect(dets.getIPAddress());
                            this.dealerSockets.put(dets.getPrivateNodeName(), newSocket);
                            this.printState("Added new server: " + dets.getPrivateNodeName(), "INFO", this.details.getPrivateNodeName());
                            break;
                        case QUIT:
                        case STOP:
                            // Quitting 
                            this.shutDownSocket.send(
                                this.utils.convertToBytes(
                                    this.createMessageToSend(MessageType.Command.HEARTBEAT, 
                                        null, PUBLICNODENAME)
                                ), 0
                            );
                            this.printState( 
                                "Sent QUIT message ON [Node Shutdown Endpoint] TO (Management Server)", 
                                "INFO", 
                                this.details.getPrivateNodeName()
                            );
                            message = (Message) this.utils.convertFromBytes(this.shutDownSocket.recv(0));
                            strToLog = "MESSAGE RECEIVED: (" + message.getMessageType() + ") ON [Node Shutdown Endpoint] FROM (Management Server)";
                            this.printState(
                                strToLog, 
                                "INFO", 
                                this.details.getPrivateNodeName()
                            );
                            if ("QUIT".equalsIgnoreCase(message.getMessageType().name())){
                                this.timerThreadSocket.send(
                                    this.utils.convertToBytes(
                                        this.createMessageToSend(MessageType.Command.QUIT, null)
                                    ), 0
                                );
                                this.printState(
                                    "Sending QUIT to timerThread", 
                                    "INFO", 
                                    this.details.getPrivateNodeName()
                                );
                                this.fileThreadSocket.send(
                                    this.utils.convertToBytes(
                                        this.createMessageToSend(MessageType.Command.QUIT, 
                                            MessageType.Raft.STOP)
                                    ), 0
                                );
                                this.printState(
                                    "Sending QUIT to fileThread", 
                                    "INFO", 
                                    this.details.getPrivateNodeName()
                                );
                                Thread.currentThread().interrupt();
                            }
                            break;
                        case EJECT:
                            String nodeIdToEject = (String) message.getPayloadPart();
                            this.processEject(nodeIdToEject);
                            strToLog = "Ejecting nodeId=" + nodeIdToEject + " from list of nodes";
                            this.printState(
                                strToLog, 
                                "INFO", 
                                this.details.getPrivateNodeName()
                            );
                            break;
                        case GETLOGS:
                            break;
                        default:
                            this.printState(
                                "This command is not recognised", 
                                "WARNING",
                                this.details.getPrivateNodeName()
                            );
                            break;
                    }
                }
                
                // the socket that subscribes to the management server
                if (this.poller.pollin(1)){
                    message = (Message) this.utils.convertFromBytes(this.heartBeatSubSocket.recv(0));
                    strToLog = "MESSAGE RECEIVED: (" + (String) message.getPayloadPart() + ") ON [Subs Socket] FROM (Management server)";
                    this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                    
                    // Communicating with the management server
                    
                    this.heartBeatRepSocket.send(
                        this.utils.convertToBytes(
                             this.createMessageToSend(MessageType.Command.HEARTBEAT, 
                                null, PUBLICNODENAME)
                        ), 0
                    );
                    this.printState("MESSAGE SENT: (" + PUBLICNODENAME + ") ON [Node Rep Endpoint] TO (Management Server)", 
                            "INFO", this.details.getPrivateNodeName());
                    message = (Message) this.utils.convertFromBytes(this.heartBeatRepSocket.recv(0));
                    strMessage = (String) message.getPayloadPart();
                    strToLog = "MESSAGE RECEIVED: (" + strMessage + ") ON [Node Rep Endpoint] FROM (Management Server)";
                    this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                    
                    if ("I have seen you".equalsIgnoreCase(strMessage) && this.currentClusterState == SystemState.ClusterState.DOWN){
                        this.manServerSocket.send(
                            this.utils.convertToBytes(
                                this.createMessageToSend(MessageType.Command.HEARTBEAT,
                                    null, this.details)
                            ),0
                        );
                        strToLog = "MESSAGE SENT: (" + 
                                this.details + 
                                ") ON [Node Public Endpoint] TO (Management Server)";
                        this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                        this.currentClusterState = SystemState.ClusterState.STARTED;
                        this.printState("Changing ClusterState from DOWN to STARTED", 
                            "INFO", this.details.getPrivateNodeName());
                    }
                }
                
                // the socket that receives messages from the other raft nodes
                if (this.poller.pollin(2)){
                    String socketId = new String(this.raftNodeRcvSocket.recv(0));
                    strToLog = "This is the socket id=" + socketId;
                    this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                    message = (Message) this.utils.convertFromBytes(this.raftNodeRcvSocket.recv(0));
                    //String nodeIdSent = (String) message.getLastPayloadPart();   // the raftnodeId of the sending node
                    String nodeIdSent = message.getNodeIDFrom();
                    MessageType.Raft raftype = message.getPayloadType();
                    strToLog = "Received a RAFT message of type = [" + raftype + "] from " + nodeIdSent;
                    this.printState(
                        strToLog,
                        "INFO",
                        this.details.getPrivateNodeName()
                    );
                    this.printTimeReceived(message, raftype);
                    switch(raftype){
                        case APPENDENTRIES:
                            this.processAppendEntries(message, nodeIdSent);
                            break;
                        case APPENDENTRIES_REP:
                            this.processAppendEntriesRep(message, nodeIdSent);
                            break;
                        case REQUESTVOTE:
                            this.processRequestVote(message, nodeIdSent);
                            break;
                        case SETLEADERID:
                            this.leaderAddress = (String) message.getPayloadPart();
                            break;
                        //case SETLEADERID_REP:
                        //    this.processSetLeaderId(message, nodeId);
                        //    break;
                        case REQUESTVOTE_REP:
                            this.processRequestVoteRep(message, nodeIdSent);
                            // the boolean value allows for processing of unwanted 
                            // REQUESTVOTE_REP messages
                            if (this.isALeader() && this.hasStartedAppendEntries == false){
                                this.printState("Sending the first heartbeat", "INFO",
                                    this.details.getPrivateNodeName());
                                this.timerThreadSocket.send(this.utils.convertToBytes(
                                    new Message(MessageType.Command.HEARTBEAT)
                                    ), 0
                                );
                                this.hasStartedAppendEntries = true;
                            }
                            break;
                        case CLIENTPOST:
                            this.processClientPost(message, nodeIdSent);
                            break;
                        case CLIENTGET:
                            this.processClientGet(message, nodeIdSent);
                            break;
                        default:
                            this.printState(
                                "This command is not recognised", 
                                "WARNING",
                                this.details.getPrivateNodeName()
                            );
                            break;
                    }
                    strToLog = "Processing end of a RAFT message of type=[" + raftype + "]";
                    this.printState(
                        strToLog,
                        "INFO",
                        this.details.getPrivateNodeName()
                    );
                }
                
                
                  // the socket that sends messages to the timer server
                /**
                 *  I had initially wanted to use a TimerTask but for synchronisation issues.
                 *  The algorithm goes thusly:
                 *
                 *  The raft thread starts the timer thread and sends a TIME_OUT message
                 *  - The timerthread receives the TIME_OUT message, 
                 *      - It delays running an electionTimerTask for the electionTimeOut
                 *      - At the end of the delay, it run the scheduled electionTimer task.
                 *      - The electionTimerTask sends a TIME_OUT message to the raft thread
                 *  - When the raft thread gets the TIME_OUT message, it 
                 *      - checks to see if it does not know its leader. 
                 *      - if it does not know its leader(that is, it is also not the leader),
                 *          - it sends a message TIME_OUT back to the timerThread
                 *          - it changes its state to candidate if it is a follower
                 *      - if it does know its leader,
                 *          - it waits for a time to pass (represented by incrementing a counter)
                 *          - when this time has passed, it loses its memory of its leader
                 *  - When the raft thread gets the HEARTBEAT message, it
                 *      - checks to see if it is the leader
                 *      - if it is, it dispatches APPENDENTRIES to every node in the cluster
                 *      - if it is not the leader and it does not know its leader( indicating that
                 *      it has just transitioned from leader to follower), it sends a message 
                 *      TIME_OUT back to the timerThread
                 */
                
                
                if (this.poller.pollin(3)){
                    message = (Message) this.utils.convertFromBytes(this.timerThreadSocket.recv(0));
                    type = message.getMessageType();
                    
                    strToLog = "Received a message of type=[" + type + "]";
                    this.printState(
                        strToLog,
                        "INFO",
                        this.details.getPrivateNodeName()
                    );
                    this.printTimeReceived(message, null);
                    switch(type){
                        case TIME_OUT:
                            this.printElectionTimeouts();
                            if (this.isLeaderKnown() == false){
                                strToLog = "State of node =[" + this.getRaftNodeState() + "]";
                                this.printState(
                                    strToLog,
                                    "INFO",
                                    this.details.getPrivateNodeName()
                                );
                                this.timerThreadSocket.send(this.utils.convertToBytes(
                                    new Message(MessageType.Command.TIME_OUT)
                                    ), 0
                                );
                                if (this.isAFollower()){
                                    this.changeStateToCandidate();
                                }
                            } else if (this.isLeaderKnown() == true && this.isAFollower()){ 
                                this.waitCount++;
                                strToLog = "Current tick count = " + String.valueOf(waitCount);
                                this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                                this.printState("Waiting for leader to send message", "INFO", 
                                    this.details.getPrivateNodeName());
                                if (this.waitCount == WAITCOUNT){
                                    this.waitCount = 0;
                                    this.printState(
                                        "Resetting all state, Starting new Election",
                                        "INFO",
                                        this.details.getPrivateNodeName()
                                    );
                                    this.leaderAddress="";
                                }
                            }
                            break;
                        case HEARTBEAT:
                            if (this.isLeaderKnown() == true && this.isALeader()){
                                if (this.waitCount > 0){
                                    this.waitCount = 0;
                                }
                                this.printState(
                                    "Sending Heartbeats",
                                    "INFO",
                                    this.details.getPrivateNodeName()
                                );
                                this.broadCastAppendEntries();
                            } else if (this.isLeaderKnown() == false){
                                strToLog = "State of node=[" + this.getRaftNodeState() + "]";
                                this.timerThreadSocket.send(this.utils.convertToBytes(
                                    new Message(MessageType.Command.TIME_OUT)
                                    ), 0
                                );
                                this.changeStateToCandidate();
                            }
                            break;
                        default:
                            this.printState(
                                "This command is not recognised", 
                                "WARNING",
                                this.details.getPrivateNodeName()
                            );
                            break;
                    }
                    strToLog = "Processing end of message of type=[" + type + "]";
                    this.printState(
                        strToLog,
                        "INFO",
                        this.details.getPrivateNodeName()
                    );
                }
                
            }
            
            if (Thread.currentThread().isInterrupted()){
                this.printState("Thread is interrupted", "INFO", this.details.getPrivateNodeName());
                this.tearDownSockets();
                return;
            }
        } catch (Exception ie){
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ie.printStackTrace(pw);
            System.out.println(sw.toString());
        }
        
        //this.tearDownSockets(); 
    }
    
     public static void main(String [] args){
        startLogging();
        String docker_net_alias = System.getenv("NETALIAS");
        Peer peer = new Peer(docker_net_alias);
        peer.start();
    }
}
