/**
 *  Copyright 2017 Okusanya Oluwadamilola
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
 
/**
 *  TimerThread class
 *  Class that contains the timer object
 *  - This initiates the election process in the raft thread by sending 
 *  timeouts and initiates the replication process by sending heartbeats
 */

import java.util.TimerTask;
import java.util.Timer;
import org.zeromq.ZMQ;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.SimpleFormatter;

public class TimerThread implements Runnable{

    // Timers
    private Timer electionTimer,               // election timeout timer
        heartbeartTimer;               // heartbeat timer (AppendEntries)
                  
    // Timeouts
    private final int heartbeatTimeOut = 10000;  // 1s
    private final int electionMaxTimeOut = 6*60*1000;   // 6mins 
    private final int electionMinTimeOut = 1*60*1000;   // 1mins
    
    private int electionTimeOut;
    
    private boolean stopped = false;
    private ZMQ.Socket socket;
    
    private final Utils utils = new Utils();
                
    public final static Logger logger = Logger.getLogger(TimerThread.class.getName());
    public static FileHandler handler;
    private static String LOGFILENAME;
    public static String LOGFILENAMEID;
    
    private final String nodeToSendTo;
    
    TimerThread(ZMQ.Context context, String FILEID, String nodeToSendTo){
        this.socket = context.socket(ZMQ.PAIR);
        this.socket.connect(Constants.TIMERSERVER_PORT);
        this.socket.setLinger(0);
        this.LOGFILENAMEID = FILEID;
        this.nodeToSendTo = nodeToSendTo;
        this.electionTimeOut = 1;
        //this.generateNewTimeOut();
    }
    
    public static void startLogging(){
        try{
            LOGFILENAME = "logs/TimeThread_" + LOGFILENAMEID + ".log";
            handler = new FileHandler(LOGFILENAME, false);
        } catch ( SecurityException | IOException e ){
            e.printStackTrace();
        }
        
        handler.setFormatter(new SimpleFormatter());
        logger.setUseParentHandlers(false);
        logger.addHandler(handler);
        logger.setLevel(Level.INFO);
    }
    
    private void generateNewTimeOut(){
        this.electionTimeOut = ThreadLocalRandom.current().nextInt(this.electionMinTimeOut, 
                this.electionMaxTimeOut
        );
        String str = "[TimerThread]: ElectionTimeout (" + this.electionTimeOut + ")";
        System.out.println(str);
        logger.log(Level.INFO, str);
    }
    
    public void sendMessageToNode(String type){
        switch(type){
            case "TIMEOUT":
                this.socket.send(this.utils.convertToBytes(
                    new Message(MessageType.Command.TIME_OUT)
                ), 0);
                break;
            case "HEARTBEAT":
                this.socket.send(this.utils.convertToBytes(
                    new Message(MessageType.Command.HEARTBEAT)
                ), 0);
                break;
        }
        String str = "[TimerThread]: Sent (" + type + ") to " + this.nodeToSendTo;
        System.out.println(str);
        logger.log(Level.INFO, str);
    }
    
    private void startSendingTimeOuts(){
        if (this.electionTimer == null){
            TimerTask timerTask = new TimerTask() {
                @Override
                public void run(){
                    sendMessageToNode("TIMEOUT");
                }
            };
            this.electionTimer = new Timer();
            this.electionTimer.scheduleAtFixedRate(timerTask, this.electionTimeOut, this.electionTimeOut);
        }   
    }
    
    private void startSendingHeartbeats(){
        if (this.heartbeartTimer == null){
            TimerTask timerTask = new TimerTask() {
                @Override
                public void run(){
                    sendMessageToNode("HEARTBEAT");
                }
            };
            this.heartbeartTimer = new Timer();
            this.heartbeartTimer.scheduleAtFixedRate(timerTask, 0, this.heartbeatTimeOut);
        }   
    }
    
    private void cancelElectionTimer(){
        if (heartbeartTimer != null){
            heartbeartTimer.cancel();
            heartbeartTimer.purge();
            heartbeartTimer = null;
        }
    }
    
    private void cancelHeartBeatTimer(){
        if (electionTimer != null){
            electionTimer.cancel();
            electionTimer.purge();
            electionTimer = null;
        }
    }
    
    private void resetElectionTimer(){
        this.cancelHeartBeatTimer(); // in case the node becomes a leader;
        this.cancelElectionTimer();
        this.generateNewTimeOut();
    }
    
    private void tearDown(){
        this.stopWorker();
        this.cancelElectionTimer();
        this.cancelHeartBeatTimer();
    }
    
    /** The algorithm is as follows
     *  
     *  The timerthread receives a message.
     *  - If the message is a TIME_OUT, then the timerthread
     *  resets the heartbeatTimeOutTask and generates a new 
     *  electionTimeOut
     *  - If the message is a HEARTBEAT, then the timerthread
     *  resets the electionTimeOutTask.
     */
    
    
    @Override
    public void run(){
        startLogging();
        String str = "[TimerThread]: ElectionTimeout (" + this.electionTimeOut + ")";
        System.out.println(str);
        logger.log(Level.INFO, str);
        str = "[TimerThread]: HeartTimeOut (" + this.heartbeatTimeOut + ")";
        System.out.println(str);
        logger.log(Level.INFO, str);
        while (this.stopped == false){
            Message message = (Message) this.utils.convertFromBytes(this.socket.recv(0));
            if (message != null){
                MessageType.Command type = message.getMessageType();
                switch(type){
                    case TIME_OUT:
                        this.resetElectionTimer();
                        this.startSendingTimeOuts();
                        break;
                    case HEARTBEAT:
                        this.cancelHeartBeatTimer();
                        this.startSendingHeartbeats();
                        break;
                    case QUIT:
                        this.tearDown();
                        System.out.println("Closing my socket");
                        this.socket.close();
                        Thread.currentThread().interrupt();
                        break;
                }
            }
        }
        
        if (this.stopped == true){
            this.socket.close();
            Thread.currentThread().interrupt();
        }
    }
    
    public void stopWorker(){
        this.stopped = true;
    }
}
