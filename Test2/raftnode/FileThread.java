/**
 *  Copyright 2017 Okusanya Oluwadamilola
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import java.util.HashMap;
import java.util.ArrayList;
import java.util.Map;
import java.util.Collections;
import java.util.ArrayDeque; 
import java.util.Arrays; 
import org.zeromq.ZMQ;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;
import java.util.List;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.io.IOException; 

/**
 *  FileThread object for the raft substrate of the
 *  FittChart application
 *  
 *  It saves the persistent state associated with peer as per the RAFT protocol 
 *  which are the node's currentTerm, the node's votedFor, the node's log
 */

public class FileThread implements Runnable{
    public String raftLogFile,      // the file to represent the log 
                stateFile;          // the file to represent all the other parts of the peer state
    private volatile boolean stopped;
    private ZMQ.Socket socket;
    private Utils utils;
    
    FileThread(ZMQ.Context context, String fileID){
        this.socket = context.socket(ZMQ.PAIR);
        this.socket.connect(Constants.FILESERVER_PORT);
        this.socket.setLinger(0);
        this.raftLogFile = "logs/RaftLog_" + fileID + ".log";
        this.stateFile = "logs/PersistentState_" + fileID + ".log";
        this.utils = new Utils();
    }
    
    @Override
    public void run(){
        while(this.stopped == false){
        // If there is no stop, then there would be a Context terminated exception.
            Message messageRecv = (Message) this.utils.convertFromBytes(this.socket.recv(0));
            MessageType.Raft type = (MessageType.Raft) messageRecv.getPayloadType();
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yy HH:mm:ss:SSS");
            LocalDateTime localdate = LocalDateTime.now();
            String logDate = localdate.format(dateFormatter);
            Charset utf8 = StandardCharsets.UTF_8;
            String response = "", strs = "", toWrite = "";
            //List<String> lines;
            System.out.print("Writing ");
            try{
                switch(type){
                    case TERM:
                        String term = (String) messageRecv.getPayloadPart();
                        toWrite = "TERM: " + term + " at " + logDate;
                        strs = toWrite + System.lineSeparator();
                        System.out.print(strs);
                        Files.write(Paths.get(this.stateFile), strs.getBytes(StandardCharsets.UTF_8),
                            StandardOpenOption.CREATE, StandardOpenOption.APPEND);
                        response = "SUCCESS";
                        break;
                    case VOTEDFOR:
                        String votedFor = (String) messageRecv.getPayloadPart();
                        toWrite = "VOTEDFOR: " + votedFor + " at " + logDate;
                        strs = toWrite + System.lineSeparator();
                        System.out.print(strs);
                        //lines = Arrays.asList(toWrite);
                        Files.write(Paths.get(this.stateFile), strs.getBytes(StandardCharsets.UTF_8),
                            StandardOpenOption.CREATE, StandardOpenOption.APPEND);
                        response = "SUCCESS";
                        break;
                    case LOG:
                        System.out.println();
                        break;
                    case STOP:
                        this.stopWorker();
                        System.out.println("Closing my socket");
                        socket.close();
                        Thread.currentThread().interrupt();
                        break;
                    default:
                        response = "FAIL";
                        break;
                }
                if (this.stopped == true){
                    return;
                }
                this.socket.send(response.getBytes(), 0);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (this.stopped == true){
            socket.close();
            Thread.currentThread().interrupt();
        }
    }
    
    public void stopWorker(){
        this.stopped = true;
    }
}
