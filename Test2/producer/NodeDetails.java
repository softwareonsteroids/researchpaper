/**
 *  Copyright 2017 Okusanya Oluwadamilola
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
 
/**
 *  NodeDetails class
 *  This stores the node connection details 
 *  - Public address = for connection to the other nodes in the system
 *  - Private address =  for connection to the nodes in the Raft cluster
 */
 
import java.io.Serializable; 
 
public class NodeDetails implements Serializable{
    
    private final String ipAddress;
    private final String privateNodeName;
    private final String publicNodeName;
    private static final long serialVersionID = 6190482962565891534L;
    private final boolean isLeader;
    
    public String getIPAddress(){
        return this.ipAddress;
    }
    
    public String getPublicNodeName(){
        return this.publicNodeName;
    }
    
    public String getPrivateNodeName(){
        return this.privateNodeName;
    }
    
    public boolean isLeader(){
        return this.isLeader;
    }
    
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("IPAddress: (");
        sb.append(this.ipAddress);
        sb.append("),Public Node Name: (");
        sb.append(this.publicNodeName);
        sb.append("),Private Node Name: (");
        sb.append(this.privateNodeName);
        sb.append(")");
        return sb.toString();
    }
    
    private NodeDetails(NodeDetailsBuilder builder){
        this.privateNodeName = builder.privateNodeName;
        this.publicNodeName = builder.publicNodeName;
        this.isLeader = builder.isLeader;
        this.ipAddress = builder.ipAddress;
    }
    
    public static class NodeDetailsBuilder{
        private boolean isLeader;           // required;
        private String ipAddress, 
            privateNodeName,
            publicNodeName;
        
        public NodeDetailsBuilder(boolean isLeader){
            this.isLeader = isLeader;
        }
        
        public NodeDetailsBuilder setIPAddress(String ipAddress){
            this.ipAddress = ipAddress;
            return this;
        }
        
        public NodeDetailsBuilder setPublicNodeName(String publicNodeName){
            this.publicNodeName = publicNodeName;
            return this;
        }
        
        public NodeDetailsBuilder setPrivateNodeName(String privateNodeName){
            this.privateNodeName = privateNodeName;
            return this;
        }
        
        public NodeDetails build(){
            return new NodeDetails(this);
        }
    }
}
 
