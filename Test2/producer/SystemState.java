/**
 *  Copyright 2017 Okusanya Oluwadamilola
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
 
/**
 *  State class
 *  This enum has names for the various states in the cluster 
 */ 
 
public class SystemState{

    public static enum ClusterState{
        DOWN,       // initial state
        STARTED,    // state of the cluster that just started
        RUNNING     // state of the cluster that is running
    }
    
    public static enum RaftState{
        LEADER,     // final state
        FOLLOWER,   // initial state
        CANDIDATE   // intermediate state
    }
}
