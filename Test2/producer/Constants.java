/**
 *  Copyright 2017 Okusanya Oluwadamilola
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
 
/**
 *  Constants class
 *  Class that contains all the constants used in the system
 */ 

public class Constants{

    static final String MANAGEMENT_SERVER_COMMAND_PORT="tcp://managementserver:5600";
    static final String MANAGEMENT_SERVER_CLUSTER_PORT="tcp://*:5600";
    
    static final String MANAGEMENT_SERVER_SUB_PORT="tcp://managementserver:5006";
    static final String MANAGEMENT_SERVER_PUB_PORT="tcp://*:5006";
    
    static final String MANAGEMENT_SERVER_HEARTBEAT_PORT="tcp://managementserver:5007";
    static final String MANAGEMENT_SERVER_HEARTBEAT_RCV_PORT="tcp://*:5007";
    
    static final String MANAGEMENT_SERVER_SHUTDOWN_PORT="tcp://managementserver:5020";
    static final String MANAGEMENT_SERVER_SHUTDOWN_RCV_PORT="tcp://*:5020";
    
    static final String MANAGEMENT_CLIENT_SERVER_PORT="tcp://managementserver:4010";
    static final String MANAGEMENT_SERVER_CLIENT_PORT="tcp://*:4010";
    
    static final String RAFTCLUSTER_RAFTNODE_RECV_PORT="tcp://*:6500";
    
    static final String FILESERVER_PORT="inproc://filethreadSocket";
    static final String TIMERSERVER_PORT="inproc://timerthreadSocket";
    static final String LONGRUNNING_SENDER_PORT="inproc://longrunningsenderSocket";
}
