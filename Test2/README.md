# My research paper project's README 

### To run:

> docker-compose build 

> docker network create cluster_network

> > (Run the command below in a new tab)

> docker run --network-alias managementserver --network cluster_network -e NOOFNODES='3' -it managementserver

> > (Run the command below in a new tab)

> docker run --network-alias raftnode1 --network cluster_network -e NETALIAS="raftnode1" -it raftnode

> > (Run the command below in a new tab)

> docker run --network-alias raftnode2 --network cluster_network -e NETALIAS="raftnode2" -it raftnode

> > (Run the command below in a new tab)

> docker run --network-alias raftnode3 --network cluster_network -e NETALIAS="raftnode3" -it raftnode

> > (Run the command below in a new tab)

> docker run --network cluster_network -it managementclient

> > (Run the command below in a new tab)

> docker run --network cluster_network -it producer

> > (Run the command below in a new tab)

> docker run --network cluster_network -it consumer

Timeline

| Action                           | Partial | Full |
| -------------------------------- | ------- | ---- |
| Implementing a homegrown cluster |   [x]   |  [ ] | 
| Testing the homegrown cluster    |   [x]   |  [ ] |
| Implementing the RAFT process    |   [x]   |  [ ] |
| Testing the RAFT process         |   [x]   |  [ ] |