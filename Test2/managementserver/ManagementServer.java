/**
 *  Copyright 2017 Okusanya Oluwadamilola
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
 
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Collection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.io.IOException;
import org.zeromq.ZMQ; 
 
 
/**
 *  ManagementServer class
 *  Class that contains the managementserver object
 *  - This monitors the entire system
 */ 
 
/** 
class PairK>{
    
    private T first;
    private K second;
    
    public Pair(T first, K second){
        this.first = first;
        this.second = second;
    }
    
    public T getFirst(){
        return first;
    }
    
    public K getSecond(){
        return second;
    }
}
**/

public class ManagementServer extends Thread{
    
    private ArrayList<NodeDetails> clusterOfNodes;
    private HashSet<NodeDetails> clientNodes;
    private SystemState.ClusterState clusterState;
    private String leaderId;
    
     // Sockets
    private ZMQ.Socket manClientSocket,         // management client socket
                    heartBeatRcvSocket,         // socket to respond to heartbeats 
                    clusterSocket,              // socket to talk to the cluster nodes
                    shutDownSocket;             // socket to shutdown the cluster nodes
    private ZMQ.Poller poller;
    private ZMQ.Context context= ZMQ.context(1);
    
    // Raft Nodes
    private int noofReqNodes,                   // required number of nodes in the cluster
                noOfAvailRaftNodes,                 // number of available raft nodes in the cluster
                noOfAvailClusterNodes,              // number of available nodes in the cluster
                numOfLogsToSend;                // number of log messages to send back
    
    // Application logs to send back to the management client
    private ArrayList<String> applogs;
    
    // Logging
    private final static String LOGFILENAME = "logs/ManagementServer.log";
    private final static Logger logger = Logger.getLogger(ManagementServer.class.getName());
    private static FileHandler handler = null;
    
    private HeartbeatWorker worker;
    
    private Utils utils;
    
    ManagementServer(int num){
        this.noofReqNodes = num;
        this.noOfAvailRaftNodes = 0;
        this.noOfAvailClusterNodes = 0;
        this.numOfLogsToSend = 0;
        this.clusterState = SystemState.ClusterState.DOWN;
        this.clusterOfNodes = new ArrayList<>();
        this.clientNodes = new HashSet<>();
        this.utils = new Utils();
        this.setUpSockets();
    }
    
    public static void startLogging(){
        try{
            handler = new FileHandler(LOGFILENAME, true);
        } catch ( SecurityException | IOException e){
            e.printStackTrace();
        }
        
        handler.setFormatter(new SimpleFormatter()); 
        logger.setUseParentHandlers(false);        // disable logging to the console
        logger.addHandler(handler);
        logger.setLevel(Level.INFO);
    }
    
    private void setUpSockets(){
        this.manClientSocket = this.context.socket(ZMQ.REP);
        this.manClientSocket.bind(Constants.MANAGEMENT_SERVER_CLIENT_PORT);
        this.heartBeatRcvSocket = this.context.socket(ZMQ.REP);
        this.heartBeatRcvSocket.bind(Constants.MANAGEMENT_SERVER_HEARTBEAT_RCV_PORT);
        this.clusterSocket = this.context.socket(ZMQ.ROUTER);
        this.clusterSocket.bind(Constants.MANAGEMENT_SERVER_CLUSTER_PORT);
        this.shutDownSocket = this.context.socket(ZMQ.REP);
        this.shutDownSocket.bind(Constants.MANAGEMENT_SERVER_SHUTDOWN_RCV_PORT);
        
        this.poller = new ZMQ.Poller(4);
        this.poller.register(this.clusterSocket, ZMQ.Poller.POLLIN);
        this.poller.register(this.manClientSocket, ZMQ.Poller.POLLIN);
        this.poller.register(this.heartBeatRcvSocket, ZMQ.Poller.POLLIN);
        this.poller.register(this.shutDownSocket, ZMQ.Poller.POLLIN);
    }
    
    private void tearDownSockets(){
        this.manClientSocket.close();
        this.heartBeatRcvSocket.close();
        this.clusterSocket.close();
        this.shutDownSocket.close();
        this.context.term();
    }
    
    private void printNodes(){
        String strToLog = "Current total num of raftNodes: " + this.noOfAvailRaftNodes; 
        this.printState(strToLog, "INFO");
        for (NodeDetails details: this.clusterOfNodes){
            this.printState(details.toString(), "INFO");
        }
    }
    /**
    private PairBoolean, Integer> canFind(String nodename){
        int count = 0;
        for ( NodeDetails item : this.clusterOfNodes){
            System.out.println(item);
            if( item.getPublicNodeName().equalsIgnoreCase(nodename)){
                this.printState("Found the node to delete in clusterOfNodes", "INFO");
                return new Pair(true,count) ;
            }
            ++count;
        }
        return null;
    }**/
    
    private void removeClusterNode(String nodename){
        Iterator<NodeDetails> it = this.clusterOfNodes.iterator();
        while(it.hasNext()){
            NodeDetails item = it.next();
            if (item.getPublicNodeName().equalsIgnoreCase(nodename)){
                String strToLog = "Found node to delete in clusterOfNodes=" + item.getPrivateNodeName();
                this.printState(strToLog, "INFO");
                it.remove();
                this.printState("Removed node", "INFO");
                break;
            }
        }
        this.printNodes();
    }
    
    
    private void printState(String message, String type){
        String strToWrite = "[STATE at ManagementServer ]: " + message;
        System.out.println(strToWrite);
        switch(type){
            case "INFO":
                logger.log(Level.INFO, strToWrite);
                break;
            case "WARNING":
                logger.log(Level.WARNING, strToWrite);
                break;
            case "SEVERE":
                logger.log(Level.SEVERE, strToWrite);
                break;
            default:
                System.out.println("Cannot log!!!!");
                break;
        }
    }
    
    public Message createMessageToSend(MessageType.Command messageType, 
            MessageType.Raft raftype, Object ... args){
        Message messageToSend = new Message(messageType);
        if (raftype != null){
            messageToSend.setPayloadType(raftype);
        }
        if (args != null){
            for (Object arg: args){
                messageToSend.addPayloadPart(arg);
            }
        }
        return messageToSend;
    }
    
    @Override
    public void run(){
        this.worker = new HeartbeatWorker(this.context);
        Thread heartBeatthr = new Thread(this.worker);
 
        try{
            while(!Thread.currentThread().isInterrupted()){
                Message message;
                String strMessage, strToLog, nodeId;
                MessageType.Command type;
                
                this.poller.poll();
                
                // this socket communicates with all the nodes in the network except the man client
                if (this.poller.pollin(0)){
                    nodeId = new String(this.clusterSocket.recv(0));
                    message = (Message) this.utils.convertFromBytes(this.clusterSocket.recv(0));
                    type = message.getMessageType();
                    strToLog = "[ClusterSocket]: Processing(start) message of type=" + type.name();
                    this.printState(strToLog, "INFO");
                    switch(type){
                        case HEARTBEAT:
                            strToLog = "Received new raft node Details";
                            this.printState(strToLog, "INFO");
                            NodeDetails dets = (NodeDetails)message.getPayloadPart();
                            if (dets.getPrivateNodeName().contains("Raft")){
                                this.clusterOfNodes.add(dets);
                            } else {
                                this.clientNodes.add(dets);
                            }
                            this.noOfAvailClusterNodes++;
                            if (this.noOfAvailRaftNodes < this.noofReqNodes && 
                                dets.getPrivateNodeName().contains("Raft")){
                                this.noOfAvailRaftNodes++;
                                strToLog = "Added new raft node=" + dets.getPrivateNodeName();
                                this.printState(strToLog, "INFO");
                                this.printNodes();
                                if (this.noOfAvailRaftNodes == this.noofReqNodes && 
                                    this.clusterState == SystemState.ClusterState.STARTED){
                                    for (NodeDetails item: this.clusterOfNodes){
                                        this.clusterSocket.send(
                                            item.getPublicNodeName().getBytes(), 
                                            ZMQ.SNDMORE
                                        );
                                        this.clusterState = SystemState.ClusterState.RUNNING;
                                        this.clusterSocket.send( 
                                            this.utils.convertToBytes(
                                                this.createMessageToSend(MessageType.Command.START, 
                                                    null, clusterOfNodes)
                                            ), 
                                            0
                                        );
                                        strToLog = "Just sent list of nodes to " + 
                                            item.getPrivateNodeName();
                                        this.printState(strToLog, "INFO");
                                    }
                                }
                            } else {
                                if (this.noOfAvailRaftNodes >= this.noofReqNodes){  
                                    if (dets.getPrivateNodeName().contains("Raft") == false){
                                        for (NodeDetails item: this.clientNodes){
                                            strToLog = "Sending cluster list to client node=" + 
                                                item.getPublicNodeName();
                                            this.printState(strToLog, "INFO");
                                            this.clusterSocket.send(
                                                item.getPublicNodeName().getBytes(), 
                                                ZMQ.SNDMORE
                                            );
                                            this.clusterSocket.send( 
                                                this.utils.convertToBytes(
                                                    this.createMessageToSend(MessageType.Command.START, 
                                                        null, clusterOfNodes)
                                                ), 0
                                            );
                                            strToLog = "Just sent list of nodes to " + 
                                                item.getPublicNodeName();
                                            this.printState(strToLog, "INFO");
                                        }
                                    } else{
                                        strToLog = "Sending cluster list to raft node=" + 
                                                dets.getPrivateNodeName();
                                        this.printState(strToLog, "INFO");
                                        this.clusterSocket.send(
                                            dets.getPublicNodeName().getBytes(), 
                                            ZMQ.SNDMORE
                                        );
                                        this.clusterSocket.send( 
                                            this.utils.convertToBytes(
                                                this.createMessageToSend(MessageType.Command.START, 
                                                    null, clusterOfNodes)
                                            ), 0
                                        );
                                        strToLog = "Just sent list of nodes to " + 
                                            dets.getPublicNodeName();
                                        for (NodeDetails item: this.clusterOfNodes){
                                            if (!item.getPrivateNodeName().equalsIgnoreCase(dets.getPrivateNodeName())){
                                                this.clusterSocket.send(
                                                    item.getPublicNodeName().getBytes(), 
                                                    ZMQ.SNDMORE
                                                );
                                                this.clusterSocket.send( 
                                                    this.utils.convertToBytes(
                                                        this.createMessageToSend(MessageType.Command.ADDSERVER, 
                                                            null, dets)
                                                    ), 0
                                                );
                                            }
                                        }   
                                        this.printState(strToLog, "INFO");
                                    }
                                }
                            }
                            break;
                        case SETLEADER:
                            this.leaderId = (String) message.getPayloadPart();
                            strToLog = "Setting the leader Address=" + this.leaderId;
                            this.printState(strToLog, "INFO");
                            break;
                        default:
                            this.printState("This command is not recognised","WARNING");
                            break;
                    }
                    strToLog = "[ClusterSocket]: Processing(end) message of type=" + type.name();
                    this.printState(strToLog, "INFO");
                }
                
                // this.socket communicates with only the man client
                if (this.poller.pollin(1)){
                    message = (Message) this.utils.convertFromBytes(this.manClientSocket.recv(0));
                    type = message.getMessageType();
                    strToLog = "[ClusterSocket]: Processing(start) message of type=" + type.name();
                    this.printState(strToLog, "INFO");
                    switch(type){
                        case START:
                            if (this.clusterState == SystemState.ClusterState.DOWN){
                                heartBeatthr.start();
                                this.clusterState = SystemState.ClusterState.STARTED;
                                this.printState("Changing ClusterState from DOWN to STARTED", "INFO");
                                this.manClientSocket.send(
                                    this.utils.convertToBytes(
                                        this.createMessageToSend(MessageType.Command.ACK, 
                                            null, "Just started the cluster")
                                    ),0
                                );
                                this.printState("Started sending heartbeats to all nodes", "INFO");
                            }
                            break;
                        case QUIT:
                            for (NodeDetails item: this.clusterOfNodes){
                                this.clusterSocket.send(item.getPublicNodeName().getBytes(), ZMQ.SNDMORE);
                                this.clusterSocket.send(
                                    this.utils.convertToBytes(new Message(MessageType.Command.QUIT))
                                    ,0
                                );
                            }
                            this.manClientSocket.send(
                                this.utils.convertToBytes(
                                    this.createMessageToSend(MessageType.Command.ACK, 
                                            null, "Just sent QUIT to all the nodes")
                                ),0
                            );
                            this.printState("Started sending QUIT to all nodes", "INFO");
                            break;
                        case GETLOGS:
                            break;
                        case GETLEADERID:
                            strToLog = "This is the leaderId=" + this.leaderId;
                            this.manClientSocket.send(
                                this.utils.convertToBytes(
                                    this.createMessageToSend(MessageType.Command.ACK, 
                                            null, strToLog)
                                ),0
                            );
                            break;
                        case GETALLIDS:
                            if (this.noOfAvailRaftNodes == this.noofReqNodes){
                                StringBuilder sb = new StringBuilder();
                                int count = 1;
                                for (NodeDetails item: this.clusterOfNodes){
                                    sb.append("[Node " + String.valueOf(count) + ":");
                                    sb.append(item.getPublicNodeName());
                                    sb.append("] ");
                                    ++count;
                                }
                                this.manClientSocket.send(
                                    this.utils.convertToBytes(
                                        this.createMessageToSend(MessageType.Command.ACK, 
                                            null, sb.toString())
                                    ), 0
                                );
                            } else {
                                this.manClientSocket.send(
                                    this.utils.convertToBytes(
                                        this.createMessageToSend(MessageType.Command.ACK, 
                                            null, "Not Ready")
                                    ), 0
                                );
                            }
                            break;
                        case STOP:
                            String nodeToSendTo = (String) message.getPayloadPart();
                            this.clusterSocket.send(nodeToSendTo.getBytes(), ZMQ.SNDMORE);
                            this.clusterSocket.send(
                                this.utils.convertToBytes(new Message(MessageType.Command.STOP))
                                , 0
                            );
                            this.manClientSocket.send(
                                this.utils.convertToBytes(
                                    this.createMessageToSend(MessageType.Command.ACK,
                                            null, "Just sent STOP to a particular node")
                                ), 0
                            );
                            this.printState("Started sending STOP to " + nodeToSendTo, "INFO");
                            
                            // Send an EJECT message to every node in the cluster to remove the node 
                            // that has died from their lists 
                            for (NodeDetails item: this.clusterOfNodes){
                                if (!item.getPublicNodeName().equalsIgnoreCase(nodeToSendTo)){
                                    this.clusterSocket.send(item.getPublicNodeName().getBytes(), ZMQ.SNDMORE);
                                    this.clusterSocket.send(
                                        this.utils.convertToBytes(
                                            this.createMessageToSend(MessageType.Command.EJECT, 
                                                null, nodeToSendTo)
                                        ), 0
                                    );
                                    this.printState("Started sending EJECT to " + item.getPublicNodeName(), "INFO");
                                }
                            }
                            break;
                        default:
                            this.printState("This command is not recognised","WARNING");
                            break;
                    }
                    strToLog = "[ClusterSocket]: Processing(end) message of type=" + type.name();
                    this.printState(strToLog, "INFO");
                }
                
                // this socket receives replies to the heartbeats
                if (this.poller.pollin(2)){
                    message = (Message) this.utils.convertFromBytes(this.heartBeatRcvSocket.recv(0));
                    String nodename = (String) message.getPayloadPart();
                    strToLog = "MESSAGE RECEIVED: (" + message.getMessageType().name() + ") ON [Node Public Endpoint] FROM (" + nodename + ")";
                    this.printState(strToLog, "INFO");
                    this.heartBeatRcvSocket.send(
                        this.utils.convertToBytes(
                            this.createMessageToSend(MessageType.Command.HEARTBEAT, 
                                    null, "I have seen you")
                        ),0
                    );
                    strToLog = "MESSAGE SENT: (I have seen you) ON [Node Public Endpoint] TO (" + nodename + ")";
                    this.printState(strToLog, "INFO");
                }
                
                // this socket receives replies with the shutdown message
                if (this.poller.pollin(3)){
                    message = (Message) this.utils.convertFromBytes(this.shutDownSocket.recv(0));
                    strMessage = (String) message.getPayloadPart();
                    strToLog = "MESSAGE RECEIVED: (" + message.getMessageType() + ") ON [Node Shutdown Endpoint] FROM (" + strMessage + ")";
                    this.printState(strToLog, "INFO");
                    this.shutDownSocket.send(
                        this.utils.convertToBytes(
                            this.createMessageToSend(MessageType.Command.QUIT, 
                                null, "GOODBYE")
                        ), 0
                    );
                    strToLog = "MESSAGE SENT: (GOODBYE) ON [Node Public Endpoint] TO (" + strMessage + ")";
                    //Pair<Boolean, Integer> p = this.canFind(strMessage);
                    this.removeClusterNode(strMessage);
                    
                    if (this.noOfAvailClusterNodes == 0){
                        this.worker.stopHeartBeats();
                        this.clusterState = SystemState.ClusterState.DOWN;
                        this.printState("Changing ClusterState from RUNNING to NOTSTARTED", "INFO");
                        Thread.currentThread().interrupt();
                    }
                }
            }
            
            if (Thread.currentThread().isInterrupted()){
                this.printState("Closing sockets", "INFO");
                this.tearDownSockets();
                return;
            }
            
        } catch (Exception e){
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            System.out.println(sw.toString());
        }
        
        this.tearDownSockets();
    }
    
     public static void main(String [] args){
        startLogging();
        int subscribers = Integer.parseInt(System.getenv("NOOFNODES"));
        if (subscribers % 2 == 0) {
            subscribers++;
        }
        String str = "Waiting for " + subscribers + " subscribers...";
        System.out.println(str);
        ManagementServer server = new ManagementServer(subscribers);
        server.start();
    }
    
}
