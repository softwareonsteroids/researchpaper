# My research paper project's README 

### To run:

> docker-compose build 

> docker network create cluster_network

> > (Run the command below in a new tab)

> docker run --network-alias clusterman --network cluster_network -e SUBSCRIBERS='3' -it clusterman

> > (Run the command below in a new tab)

> docker run --network-alias clusternode1 --network cluster_network -it clusternode

> > (Run the command below in a new tab)

> docker run --network-alias clusternode2 --network cluster_network -it clusternode

> > (Run the command below in a new tab)

> docker run --network-alias clusternode3 --network cluster_network -it clusternode

> > (Run the command below in a new tab)

> docker run --network cluster_network -it commandclient

Timeline

| Action                           | Partial | Full |
| -------------------------------- | ------- | ---- |
| Implementing a homegrown cluster |   [ ]   |  [ ] | 
| Testing the homegrown cluster    |   [x]   |  [ ] |
| Implementing the RAFT process    |   [x]   |  [ ] |
| Testing the RAFT process         |   [ ]   |  [ ] |