/**
 *  Copyright 2017 Okusanya Oluwadamilola
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import java.util.HashMap;
import java.util.ArrayList;
import java.util.Map;
import java.util.Collections;
import java.util.ArrayDeque; 
import java.util.Arrays; 
import org.zeromq.ZMQ;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.io.IOException; 

/**
 *  FileThread object for the raft substrate of the
 *  FittChart application
 *  
 *  It saves the persistent state associated with peer as per the RAFT protocol 
 *  which are the node's currentTerm, the node's votedFor, the node's log[]
 */

public class FileThread implements Runnable{
    public String raftLogFile,      // the file to represent the log 
                stateFile;          // the file to represent all the other parts of the peer state
    private volatile boolean stopped;
    private ZMQ.Socket socket;
    private Utils utils;
    
    FileThread(ZMQ.Context context, String address, int fileID){
        this.socket = context.socket(ZMQ.PAIR);
        this.socket.connect(address);
        this.raftLogFile = "RaftLog_" + fileID + ".log";
        this.stateFile = "PersistentState_" + fileID + ".log";
        this.utils = new Utils();
    }
    
    @Override
    public void run(){
        while(stopped == false){
        // If there is no stop, then there would be a Context terminated exception.
            ArrayDeque messageRecv = (ArrayDeque) this.utils.convertFromBytes(this.socket.recv(0));
            RaftEnums.PersistentStateType type = (RaftEnums.PersistentStateType) messageRecv.poll();
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
            Date today = Calendar.getInstance().getTime();
            String logDate = dateFormat.format(today);
            Charset utf8 = StandardCharsets.UTF_8;
            String response = "", strs = "", toWrite = "";
            List<String> lines;
            System.out.print("Writing ");
            try{
                switch(type){
                    case TERM:
                        String term = (String) messageRecv.poll();
                        toWrite = "TERM: " + term + "at" + logDate;
                        strs = toWrite + "\n";
                        System.out.print(strs);
                        lines = Arrays.asList(toWrite);
                        Files.write(Paths.get(this.stateFile), lines, utf8,
                            StandardOpenOption.CREATE, StandardOpenOption.APPEND);
                        response = "SUCCESS";
                        break;
                    case VOTEDFOR:
                        String votedFor = (String) messageRecv.poll();
                        toWrite = "VOTEDFOR: " + votedFor + "at" + logDate;
                        strs = toWrite + "\n";
                        System.out.print(strs);
                        lines = Arrays.asList(toWrite);
                        Files.write(Paths.get(this.stateFile), lines, utf8,
                            StandardOpenOption.CREATE, StandardOpenOption.APPEND);
                        response = "SUCCESS";
                        break;
                    case LOG:
                        System.out.println();
                        break;
                    default:
                        response = "FAIL";
                        break;
                }
                this.socket.send(response.getBytes(), 0);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (stopped == true){
            socket.close();
        }
    }
    
    public void stopWorker(){
        stopped = true;
    }
}