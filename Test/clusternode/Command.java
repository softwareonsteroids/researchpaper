/**
 *  Copyright 2017 Okusanya Oluwadamilola
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import java.io.Serializable;
 
public class Command implements Serializable{

    private final RaftEnums.CommandType command;                 // required
    private final String nodeId;
    private final int numOfLogLinesToRetrieve;
    private final int timeOut;
    private static final long serialVersionID = 6190482962565891534L;
    
    public RaftEnums.CommandType getCommand(){
        return this.command;
    }
    
    public String getNodeId(){
        return this.nodeId;
    }
    
    public int getNumOfLinesToRetrieve(){
        return this.numOfLogLinesToRetrieve;
    }
    
    public int getTimeOut(){
        return this.timeOut;
    }
    
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("RaftEnums.CommandType:");
        sb.append(this.command.name());
        sb.append(" NodeId:");
        sb.append(this.nodeId);
        sb.append(" NumOfLogLines:");
        sb.append(String.valueOf(this.numOfLogLinesToRetrieve));
        sb.append(" TimeOut:");
        sb.append(String.valueOf(this.timeOut));
        return sb.toString();
    }
    
    private Command(CommandBuilder builder){
        this.command = builder.command;
        this.nodeId = builder.nodeId;
        this.numOfLogLinesToRetrieve = builder.numOfLogLinesToRetrieve;
        this.timeOut = builder.timeOut;
    }
    
    public static class CommandBuilder{
        private RaftEnums.CommandType command;             // required
        private String nodeId;
        private int numOfLogLinesToRetrieve;
        private int timeOut;
        
        public CommandBuilder(String command){
            switch(command){
                case "START":       this.command = RaftEnums.CommandType.START;
                                    break;
                case "QUIT":        this.command = RaftEnums.CommandType.QUIT;
                                    break;
                case "PAUSE":       this.command = RaftEnums.CommandType.PAUSE;
                                    break;
                case "GETALLIDS":   this.command = RaftEnums.CommandType.GETALLIDS;
                                    break;
                case "GETLEADERID": this.command = RaftEnums.CommandType.GETLEADERID;
                                    break;
                case "GETLOGS":     this.command = RaftEnums.CommandType.GETLOGS;
                                    break;
                default:            break;
            }
        }
        
        public CommandBuilder receipient(String nodeId){
            this.nodeId = nodeId;
            return this;
        }
        
        public CommandBuilder numOfLogLinesToRetr(int numOfLogLinesToRetrieve){
            this.numOfLogLinesToRetrieve = numOfLogLinesToRetrieve;
            return this;
        }
        
        public CommandBuilder timeOut(int timeOut){
            this.timeOut = timeOut;
            return this;
        }
        
        public Command build(){
            return new Command(this);
        } 
    }
}


