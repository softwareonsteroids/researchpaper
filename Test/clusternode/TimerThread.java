/**
 *  Copyright 2017 Okusanya Oluwadamilola
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
 
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.io.IOException;
import org.zeromq.ZMQ;
 
/**
 *  TimerThread object for the raft substrate of the 
 *  FittChart application
 */


class TimerThread implements Runnable{
    
    private Timer electT,               // election timeout timer
                  heartT;               // heartbeat timer (AppendEntries)
                  
    private boolean stopped = false;
    private ZMQ.Socket socket;
    private int electionTimeOut,        // the election timeout in milliseconds
                heartTimeOut;           // the heart timeout in milliseconds
                
    public final static Logger logger = Logger.getLogger(TimerThread.class.getName());
    public static FileHandler handler;
    private static String LOGFILENAME;
    public static int LOGFILENAMEID;
    
    private final String nodeToSendTo;
    
    TimerThread (ZMQ.Context context, String address, 
                int electionTimeOut, int heartTimeOut, 
                final int FILEID, final String nodeToSendTo){
        this.socket = context.socket(ZMQ.PAIR);
        this.socket.connect(address);
        this.electionTimeOut = electionTimeOut;
        this.LOGFILENAMEID = FILEID;
        this.nodeToSendTo = nodeToSendTo;
        this.heartTimeOut = heartTimeOut;
    }
    
    public static void startLogging(){
        try{
            LOGFILENAME = "TimeThread_" + LOGFILENAMEID + ".log";
            handler = new FileHandler(LOGFILENAME, false);
        } catch ( SecurityException | IOException e ){
            e.printStackTrace();
        }
        
        handler.setFormatter(new SimpleFormatter());
        logger.setUseParentHandlers(false);
        logger.addHandler(handler);
        logger.setLevel(Level.INFO);
    }
    
    
    public void setElectionTimeOut(int timeout){
        this.electionTimeOut = timeout;
    }
    
    public void sendTimeOut(){
        this.socket.send("TIMEOUT".getBytes(), 0);
        String str = "[TimerThread]: Sent (TIMEOUT) to " + this.nodeToSendTo;
        System.out.println(str);
        logger.log(Level.INFO, str);
    }
    
    public void sendHeartBeat(){
        this.socket.send("HEARTBEAT".getBytes(), 0);
        String str = "[TimerThread]: Sent (Are you there?) to " + this.nodeToSendTo;
        System.out.println(str);
        logger.log(Level.INFO, str);
    }
    
    private void startElectionTimerTask(){
        if (this.electT == null){
            TimerTask timerTask = new TimerTask() {
                @Override
                public void run(){
                    sendTimeOut();
                }
            };
            this.electT = new Timer();
            this.electT.schedule(timerTask, 0, this.electionTimeOut);
        } 
    }
    
    private void startHeartBeatTimerTask(){
        if ( this.heartT == null){
            TimerTask timerTask = new TimerTask() {
                @Override
                public void run(){
                    sendHeartBeat();
                }
            };
            this.heartT = new Timer();
            this.heartT.schedule(timerTask, 0, this.heartTimeOut);
        }
    }
    
    @Override
    public void run(){
        startLogging();
        String str = "[TimerThread]: ElectionTimeout (" + this.electionTimeOut + ")";
        System.out.println(str);
        logger.log(Level.INFO, str);
        str = "[TimerThread]: HeartTimeOut (" + this.heartTimeOut + ")";
        System.out.println(str);
        logger.log(Level.INFO, str);
        while (stopped == false){
            String messageRecv = new String(this.socket.recv(0));
            if (!messageRecv.isEmpty()){
                if ("Start Election Timeout".equalsIgnoreCase(messageRecv)){
                    if (this.heartT != null){
                        this.heartT.cancel();
                    }
                    this.startElectionTimerTask();
                } else if ("Start HeartBeat Timeout".equalsIgnoreCase(messageRecv)){
                    if (this.electT != null){
                        this.electT.cancel();
                    }
                    this.startHeartBeatTimerTask();
                } 
            }
        }
        
        if (stopped == true){
            this.socket.close();
        }
    }
    
    public void stopWorker(){
        this.stopped = true;
    }
} 
