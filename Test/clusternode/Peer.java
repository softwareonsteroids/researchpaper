/**
 *  Copyright 2017 Okusanya Oluwadamilola
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import java.io.PrintWriter;
import java.io.StringWriter; 
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.io.IOException;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Map;
import java.util.Collections;
import java.util.ArrayDeque;
import java.util.Map.Entry; 
import java.util.concurrent.ThreadLocalRandom;
import org.zeromq.ZMQ;


/**
 *  Peer object for the raft substrate of the 
 *  FittChart application
 */
 
public class Peer extends Thread{
    
    
    /**** Peer Communication State start****/
    // Map that stores the mapping of remote peers privateSndSocketID to privateRcvSocketIP
    private HashMap<String, String> remotePeersIDtoIP;
    
    /* Identities */
    private final String PUBLICNODENAME = "Cluster-" + 
                System.getenv("HOSTNAME");
    private final String PRIVATENODENAME = "Peer-" + 
        System.getenv("HOSTNAME");              // the socket identity for this node's privateSndSocket that 
                                                // identifes this node to the privateRcvSocket of another peer
    public final static String APPNAME = "Peer-" + 
        System.getenv("HOSTNAME");
    private String PRIVATENODEADDR;             // the address that this node's privateRcvSocket gets bound to
    private int waitCount = 0,                  // number of ticks to wait before starting a new election
            oldSize = 0,                        // initial size of rcvAppendEntries
            sleepTime = 0,                      // time to pause the thread after receiving a PAUSE command
            majority = 0,                       // the quorum required for diferent stages of the algorithm
            WAITCOUNT = 10;                     // set number of ticks to wait before starting a new election
                
    
    // Name of the log for a particular Node
    private final static int APPLOGFILENAMEID = ThreadLocalRandom.current().nextInt(1, 25+1);
    private static String APPLOGFILENAME;
    
    private RaftEnums.ClusterNodeState nodeState;
    
    // Sockets
    private ZMQ.Socket privateRcvSocket,        // receives Raft messages from the remote peers
                    privateSndSocket,           // sends Raft messages to the remote peers
                    publicRcvSndSocket,         // communicates with management server and the clients(producers,consumers)
                    timeOutSocket,              // communicates with the TimerThread
                    manSubSocket,               // subscribers to the HeartBeatWorker from the Management Server
                    manRepSocket,               // replies to the heartbeat messages from the HeartBeatWorker
                    fileThreadSocket,           // communicates with FileThread object
                    shutDownSocket;             // replies to the QUIT messages 
    
    private ZMQ.Context context = ZMQ.context(1);
    private ZMQ.Poller poller;
    
    // Utils object
    private Utils utils;
    
    // Timer object
    private TimerThread timerThr;
    
    // FileThread object
    private FileThread fileThr;
     
    // Logger
    public final static Logger logger = Logger.getLogger(Peer.class.getName());
    public static FileHandler handler;
    
    // is Heartbeats started
    public boolean isHeartBeatsStarted = false;
    /**** Peer Communication State end****/
    
    
    /**** Peer State variables start ******/
    private int curTerm,            // the lastest term that server has seen
                commitIndex,        // highest log entry known to be committed
                lastApplied;        // index of log entry applied to state machine 
                
    
    private String votedFor = "",        // the node that received a vote in the current term
                leaderId = "",           // the node that is the current leader 
                selfEndpoint = "";       // this node Id
        
    // for each remote peer, index of the next log entry to send to that server
    private HashMap<String, Integer> nextIndex;
    
    // for each remote peer, index of the highest log entry 
    // known to be replicated on the server
    private HashMap<String, Integer> matchIndex;
    
    // the state of the node
    private RaftEnums.RaftNodeState raftNodeElectionState;
    
    // the list of voters that have voted for the node during the current term
    private ArrayList<String> voters;
    
    // the log associated with the node
    private RaftLog log;
    
    /**** Peer State variables end*****/
    
    Peer(String addr){// the socket identity for this privateSndSocket that 
    // identifes this node to the privateRcvSocket of another peer
        this.PRIVATENODEADDR = addr;
        this.utils = new Utils();
        this.remotePeersIDtoIP = new HashMap<>();

        this.raftNodeElectionState = RaftEnums.RaftNodeState.FOLLOWER;
        this.log = new RaftLog();
        this.matchIndex = new HashMap<String, Integer>();
        this.nextIndex = new HashMap<String, Integer>();
        this.voters = new ArrayList<>();
    
        this.nodeState = RaftEnums.ClusterNodeState.DOWN;
        this.poller = new ZMQ.Poller(4);
        this.setUpSockets();
    }
    
    private void setUpSockets(){
        this.timeOutSocket = this.context.socket(ZMQ.PAIR);
        this.timeOutSocket.bind("inproc://timeOutSocket");
        
        this.fileThreadSocket = this.context.socket(ZMQ.PAIR);
        this.fileThreadSocket.bind("inproc://fileThreadSocket");
        
        this.privateRcvSocket = this.context.socket(ZMQ.ROUTER);
        this.privateRcvSocket.bind("tcp://*:6500");
        
        this.privateSndSocket = this.context.socket(ZMQ.DEALER);
        this.privateSndSocket.setIdentity(this.PRIVATENODENAME.getBytes());
        
        this.publicRcvSndSocket = this.context.socket(ZMQ.DEALER);
        this.publicRcvSndSocket.setIdentity(this.PUBLICNODENAME.getBytes());
        this.publicRcvSndSocket.connect("tcp://clusterman:5600");
        
        this.manSubSocket = this.context.socket(ZMQ.SUB);
        this.manSubSocket.connect("tcp://clusterman:5006");
        this.manSubSocket.subscribe("".getBytes());
        
        this.manRepSocket = this.context.socket(ZMQ.REQ);
        this.manRepSocket.connect("tcp://clusterman:5007");
        
        this.shutDownSocket = this.context.socket(ZMQ.REQ);
        this.shutDownSocket.connect("tcp://clusterman:5020");
        
        this.poller.register(this.publicRcvSndSocket, ZMQ.Poller.POLLIN);
        this.poller.register(this.privateRcvSocket, ZMQ.Poller.POLLIN);
        this.poller.register(this.timeOutSocket, ZMQ.Poller.POLLIN);
        this.poller.register(this.manSubSocket, ZMQ.Poller.POLLIN);
        
        this.timerThr = new TimerThread(this.context, "inproc://timeOutSocket", 
                        ThreadLocalRandom.current().nextInt(6000, 50000+1), 
                        ThreadLocalRandom.current().nextInt(30, 70+1), APPLOGFILENAMEID,
                        PRIVATENODENAME);
                        
        this.fileThr = new FileThread(this.context,"inproc://fileThreadSocket", APPLOGFILENAMEID);
    }
    
    public void changeStateToLeader(){
        this.printState("Changing from Candidate to Leader", "INFO");
        this.raftNodeElectionState = RaftEnums.RaftNodeState.LEADER;
        this.voters.clear();
        this.votedFor = "";
        
        // Reinitialization of entries after election
        for (String id: this.remotePeersIDtoIP.keySet()){
            this.nextIndex.put(id, this.log.getLastLogIndex() + 1);
            this.matchIndex.put(id, 0);
        }
    
        this.unicastMessage(this.PRIVATENODEADDR, "SET LEADER", true);
    }
    
    
    public void changeStateToCandidate(){
        if (this.raftNodeElectionState == RaftEnums.RaftNodeState.FOLLOWER){
            this.curTerm++;
            ArrayDeque message = new ArrayDeque();
            message.add(String.valueOf(this.curTerm));
            this.save(message, RaftEnums.PersistentStateType.TERM);
            message.clear();
            this.raftNodeElectionState = RaftEnums.RaftNodeState.CANDIDATE;
            this.voters.clear();
            this.voters.add(this.PRIVATENODEADDR);
            this.votedFor = this.PRIVATENODEADDR;
            message.add(this.votedFor);
            this.save(message, RaftEnums.PersistentStateType.VOTEDFOR);
            message.clear();

            this.printState("Broadcasting Request Votes", "INFO");
            this.broadCastRequestVotes();
        } else {
            this.printState("Node is in Candidate State (Change Ignored)", "INFO");
        }
    }
    
    private void save(ArrayDeque message, RaftEnums.PersistentStateType type){
        message.addFirst(type);
        String strToWrite = "Saving " + type.name() + " to disk";
        this.printState(strToWrite, "INFO");
        this.fileThreadSocket.send(this.utils.convertToBytes(message), 0);
        String response = new String(this.fileThreadSocket.recv(0));
        while ("FAIL".equalsIgnoreCase(response)){
            this.fileThreadSocket.send(this.utils.convertToBytes(message), 0);
            response = new String(this.fileThreadSocket.recv(0));
        }
    }
    
    public void changeStateToFollower(int candidateTerm, String leader){
        this.printState("Changing to follower", "INFO");
        this.raftNodeElectionState = RaftEnums.RaftNodeState.FOLLOWER;
        this.leaderId = leader;
        this.curTerm =  Math.max(candidateTerm, this.curTerm);
        ArrayDeque messageToSave = new ArrayDeque();
        messageToSave.add(String.valueOf(this.curTerm));
        this.save(messageToSave, RaftEnums.PersistentStateType.TERM);
        messageToSave.clear();
        this.voters.clear();
        this.votedFor = "";
    }
    
    private void unicastMessage(String id, Object message, boolean isString){
        String logStr = "";
        if (isString){
            String sndMessage = (String) message;
            sndMessage = sndMessage + "," + id;
            this.publicRcvSndSocket.send(sndMessage.getBytes(), 0);
            logStr = "Sending (" + sndMessage + ") to publicEndPoint ('tcp://clusterman:5600')"; 
            this.printState(logStr, "INFO");
        } else{
            ArrayDeque sndMessage = (ArrayDeque) message;
            String addr = this.remotePeersIDtoIP.get(id);
            this.privateSndSocket.connect(addr);
            this.privateSndSocket.send(this.utils.convertToBytes(sndMessage), 0);
            logStr = "Sending a message to privateRcvSocket (" + addr + ")"; 
            this.printState(logStr, "INFO");
            this.privateSndSocket.disconnect(addr);
        }
    }
    
    private void broadCastRequestVotes(){
              
        // Building messages
        int term = this.log.getLastTerm();
        int logIndex = this.log.getLastLogIndex();
        ArrayDeque message = new ArrayDeque();
        message.add(RaftEnums.RaftRPC.REQUESTVOTE);
        message.add(this.curTerm);
        message.add(logIndex);
        message.add(term);
        
        String strToWrite = "[Message Sent by " + "ClusterNode_" + APPLOGFILENAMEID + "]: Broadcasting request votes";
        this.printState(strToWrite, "INFO");
        for (String addr: this.getRemotePeers().values()){
            this.privateSndSocket.connect(addr);
            this.privateSndSocket.send(this.utils.convertToBytes(message), 0);
            strToWrite = "[Message Sent by " + "ClusterNode_" + APPLOGFILENAMEID + "]: Sending request vote to (" + this.privateSndSocket + ")";
            this.printState(strToWrite, "INFO");
            this.privateSndSocket.disconnect(addr);
        }
    }
    
    
    public void processRequestVote(ArrayDeque message){
        message.poll();
        int candidate_Term = (int) message.poll();
        int candidate_LogIndex = (int) message.poll();
        int candidate_LogTerm = (int) message.poll();
        String candidateId = (String) message.poll();
        message.clear();
        
        
        
        // The candidate's term is greater than the node's term
        
        if (this.curTerm < candidate_Term){
            this.printState("[RequestVote]: Changing to Follower from Candidate", "INFO");
            // force the node to go to follower state even if it is follower state
            this.changeStateToFollower(candidate_Term, "");         
        }
        
        message.add(RaftEnums.RaftRPC.REQUESTVOTE_REP);
        message.add(this.curTerm);
        
        if (this.curTerm > candidate_Term || isCandidateLogUptodate(candidate_LogIndex, candidate_LogTerm) == false){
            message.add(RaftEnums.ResponseType.NACK);
            
            if (this.curTerm > candidate_Term){
                this.printState("[RequestVote]: Candidate Term too small", "WARNING");
            } else {
                this.printState("[RequestVote]: Candidate Logs not up to date", "WARNING");
            }
        } else if (isCandidateLogUptodate(candidate_LogIndex, candidate_LogTerm)){
            // The candidate node that is receiving its own request vote starts here
            // The node grants the vote 
            message.add(RaftEnums.ResponseType.ACK);
            
            // The node saves the candidate's votedFor 
            ArrayDeque messageToSave = new ArrayDeque();
            if (this.votedFor == "" || !this.votedFor.equalsIgnoreCase(candidateId)){ 
                this.votedFor = candidateId;
                messageToSave.add(this.votedFor);
                this.save(messageToSave, RaftEnums.PersistentStateType.VOTEDFOR);
            }
        }
        
        this.unicastMessage(candidateId, message, false);
    }
    
    public void processRequestVoteRep(ArrayDeque message){
        if (this.isCandidate() == false){
            this.printState("Received RequestVote rep and am not a candidate in this term, Disregarding", "WARNING");
            return;
        }
        
        message.poll();         // Removing message Type = RaftEnums.RaftRPC.REQUESTVOTE_REP
        int followerTerm = (int) message.poll();
        RaftEnums.ResponseType type = (RaftEnums.ResponseType) message.poll();
        String followerId = (String) message.poll();
         
        if (this.curTerm > followerTerm){
            String str = "Request Vote from " + followerId + " ignored, stale term";
            this.printState(str, "WARNING");
        } else if (this.curTerm < followerTerm){
            String str = "Request Vote from " + followerId + " denied";
            this.printState(str, "WARNING");
            this.changeStateToFollower(followerTerm, "");
        } else {
            if (type == RaftEnums.ResponseType.ACK){
                this.voters.add(followerId);
                // this works if voters.size >= majoirty
                if (this.getNumberOfVoters() >= this.getMajority()){
                    this.changeStateToLeader();
                }
            }else if (type == RaftEnums.ResponseType.NACK){
                String str = "Request Vote from " + followerId + " denied";
                this.printState(str, "WARNING");
            }
        }
        
    }
    
    public void broadCastAppendEntries(){
        for (Map.Entry<String, String> entry: this.getRemotePeers().entrySet()){
            String id = entry.getKey();
            String addr = entry.getValue();
            
            if (!addr.equalsIgnoreCase(this.PRIVATENODEADDR)){
                int nextIndex = this.getNextIndexFor(id);
                int entryLastLogIndex = this.log.getEntryAtPrevIndex(nextIndex).getIndex();
                int entryLastLogTerm = this.log.getEntryAtPrevIndex(nextIndex).getTerm();
                ArrayList<LogEntry> entries = this.log.getEntriesFromToEnd(nextIndex);
                ArrayDeque message = new ArrayDeque();
                message.add(RaftEnums.RaftRPC.APPENDENTRIES);
                message.add(this.curTerm);
                message.add(entryLastLogIndex);
                message.add(entryLastLogTerm);
                message.add(this.commitIndex);
                message.add(entries);
                
                this.privateSndSocket.connect(addr);
                String str = "Sending append entries to (" + this.privateSndSocket + ")";
                this.printState(str, "INFO");
                this.privateSndSocket.send(this.utils.convertToBytes(message), 0);
                this.privateSndSocket.disconnect(addr);
            }
        }
    }
        
    public void processAppendEntries(ArrayDeque messageRecv){
        messageRecv.poll();         // RaftEnums.RaftRPC.APPENDENTRIES
        int remoteTerm = (int) messageRecv.poll();
        int leader_prevLogIndex = (int) messageRecv.poll();
        int leader_prevTerm = (int) messageRecv.poll();
        int leader_commitIndex = (int) messageRecv.poll();
        ArrayList<LogEntry> entries = (ArrayList<LogEntry>) messageRecv.poll();
        String leaderId = (String) messageRecv.poll();
        
        messageRecv.clear();
        if (this.curTerm == remoteTerm){
            if (this.isLeader() || this.isCandidate()){
                // Candidate 
                // just in case an election just happened before this node received this AppendEntries
                // Leader
                // just in case if a better leader is found
                if (this.isLeader()){
                    this.printState("[AppendEntries]: A better leader is found", "SEVERE");
                    this.changeStateToFollower(remoteTerm, "");
                    return;
                } else if (this.isCandidate()){
                    this.printState("[AppendEntries]: A leader is found", "SEVERE");
                    this.changeStateToFollower(remoteTerm, leaderId);
                }
            }
            
            if (this.leaderId == ""){
                this.leaderId = leaderId;
            }
            
            LogEntry prev = this.log.getEntryAt(leader_prevLogIndex);
            messageRecv.add(RaftEnums.RaftRPC.APPENDENTRIES_REP);
            messageRecv.add(this.curTerm);
            int prevLogIndex = prev.getIndex();
            int prevEntryTerm = prev.getTerm();
                
            if (prevEntryTerm == leader_prevTerm){
                // check if the log is the same, if so send back a Heartbeat response
                
                int lastLogIndex = this.log.getLastLogIndex();
                if ( entries.isEmpty()){
                    this.printState("[AppendEntries]: Found heartbeat", "INFO");
                    messageRecv.add(RaftEnums.ResponseType.HEARTBEAT);
                } else {
                    String str = "[AppendEntries]: Found a new entry," + "Previous entry_index=" + 
                            leader_prevLogIndex+ " Previous entry_term=" + leader_prevTerm;
                    this.printState(str, "INFO");
                    this.log.appendEntries(entries);
                    messageRecv.add(RaftEnums.ResponseType.ACK);
                    messageRecv.add(lastLogIndex);
                }
                
                this.unicastMessage(leaderId, messageRecv, false);
                
                if (leader_commitIndex > this.commitIndex){
                    this.commitIndex = Math.min(leader_commitIndex, lastLogIndex);
                }
                
                // this.applyCommitEntries();
            } else {
                    // Catch if prevEntryTerm != leader_prevTerm or there is no previous item
                    // in the log for the leader_prevLogIndex
                String str = "[AppendEntries]: Found a conflicting index," + "Local entry_term=" 
                                + prevEntryTerm + " Actual leader entry_term=" + leader_prevTerm;
                this.printState(str, "WARNING");
                int conflictingIndex = this.log.findConflictingIndex(leader_prevTerm);
                this.log.deleteFrom(conflictingIndex);
                messageRecv.add(RaftEnums.ResponseType.NACK);
                messageRecv.add(conflictingIndex);
                this.unicastMessage(leaderId, messageRecv, false);
            }
        } else {
            if (this.curTerm > remoteTerm){
                this.printState("[AppendEntries] Current Term > Remote Term: State Request", "SEVERE");
            } else if (this.curTerm < remoteTerm){
                this.printState("[AppendEntries] Current Term < Remote Term: Current Server Term Outdated", "SEVERE");
                this.changeStateToFollower(remoteTerm, leaderId);
            }
            messageRecv.add(RaftEnums.RaftRPC.APPENDENTRIES_REP);
            messageRecv.add(this.curTerm);
            messageRecv.add(RaftEnums.ResponseType.NACK);
            this.unicastMessage(leaderId, messageRecv, false);
        }
    }
    
    public void processAppendEntriesRep(ArrayDeque messageRecv, String followerId){
        if (this.isLeader() == false){
            return;
        }
    
        messageRecv.poll();
        int followerTerm = (int) messageRecv.poll();
        RaftEnums.ResponseType type = (RaftEnums.ResponseType) messageRecv.poll();
        
        String strToWrite = "";
        if (this.curTerm > followerTerm){
            strToWrite = "[AppendEntriesRep]: Stale Request from " + followerId; 
            this.printState(strToWrite, "WARNING");
        } else if (this.curTerm < followerTerm){
            this.printState("[AppendEntriesRep]: Server outdated, will switch to follower", "WARNING");
            this.changeStateToFollower(followerTerm, "");
        } else {
            if (type == RaftEnums.ResponseType.ACK){
                int followerLogIndex = (int) messageRecv.poll();
                this.setMatchIndexFor(followerId, followerLogIndex);
                int new_next_index = Math.min(followerLogIndex + 1,
                                        this.log.getLastLogIndex() + 1);
                this.setNextIndexFor(followerId, new_next_index);
                
                // If there exists a N > commitIndex, a majority of matchIndex[i] >= N,
                // and log[N].term == curTerm, set commitIndex = N
                
                ArrayList<Integer> all_match_values = 
                    new ArrayList<Integer>(this.matchIndex.values());
                all_match_values.add(this.log.getLastLogIndex());
                Collections.sort(all_match_values, Collections.reverseOrder());
                int assigned_majority_index = (int) (all_match_values.size() / 2);
                int committedIndex = all_match_values.get(assigned_majority_index);
                int committedTerm = this.log.getEntryAt(committedIndex).getTerm();
                
                if (committedTerm == this.curTerm){
                    int newCommittedIndex = Math.max(committedIndex, this.commitIndex);
                    this.commitIndex = newCommittedIndex;
                    this.printState("[AppendEntriesRep]: Committing Entries", "INFO");
                    // this.applyCommittedLogEntries;
                    // send write responses
                }
            } else if (type == RaftEnums.ResponseType.NACK) { 
                if (messageRecv.size() != 0){
                    int conflictingIndex = (int) messageRecv.poll();
                    int newNextIndex = Math.max(1, conflictingIndex);
                    this.printState("[AppendEntriesRep]: Set new index for conflicting value", "SEVERE");
                    this.setNextIndexFor(followerId, newNextIndex);
                }
            }
        }
    }
    
    private boolean isCandidateLogUptodate(int candidate_LogIndex, int candidate_LogTerm){
        /**
         * Raft determines which of the two logs is more up to date by comparing 
         * the index and term of the last entries in the logs.
         * 
         * If the logs have last entries with different terms, then the log with
         * the later term is more up-to-date. If the logs end with the same term,
         * then whichever log is longer is more up-to-date.
         */
         int lastIndex = this.log.getLastLogIndex();
         int lastTerm = this.log.getLastTerm();
         
         if (lastTerm > candidate_LogTerm){
            return false;
         } else if (lastTerm < candidate_LogTerm){
            return true;
         } else {
            return lastIndex <= candidate_LogIndex;
         }
    }
    
    public RaftEnums.RaftNodeState getRaftNodeState(){
        return this.raftNodeElectionState;
    }
    
    public boolean isLeader(){
        return this.raftNodeElectionState == RaftEnums.RaftNodeState.LEADER;
    }
    
    public boolean isFollower(){
        return this.raftNodeElectionState == RaftEnums.RaftNodeState.FOLLOWER;
    }
    
    public boolean isCandidate(){
        return this.raftNodeElectionState == RaftEnums.RaftNodeState.CANDIDATE;
    }
    
    public String getLeaderId(){
        return this.leaderId;
    }
    
    public boolean isLeaderNotKnown(){
        return this.leaderId == "";
    }
    
    public void setLeaderId(String id){
        this.leaderId = id;
    }
    
    private int getNextIndexFor(String id){
        return this.nextIndex.get(id);
    }
    
    private void setNextIndexFor(String id, int index){
        this.nextIndex.put(id, index);
    }
    
    private int getMatchIndex(String id){
        return this.matchIndex.get(id);
    }
    
    private void setMatchIndexFor(String id, int index){
        this.matchIndex.put(id, Math.max(this.matchIndex.get(id), index));
    }
    
    private int getNumberOfVoters(){
        return this.voters.size();
    }
    
    public void tearDownSockets(){
        this.privateRcvSocket.close();
        this.privateSndSocket.close();
        this.publicRcvSndSocket.close();
        this.manRepSocket.close();
        this.manSubSocket.close();
        this.shutDownSocket.close();
        this.timeOutSocket.close();
        this.fileThreadSocket.close();
        this.context.term();
    }
    
    public HashMap<String, String> getRemotePeers(){
        return this.remotePeersIDtoIP;
    }
    
    public ZMQ.Socket getPublicEndpoint(){
        return this.publicRcvSndSocket;
    }
    
    public ZMQ.Socket getTimeOutSocket(){
        return this.timeOutSocket;
    }
    
    public int getMajority(){
        this.majority = (this.remotePeersIDtoIP.size() / 2) + 1;    // majority is above 50%
        return this.majority;
    }
    
    /*** Logging methods start***/

    private void printState(String message, String type){
        String strToWrite = "[STATE at " + this.PRIVATENODENAME +  "]: " + message;
        System.out.println(strToWrite);
        switch(type){
            case "INFO":
                logger.log(Level.INFO, strToWrite);
                break;
            case "WARNING":
                logger.log(Level.WARNING, strToWrite);
                break;
            case "SEVERE":
                logger.log(Level.SEVERE, strToWrite);
                break;
            default:
                System.out.println("Cannot log!!!!");
                break;
        }
    }
        
    private void printRemotePeers(){
        System.out.println("The remotePeersIDtoIP: ");
        int count = 0;
        for (Map.Entry<String, String> entry: this.remotePeersIDtoIP.entrySet()){
            String key = entry.getKey();
            String value = entry.getValue();
            String line = "Remote Peer" + String.valueOf(count) + ": key, " + key + " value," + value;
            this.printState(line, "INFO");
        }
    }
    
    /*** Logging methods end ***/
    
    public static void startLogging(){
        try{
            APPLOGFILENAME = "ClusterNode_" + APPLOGFILENAMEID + ".log";
            handler = new FileHandler(APPLOGFILENAME, false);
        } catch ( SecurityException | IOException e ){
            e.printStackTrace();
        }
        
        System.out.println("===============================================");
        String strToWrite = "This is " + APPNAME + " or " + "ClusterNode_" + APPLOGFILENAMEID + "'s log";
        System.out.println(strToWrite);
        System.out.println("===============================================");
        handler.setFormatter(new SimpleFormatter());
        logger.setUseParentHandlers(false);
        logger.addHandler(handler);
        logger.setLevel(Level.INFO);
    }
    
    @Override
    public void run(){
        Thread timerProcess = new Thread(this.timerThr);
        Thread fileProcess = new Thread(this.fileThr);
        
        try{
            while (!Thread.currentThread().isInterrupted()){
                ArrayDeque raftCommand;
                Command command;
                String message, nodeId, strToWrite;
                
                this.poller.poll();
                
                // the management socket
                if (this.poller.pollin(0)){
                    message = new String(this.publicRcvSndSocket.recv(0));       // "Command" string for all the other commands
                                                                    // "IPs" for string for only START
                    command = (Command) this.utils.convertFromBytes(this.publicRcvSndSocket.recv(0));
                    RaftEnums.CommandType type = command.getCommand();
                    switch(type){
                        case START:
                            this.printState(command.getCommand().name(), "INFO");
                            if (this.nodeState == RaftEnums.ClusterNodeState.STARTED){
                                this.remotePeersIDtoIP.putAll( 
                                    (HashMap<String, String>) this.utils.convertFromBytes(this.publicRcvSndSocket.recv(0))
                                    );
                                this.printRemotePeers();
                                fileProcess.start();
                                timerProcess.start();
                                this.printState("Starting First Election TimeOut", "INFO");
                                this.timeOutSocket.send("Start Election Timeout".getBytes(),  0);
                                this.nodeState = RaftEnums.ClusterNodeState.UP;
                            } else {
                                this.printState("Node is already up (Error)", "WARNING");
                                this.publicRcvSndSocket.send("Cluster has already started".getBytes(), 0);
                            }
                            break;
                        case QUIT:
                            this.printState(command.getCommand().name(), "INFO");
                            this.nodeState = RaftEnums.ClusterNodeState.DOWN;
                            
                            // Quitting
                            this.shutDownSocket.send("Shutting down!!!".getBytes(), 0);
                            message = new String(this.shutDownSocket.recv(0));
                            System.out.println("[Shutdown]: Received (" + message + ")");
                            if ("GOODBYE".equalsIgnoreCase(message)){
                                this.timerThr.stopWorker();
                                this.fileThr.stopWorker();
                                Thread.currentThread().interrupt();
                                //return;
                            }
                            break;
                        case PAUSE:
                            this.printState(command.getCommand().name(), "INFO");
                            this.sleepTime = command.getTimeOut();
                            break;
                        case GETLOGS:
                            this.printState(command.getCommand().name(), "INFO");
                            ArrayList<String> stringsToSend = 
                                    this.utils.readFromFile(command.getNumOfLinesToRetrieve(), this.APPLOGFILENAME);
                            message = String.join(", ", stringsToSend);
                            this.publicRcvSndSocket.send(message.getBytes(), 0);
                            break;
                        default:
                            this.printState("This command is not recognised", "WARNING");
                            break;
                    }
                }
                
                // private Raft socket
                if (this.poller.pollin(1)){
                    nodeId = new String(this.privateRcvSocket.recv(0));
                    raftCommand = (ArrayDeque) this.utils.convertFromBytes(this.privateRcvSocket.recv(0));
                    RaftEnums.RaftRPC type = (RaftEnums.RaftRPC) raftCommand.peekFirst();
                    switch(type){
                        case APPENDENTRIES:
                            if (this.waitCount > 0){
                                this.waitCount = 0;
                            }
                            this.printState("Processing AppendEntries", "INFO");
                            raftCommand.add(nodeId);
                            this.processAppendEntries(raftCommand);
                            break;
                        case APPENDENTRIES_REP:
                            this.printState("Processing AppendEntries rep", "INFO");
                            this.processAppendEntriesRep(raftCommand, nodeId);
                            break;
                        case REQUESTVOTE:
                            raftCommand.add(nodeId);
                            this.printState("Processing request vote", "INFO");
                            this.processRequestVote(raftCommand);
                            break;
                        case REQUESTVOTE_REP:
                            raftCommand.add(nodeId);
                            this.printState("Processing request vote rep", "INFO");
                            this.processRequestVoteRep(raftCommand);
                            if (this.isLeader() && this.isHeartBeatsStarted == false){
                                this.printState("Broadcasting AppendEntries", "INFO");
                                this.broadCastAppendEntries();
                                this.printState("Sending first heartbeat", "INFO");
                                this.timeOutSocket.send("Start HeartBeat Timeout".getBytes(), 0);
                                this.isHeartBeatsStarted = true;
                            }
                            break;
                        default:
                            break;
                    }
                }
                
                // timer Socket
                if (this.poller.pollin(2)){
                    message = new String(this.timeOutSocket.recv(0));

                    strToWrite = "Received (" + message + ")";
                    System.out.println(strToWrite);
                    strToWrite = "NodeState: (" + this.getRaftNodeState().name() + ")";
                    System.out.println(strToWrite);
                    // Two scenarios
                    // (a) The leader of the cluster is known but has not sent any new AppendEntries heartbeats
                    // (b) The leader of the cluster is not known
                    if ("TIMEOUT".equalsIgnoreCase(message)){
                        if (this.isLeaderNotKnown() == true){
                            if (this.isFollower() || this.isCandidate()){
                                if (this.isFollower()){
                                    this.printState("Changing from Follower to Candidate (TIMEOUT)", "INFO");
                                } else if (this.isCandidate()){
                                    this.printState("Changing from Candidate to Candidate (TIMEOUT)", "INFO");
                                }
                                this.changeStateToCandidate();
                                //this.timeOutSocket.send("Start Election Timeout".getBytes(), 0);
                            } 
                        } else if (this.isLeaderNotKnown() == false && this.isFollower()){
                            this.waitCount++;
                            if (this.waitCount == WAITCOUNT){
                                this.printState("Resetting all state, Starting new Election (TIMEOUT)", "INFO");
                                this.setLeaderId("");
                                this.waitCount = 0;
                            }
                        }
                    }else if ("HEARTBEAT".equalsIgnoreCase(message)){
                        if (this.isLeader()){
                            if (waitCount > 0){
                                waitCount = 0;
                            }
                            this.printState("Sending Heartbeats", "INFO");
                            this.broadCastAppendEntries();
                        }
                    }
                }
                
                // subscriber Socket 
                if (this.poller.pollin(3)){
                    message = new String(this.manSubSocket.recv(0));
                    String str = "Received: (" + message + ") on [Subs Socket]";
                    this.printState(str, "INFO");
                    
                    // Communicating with the management server
                    this.manRepSocket.send("I am here".getBytes(), 0);
                    this.printState("Sent (I am here) on [RepEndpoint]", "INFO");
                    message = new String(this.manRepSocket.recv(0));
                    str = "Received: (" + message + ") on [RepEndpoint]";
                    this.printState(str, "INFO");
                    
                    if ("I have seen you".equalsIgnoreCase(message) && this.nodeState == RaftEnums.ClusterNodeState.DOWN){
                        str = "I am ready," + this.PRIVATENODENAME + "_" + this.PRIVATENODEADDR;
                        this.publicRcvSndSocket.send(str.getBytes(), 0);
                        str = "Just sent(I am ready," + this.PRIVATENODENAME + "_" + this.PRIVATENODEADDR + ") as [Heartbeats]";
                        this.printState(str, "INFO");
                        this.nodeState = RaftEnums.ClusterNodeState.STARTED;
                    }
                }
            }
            
            if (Thread.currentThread().isInterrupted()){
                return;
            }
            
        } catch (Exception ie){
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ie.printStackTrace(pw);
            System.out.println(sw.toString());
        }
        
        this.tearDownSockets();
    }
    
    public static void main(String [] args){
        startLogging();
        String docker_net_alias = System.getenv("NETALIAS");
        String addr = "tcp://" + docker_net_alias + ":6500";
        System.out.println(addr);
        Peer peer = new Peer(addr);
        peer.start();
    }
}