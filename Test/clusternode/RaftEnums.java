/**
 *  Copyright 2017 Okusanya Oluwadamilola
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
 
/**
 *  This is the enums class for all the types of commands in the 
 *  Raft implementation.
 */

public class RaftEnums{

    public static enum ClusterState{
        NOTSTARTED,
        STARTED,
        RUNNING
    }
    
    public static enum ClusterNodeState{
        DOWN,               // intial state 
        STARTED,            // state after receiving first heartbeat 
        UP,                 // state after receiving the START command
        PAUSED,
    }
    
    public static enum PersistentStateType{
        TERM,
        VOTEDFOR,
        LOG
    }
    
    public static enum RaftNodeState{
        FOLLOWER,
        CANDIDATE,
        LEADER
    }
    // For sending messages 
    // (1) MGTa <==> RFTa
    // (2) MGTb <==> MGTb
    public static enum RaftRPC{
        APPENDENTRIES,
        APPENDENTRIES_REP,
        REQUESTVOTE,
        REQUESTVOTE_REP,
        CLIENTREQUEST,
        CLIENTREQUEST_REP,
        CLIENTQUERY,
        CLIENTQUERY_REP
    }

    public static enum ResponseType{
        ACK,            // another word for yes
        NACK,            // another word for no
        HEARTBEAT
    }

    public static enum CommandType{
        START,
        QUIT,
        PAUSE,          // REMOVESERVER
        GETALLIDS,
        GETLEADERID,
        GETLOGS,
        ADDSERVER,
    }

    // For messages
    // (1) CON <==> MGT
    // (2) PROD <==> MGT
    public static enum ClientRequestType{
        POST,     // Anagolous to CLIENTREQ
        GET       // Anagolous to CLIENTQUERY
    }
}
