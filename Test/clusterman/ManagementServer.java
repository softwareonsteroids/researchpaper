/**
 *  Copyright 2017 Okusanya Oluwadamilola
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Collection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.io.IOException;
import org.zeromq.ZMQ;

/**
 *  Server object for the management substrate of the 
 *  FittChart application
 *  
 *  @author Okusanya Oluwadamilola
 *  @version 1.0
 */
 
 
public class ManagementServer extends Thread{

    /* Mapping of ClusterNodeNum to the node's public socket ID
     This mapping is only for communication with the command client */
    private HashMap<String, String> nodeNumToPublicSocketID;
    
    /* Mapping of nodes' internal send socket id to the address of their internal recv socket
     This mapping is for communication between raft nodes */
    private HashMap<String, String> nodePrivSndSocketIDToPrivRcvSocketIP;
    
    /* Mapping of nodes' public socket ids to the address of their internal recv socket
     This mapping is for communication between the raft nodes and the clients(consumers, producers) */
    private HashMap<String, String> nodePublicSocketIDToPrivRcvSocketIP;
    
    
    // Sockets
    private ZMQ.Socket manClientSocket,         // management client socket
                    heartBeatRcvSocket,         // socket to respond to heartbeats 
                    commandSocket,              // socket to talk to the cluster of Raft Nodes
                    shutDownSocket;             // socket to shutdown the cluster of Raft Nodes
    private ZMQ.Poller poller;
    private ZMQ.Context context= ZMQ.context(1);
    
    // Raft Nodes
    private int noofReqNodes,                   // required number of nodes in the cluster
                noOfAvailNodes,                 // number of available nodes in the cluster
                nodeIdNum,                      // nodenum for the nodeNumToPublicSocketID hashMap
                numOfLogsToSend;                // number of log messages to send back
                
    // Cluster state
    private RaftEnums.ClusterState clusterState;
    
    // Application logs to send back to the management client
    private ArrayList<String> applogs;
    
    // Logging
    private final static String LOGFILENAME = "ManagementServer.log";
    private final static Logger logger = Logger.getLogger(ManagementServer.class.getName());
    private static FileHandler handler = null;
    
    // The leader Node id
    private String leaderNodeId;
    
    // Utils object 
    private Utils utils;
    
    ManagementServer(int num){
        this.noofReqNodes = num;
        this.noOfAvailNodes = 0;
        this.nodeIdNum = 1;
        this.numOfLogsToSend = 0;
        this.clusterState = RaftEnums.ClusterState.NOTSTARTED;
        this.applogs = new ArrayList<>();
        this.nodeNumToPublicSocketID = new HashMap<>();
        this.nodePrivSndSocketIDToPrivRcvSocketIP = new HashMap<>();
        this.nodePublicSocketIDToPrivRcvSocketIP = new HashMap<>();
        this.utils = new Utils();
        this.nodeNumToPublicSocketID.put("ClusterNode-0", "clusterman");
        this.setUpSockets();
    }
    
    private void setUpSockets(){
        this.manClientSocket = this.context.socket(ZMQ.REP);
        this.manClientSocket.bind("tcp://*:4010");
        this.heartBeatRcvSocket = this.context.socket(ZMQ.REP);
        this.heartBeatRcvSocket.bind("tcp://*:5007");
        this.commandSocket = this.context.socket(ZMQ.ROUTER);
        this.commandSocket.bind("tcp://*:5600");
        this.shutDownSocket = this.context.socket(ZMQ.REP);
        this.shutDownSocket.bind("tcp://*:5020");
        
        this.poller = new ZMQ.Poller(4);
        this.poller.register(this.manClientSocket, ZMQ.Poller.POLLIN);
        this.poller.register(this.commandSocket, ZMQ.Poller.POLLIN);
        this.poller.register(this.heartBeatRcvSocket, ZMQ.Poller.POLLIN);
        this.poller.register(this.shutDownSocket, ZMQ.Poller.POLLIN);
    }
    
    private void tearDown(){
        this.manClientSocket.close();
        this.heartBeatRcvSocket.close();
        this.commandSocket.close();
        this.shutDownSocket.close();
        this.context.term();
    }
    
    public static void startLogging(){
        try{
            handler = new FileHandler(LOGFILENAME, false);
        } catch ( SecurityException | IOException e){
            e.printStackTrace();
        }
        
        handler.setFormatter(new SimpleFormatter()); 
        logger.setUseParentHandlers(false);        // disable logging to the console
        logger.addHandler(handler);
        logger.setLevel(Level.INFO);
    }
    
    @Override
    public void run(){
        HeartbeatWorker worker = new HeartbeatWorker(this.context, "tcp://*:5006");
        Thread thr = new Thread(worker);
        
        try{
            while (!Thread.currentThread().isInterrupted()){
                Command command;
                String message, nodeId;
                
                this.poller.poll();
                
                if (this.poller.pollin(0)){
                    command = (Command) this.utils.convertFromBytes(this.manClientSocket.recv(0));
                    RaftEnums.CommandType type = command.getCommand();
                    switch(type){
                        case START:
                            thr.start();
                            this.clusterState = RaftEnums.ClusterState.STARTED;
                            this.manClientSocket.send(command.toString().getBytes(), 0);
                            logger.log(Level.INFO, "[Command]: Just sent START to all nodes"); 
                            break;
                        case QUIT:
                            for (String id: this.nodeNumToPublicSocketID.values()){
                                this.commandSocket.send(id, ZMQ.SNDMORE);
                                this.commandSocket.send("Command".getBytes(), ZMQ.SNDMORE);
                                this.commandSocket.send(this.utils.convertToBytes(command), 0);
                            }
                            this.manClientSocket.send(command.toString().getBytes(), 0);
                            System.out.println("[Command]: Just send QUIT to all nodes");
                            logger.log(Level.INFO, "[Command]: Just send QUIT to all nodes");
                            break;
                        case GETALLIDS:
                            StringBuilder sb = new StringBuilder();
                            sb.append("(");
                            Object [] arr = this.nodeNumToPublicSocketID.entrySet().toArray();
                            for (Object elem: arr){
                                sb.append(elem);
                                sb.append("\n");
                            }
                            sb.append(")");
                            this.manClientSocket.send(sb.toString().getBytes(), 0);
                            logger.log(Level.INFO, "[Command]: Just replied to GETALLIDS");
                        default:
                            break;
                    }
                }
                
                if (this.poller.pollin(1)){
                    nodeId = new String(this.commandSocket.recv(0));
                    message = new String(this.commandSocket.recv(0));
                    String str = "Received: (" + message + ") from " + nodeId;
                    System.out.println(str);
                    logger.log(Level.INFO, str);
                    String [] messageParts = message.trim().split(",");     // messageParts = 2 (Raft nodes), 1(everyone else)
                    
                    if ("SET LEADER".equalsIgnoreCase(messageParts[0])){
                        logger.log(Level.INFO, message);
                        this.leaderNodeId = messageParts[1];
                        str = "Received : (" + message + ") from ClusterNode " + nodeId; 
                        System.out.println(str);
                    }
                    
                    // Adding the node to the list of known nodes 
                    if ("I am ready".equalsIgnoreCase(messageParts[0])){
                        Collection appNodeIds = this.nodeNumToPublicSocketID.values();
                        if (appNodeIds == null || appNodeIds.contains(nodeId) == false){
                            String nodeNum = "Cluster-" + this.nodeIdNum;
                            this.nodeIdNum++;
                            this.nodeNumToPublicSocketID.put(nodeNum, nodeId);
                            if (this.noOfAvailNodes < this.noofReqNodes && 
                                nodeId.contains("Cluster")){
                                String [] nodeIds = messageParts[1].trim().split("_");
                                this.nodePrivSndSocketIDToPrivRcvSocketIP.put(nodeIds[0], nodeIds[1]); // nodeIds[1] contain the IP address
                                this.nodePublicSocketIDToPrivRcvSocketIP.put(nodeId, nodeIds[1]);
                                this.noOfAvailNodes++;
                                
                                System.out.println("[Setup]: Added new subscriber");
                                logger.log(Level.INFO, "[Setup]: Added new subscriber");
                                
                                System.out.println("Subscribers: " + this.noOfAvailNodes);
                                System.out.println("this.nodePrivSndSocketIDToPrivRcvSocketIP: ");
                                for (Map.Entry<String, String> entry: this.nodePrivSndSocketIDToPrivRcvSocketIP.entrySet()){
                                    String key = entry.getKey();
                                    String value = entry.getValue();
                                    System.out.println("key, " + key + " value " + value);
                                }
                                System.out.println();
                                System.out.println("this.nodePublicSocketIDToPrivRcvSocketIP: ");
                                for (Map.Entry<String, String> entry: this.nodePublicSocketIDToPrivRcvSocketIP.entrySet()){
                                    String key = entry.getKey();
                                    String value = entry.getValue();
                                    System.out.println("key, " + key + " value " + value);
                                }
                                
                                if (this.noOfAvailNodes == this.noofReqNodes && 
                                    this.clusterState == RaftEnums.ClusterState.STARTED){
                                    
                                    for (String id: this.nodeNumToPublicSocketID.values()){
                                        if (!"clusterman".equalsIgnoreCase(id)){
                                            this.commandSocket.send(id, ZMQ.SNDMORE);
                                            this.commandSocket.send("IPs".getBytes(), ZMQ.SNDMORE); 
                                            this.commandSocket.send(this.utils.convertToBytes(new Command.CommandBuilder("START").build()),
                                                ZMQ.SNDMORE);
                                            this.commandSocket.send(this.utils.convertToBytes(this.nodePrivSndSocketIDToPrivRcvSocketIP), 0);
                                            String strs = "[IPs]: Just sent " + "nodePrivSndSocketIDToPrivRcvSocketIP " + "Node" + nodeId;  
                                            System.out.println(strs);
                                            logger.log(Level.INFO, strs); 
                                        }
                                    }
                                    this.clusterState = RaftEnums.ClusterState.RUNNING;
                                }
                            }
                        }
                    }
                }
                
                if (this.poller.pollin(2)){
                    message = new String(this.heartBeatRcvSocket.recv(0));
                    String str = "[Heartbeats]: Received (" + message + ")";
                    System.out.println(str);
                    logger.log(Level.INFO, message);
                    this.heartBeatRcvSocket.send("I have seen you".getBytes(), 0);
                    logger.log(Level.INFO, "[Heartbeats]: Just sent (I have seen you)");
                }
                
                if (this.poller.pollin(3)){
                    message = new String(this.shutDownSocket.recv(0));
                    this.shutDownSocket.send("GOODBYE".getBytes(), 0);
                    logger.log(Level.INFO, "[Shutdown]: Just sent (GOODBYE)");
                    System.out.println("[Shutdown]: Just sent (GOODBYE)");
                    this.noOfAvailNodes--;
                    if (this.noOfAvailNodes == 0){
                        worker.stopHeartBeats();
                        this.clusterState = RaftEnums.ClusterState.NOTSTARTED;
                        Thread.currentThread().interrupt();
                    }
                }
            }
            
            if (Thread.currentThread().isInterrupted()){
                return;
            }
        } catch (Exception e){
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            System.out.println(sw.toString());
        }
        
        this.tearDown();
    }
    
    public static void main(String [] args){
        startLogging();
        int subscribers = Integer.parseInt(System.getenv("NOOFNODES"));
        if (subscribers % 2 == 0) {
            subscribers++;
        }
        String str = "Waiting for " + subscribers + " subscribers...";
        System.out.println(str);
        ManagementServer server = new ManagementServer(subscribers);
        server.run();
    }
}