/**
 *  Copyright 2017 Okusanya Oluwadamilola
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */


/**
 *  This manages the cluster.
 */

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectOutput;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.HashMap;
import java.util.logging.*;
import java.io.IOException;

import org.zeromq.ZMQ;

class HeartbeatWorker implements Runnable{
    private volatile boolean stopped;
    private final String stringToSend = "Hello: Are you there?";
    public ZMQ.Socket socket;
    private int time;

    HeartbeatWorker(ZMQ.Context context, String address){
        this.socket = context.socket(ZMQ.PUB);
        socket.setLinger(5000);
        socket.setSndHWM(0);
        socket.bind(address);
        this.setSleepTime(3000);
    }

    public void setSleepTime(int time){
        this.time = time;
    }

    @Override
    public void run(){
        while(stopped == false){
        // If there is no stop, then there would be a Context terminated exception.
            socket.send(stringToSend.getBytes(), 0)
            try{
                System.out.println("[Heartbeats]: Just sent (" + stringToSend + ")") ;	
                Thread.sleep(time);
            } catch (InterruptedException ie){
                ie.printStackTrace();
            }
        }
        if (stopped == true){
            socket.close();
        }
    }

    public void stopHeartBeats(){
        stopped = true;
    }
}


public class ManagementClient{
    private final static Logger fLogger = Logger.getLogger(ManagementClient.class.getName());
    fLogger.setUseParentHandlers(false);        // disable logging to the console
    private static FileHandler fLog = null;
    private HashMap<String, String> nodeIdentites= new HashMap<>(),
            HashMap<String, String> hostMap = new HashMap<>();
    private int SUBSCRIBERS_REQUIRED = 0, 
                subscribers = 0,
                numberToSend = 0,
                nodeId = 1;
    private boolean isClusterStarted = false,
                    isClusterRunning = false,
                    isContinueSet = false;
    private String leaderNodeId = "";
    
    public static void init(){
        try{
            fLog = new FileHandler("ManagementClient.log", false);
        } catch ( SecurityException | IOException e ){
            e.printStackTrace();
        }
        
        fLog.setFormatter(new SimpleFormatter()); 
        fLogger.addHandler(fLog);
        fLogger.setLevel(Level.INFO);
    }
    
    public static byte [] convertToBytes(Object object) throws IOException{
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutput out = new ObjectOutputStream(bos)){
            out.writeObject(object);
            return bos.toByteArray();
        }
    }

    public static void main(String [] args){
    
        ManagementClient.init();
        
        String clusterNodeId = "";

        SUBSCRIBERS_REQUIRED = Integer.parseInt(System.getenv("SUBSCRIBERS"));
        if (SUBSCRIBERS_REQUIRED % 2 == 0){                     // this enforces the 2f + 1 rule
            SUBSCRIBERS_REQUIRED = SUBSCRIBERS_REQUIRED + 1;
        }

        ZMQ.Context context = ZMQ.context(1);

        // Setup pair socket with the userinput thread
        ZMQ.Socket userInputSocket = context.socket(ZMQ.REP);
        userInputSocket.bind("tcp://*:5000");

        // Setup socket to receive heartbeats messages
        ZMQ.Socket synchSocket = context.socket(ZMQ.REP);
        synchSocket.bind("tcp://*:5001");

        // Setup router socket for synchronization messages, 
        ZMQ.Socket routerSocket = context.socket(ZMQ.ROUTER);
        routerSocket.bind("tcp://*:5100");
    
        // Setup socket for shutdown messages
        ZMQ.Socket shutdownClusterSocket = context.socket(ZMQ.REP);
        shutdownClusterSocket.bind("tcp://*:5101");

        // Initialise the pollin set
        ZMQ.Poller managementThreadPoller = new ZMQ.Poller(3);
        managementThreadPoller.register(userInputSocket, ZMQ.Poller.POLLIN);        // POLLIN/POLLOUT only listen for incoming/outgoing messages 
        managementThreadPoller.register(synchSocket, ZMQ.Poller.POLLIN);
        managementThreadPoller.register(routerSocket, ZMQ.Poller.POLLIN);
        managementThreadPoller.register(shutdownClusterSocket, ZMQ.Poller.POLLIN);

        // Initialize heartbeatworker
        HeartbeatWorker worker = new HeartbeatWorker(context, "tcp://*:5002");
        Thread heartbeats = new Thread(worker);


        // Main loop
        try{
            while (!Thread.currentThread().isInterrupted()){
                String message;
                managementThreadPoller.poll();

                // Sending user commands
                if (managementThreadPoller.pollin(0)){
                    message = new String(userInputSocket.recv(0));
                    String [] messageParts = message.trim().split("\\s+");
                    StringBuilder st = new StringBuilder();
                    if ("START".equalsIgnoreCase(messageParts[0]) && isClusterStarted == false){
                        heartbeats.start();
                        isClusterStarted = true;
                    } else if ("QUIT".equalsIgnoreCase(messageParts[0])){
                        for (String nodeId: nodeIdentites.values()){
                            routerSocket.send(nodeId, ZMQ.SNDMORE);
                            routerSocket.send(message.getBytes(), 0);
                        }
                    } else if ("STOP".equalsIgnoreCase(messageParts[0])){    
                            routerSocket.send(messageParts[1], ZMQ.SNDMORE);
                            routerSocket.send(messageParts[0].getBytes(), 0);
                            subscribers--;
                    } else if ("QUERY".equalsIgnoreCase(messageParts[0])){
                        if (nodeIdentites.containsKey(messageParts[1]) == true){
                            String nodeId = nodeIdentites.get(messageParts[1]);
                            routerSocket.send(nodeId, ZMQ.SNDMORE);
                            message = messageParts[0] + " " + messageParts[2];
                            fLogger.log(Level.INFO, message);
                            routerSocket.send(message.getBytes(), 0);
                        } else {
                            message = "Node number is not correct";
                            fLogger.log(Level.WARNING, message);
                        }
                    } else if ("GETIDS".equalsIgnoreCase(messageParts[0])){
                        isContinueSet = true;
                    } else if ("GETLEADERID".equalsIgnoreCase(messageParts[0]))
                        if ( this.leaderNodeId == ""){
                            fLogger.log(Level.WARNING, "CLuster leader has not been decided");
                            //System.out.println("CLuster leader has not been decided");
                            message = "CLuster leader has not been decided";
                        } else {
                            message = this.leaderNodeId;
                        }
                    } else {
                        fLogger.log(Level.INFO, "This command is not recognised!!!"); 
                    }

                    if (isContinueSet == true){
                        st.append("[Input]: The cluster identities : ( ");
                        for (String key : nodeIdentites.keySet()){
                            st.append(key);
                            st.append(" ");
                        }
                        st.append(" )");
                        isContinueSet = false;
                        userInputSocket.send(st.toString().getBytes(), 0);
                    } else {
                        userInputSocket.send(message.getBytes(), 0);
                    }
                }


                // Synchroniation sequence
                if (managementThreadPoller.pollin(1)){
                    message = new String(synchSocket.recv(0));
                    String str = "[Heartbeats]: Received (" + message + ")";
                    flog.log(Level.INFO, message);
                    if ("I am here".equalsIgnoreCase(message)){
                        synchSocket.send("I have seen you".getBytes(), 0);
                        fLogger.log(Level.INFO, "[Heartbeats]: Just sent(I have seen you)");
                    }
                }

                // Receiving and replying to heartbeats
                if (managementThreadPoller.pollin(2)){
                    clusterNodeId = new String(routerSocket.recv(0));
                    message = new String(routerSocket.recv(0));
                    String [] messageParts = message.trim().split(",");
                    String str = "Received: (" + message + ") from ClusterNode " + clusterNodeId;
                    fLogger.log(Level.INFO, str);
                    
                    if ("SET LEADER".equalsIgnoreCase(messageParts[0])){
                        flogger.log(Level.INFO, message);
                        this.leaderNodeId = messageParts[1];
                        String str = "Received : (" + message + ") from ClusterNode " + clusterNodeId; 
                        System.out.println(str);
                    }
                    
                    if ("I am ready".equalsIgnoreCase(messageParts[0])){
                        String key = "ClusterNode" + nodeId;
                        if (nodeIdentites.containsKey(key) == false && 
                            hostMap.containsKey(clusterNodeId) == false)){
                            nodeIdentites.put(key, clusterNodeId);          // this is referencing 
                            hostMap.put(clusterNodeId, messageParts[1]);
                            if (subscribers < SUBSCRIBERS_REQUIRED + 1){
                                subscribers++;
                                nodeId++;
                            }
                            fLogger.log(Level.INFO, "[Setup]: Added new subscriber");
                        }
                    }

                    if (subscribers == SUBSCRIBERS_REQUIRED && isClusterRunning == false){
                        for (String nodeId: nodeIdentites.values()){
                            routerSocket.send(nodeId, ZMQ.SNDMORE);
                            routerSocket.send("START".getBytes(), ZMQ.SNDMORE);
                            routerSocket.send(ManagementClient.convertToBytes(hostMap), 0);
                            fLogger.log(Level.INFO, "[Setup]: Just sent (START)");
                        }
                        // Allow  for node that starts up
                        
                        isClusterRunning = true;
                    } 
                }

                // For the shutdown sequence
                if (managementThreadPoller.pollin(3)){
                    message = new String(shutdownClusterSocket.recv(0));
                    shutdownClusterSocket.send("GOODBYE".getBytes(), 0);
                    fLogger.log(Level.INFO, "[Shutdown]: Just sent (GOODBYE)");
                    subscribers--;
                    if (subscribers == 0){
                        worker.stopHeartBeats();
                        Thread.currentThread().interrupt();
                    }
                }

            }

            // This closes the thread completely
            if (Thread.currentThread().isInterrupted()){
                return;
            }
        } catch (Exception e){
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            System.out.println(sw.toString());
        }

        shutdownClusterSocket.close();
        synchSocket.close();
        userInputSocket.close();
        routerSocket.close();
        context.term();
    }
}