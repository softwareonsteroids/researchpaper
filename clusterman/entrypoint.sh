#!/bin/bash

COMPLIE_ARGS=".:/usr/local/share/java/zmq.jar"
RUN_ARGS="-Djava.library.path=/usr/local/lib"
APP="ManagementClient"

javac -cp ${COMPLIE_ARGS} ${APP}.java
java -cp ${COMPLIE_ARGS} ${RUN_ARGS} ${APP}
