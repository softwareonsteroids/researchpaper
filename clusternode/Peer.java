/**
 *  Copyright 2017 Okusanya Oluwadamilola
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

// Test the network partitions using worstcase/blockade 
 
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Date;
import java.util.Random;
import java.util.ArrayList;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map.Entry;
import java.util.logging.*;
import java.io.IOException;

import org.zeromq.ZMQ;
import org.zeromq.ZMsg.ZMsg;

enum RaftProtocolState{
    FOLLOWER,
    CANDIDATE,
    LEADER
}


// Class to write log messages for Raft 
class LogFileThread implements Runnable{
    public ZMQ.Socket socket;
    private volatile boolean stopped;
    private String RAFTLOG = "Raftlog.txt";
    private Utils utils;
    
    LogFileThread(ZMQ.Context context, String address){
        this.socket = context.socket(ZMQ.PAIR);
        socket.connect(address);
        this.utils = new Utils();
    }
    
    @Override
    public void run(){
        while(stopped == false){
            try{
                ZMsg messageRecv = ZMsg.recvMsg(this.socket);
                String [] messageParts = messageRecv.popString().trim().split(",");
                if ("L".equalsIgnoreCase(messageParts[0])){
                    this.utils.writeToFile(messageParts[1], this.RAFTLOG);
                    String messageSend = "Written to log file:(" + messageParts[1] + ")";
                    this.socket.send(messageSend.getBytes(), 0);
                } else if ("R".equalsIgnoreCase(messageParts[0])){
                    ArrayList<String> gt = this.utils.readFromFile(String.valueOf(messageParts[1]), this.RAFTLOG);
                    this.socket.send(this.utils.convertToBytes(gt), 0);
                }
                messageRecv.destroy();
            } catch (InterruptedException ie){
                ie.printStackTrace();
            }
        }
        
        if (stopped == true){
            this.socket.close();
        }
    }
    
    public void stopWorker(){
        stopped = true;
    }
}


// Class for the Raft Process
class RaftThread implements Runnable{
    private final Logger rlogger = Logger.getLogger(RaftThread.class.getName());
    //boolean fool = false;
    //rlogger.setUseParentHandlers(fool);        // disable logging to the console
    private FileHandler rlog = null;
    private Random rand = new Random();
    
    public ZMQ.Socket fileThreadSocket, mainSocket;
    private volatile boolean stopped;
    private RaftProtocolState state;
    private int term, numVotesReq, actualVotesRecv, threadFileID = rand.nextInt(100) + 1;
    private String TERMFILENAME = "Term" + this.threadFileID + ".log";
    private LogFileThread lthr;
    private Utils utils;
    private Thread thr;
    
    public void init(){
        try{
            String THREADLOGFILENAME = "RaftThread" + this.threadFileID + ".log"; 
            this.rlog = new FileHandler(THREADLOGFILENAME, false);
        } catch ( SecurityException | IOException e ){
            e.printStackTrace();
        }
        
        this.rlog.setFormatter(new SimpleFormatter()); 
        this.rlogger.addHandler(this.rlog);
        this.rlogger.setLevel(Level.INFO);
    }
    
    RaftThread(ZMQ.Context context, String address){
        this.init();
        this.fileThreadSocket = context.socket(ZMQ.PAIR);
        this.mainSocket = context.socket(ZMQ.PAIR);
        this.mainSocket.connect(address);
        this.fileThreadSocket.bind("inproc://fileThread");
        this.lthr = new LogFileThread(context, "inproc://fileThread");
        this.thr = new Thread(this.lthr); 
        thr.start();
        this.state = RaftProtocolState.FOLLOWER;
        this.utils = new Utils();
        this.term = retrieveTerm();
    }
    
    public void setNumOfVotesReq(int numVotesReq){
        this.numVotesReq = ((numVotesReq - 1) / 2) + 1;      // this allows for a strict majority
        this.actualVotesRecv = 0;
    }
    
    private int retrieveTerm(){
        return new Integer.parseInt(new String(Files.readAllBytes(Paths.get(this.TERMFILENAME))));
    }

    private void sendVote(String command){
        ZMsg message = new ZMsg();
        message.add(command);
        message.add("TERM");
        message.add(String.valueOf(this.term));
        message.send(this.mainSocket);
        message.destroy();
    }
    
    @Override
    public void run(){
        while (stopped == false){
            try{
                ZMsg messageRecv = ZMsg.recvMsg(this.mainSocket);
                String command = messageRecv.popString();
                if ( "TIMEOUT".equalsIgnoreCase(command)){
                    this.term = retrieveTerm();
                    this.term++;
                    this.utils.writeToFile(String.valueOf(this.term), this.TERMFILENAME);
                    this.rlogger.log(Level.INFO, "Increasing term");
                    this.state = RaftProtocolState.CANDIDATE;
                    this.rlogger.log(Level.INFO, "Changing state to CANDIDATE");
                    this.sendVote("VOTE:REQ");
                } else if ("QUERY".equalsIgnoreCase(command)){
                    
                }else{
                    String firstPayloadDelim = messageRecv.popString();
                    int termRecv = Integer.parseInt(messageRecv.popString());
                    if (termRecv >= this.term){
                        if ("VOTE:REQ".equalsIgnoreCase(command)){
                            this.term = termRecv;
                            this.utils.writeToFile(String.valueOf(this.term), this.TERMFILENAME);
                            this.sendVote("VOTE:REP");
                        }else if ("VOTE:REP".equalsIgnoreCase(command)){
                            if (this.actualVotesRecv < this.numVotesReq){
                                this.actualVotesRecv++;
                                if (this.actualVotesRecv == this.numVotesReq){
                                    this.actualVotesRecv = 0;
                                    this.state = RaftProtocolState.LEADER;
                                    rlogger.log(Level.INFO, "Changing state to LEADER");
                                    this.sendVote("SET LEADER");
                                }
                            }
                        }else if ("SET LEADER".equalsIgnoreCase(command)){
                            if (this.state == RaftProtocolState.CANDIDATE){
                                this.state = RaftProtocolState.FOLLOWER;
                                rlogger.log(Level.INFO, "Changing state to FOLLOWER");
                                ZMsg message = new ZMsg();
                                message.add("Changing state to FOLLOWER");
                                mesage.send(this.mainSocket);
                                message.destroy();
                            }
                        }else if("LOG".equalsIgnoreCase(command) || "RETR".equalsIgnoreCase(command)){
                            messageRecv.send(this.fileThreadSocket);
                            byte [] messRecv = this.fileThreadSocket.recv(0);
                            String [] messageParts = messageRecv.popString().trim().split(",");
                            if ("L".equalsIgnoreCase(messageParts[0])){
                                String mess = new String(messRecv);
                                rlogger.log(Level.INFO, mess);
                            }
                            ZMsg message = new ZMsg();
                            message.add(messRecv);
                            message.send(this.mainSocket);
                            message.destroy();
                        }
                    }
                }
                messageRecv.destroy();
            } catch (InterruptedException ie){
                ie.printStackTrace();
            }
        }
        
        if (stopped == true){
            this.fileThreadSocket.close();
            this.mainSocket.close();
        }
    }
    
    public void stopWorker(){
        stopped = true;
    }
}

class CountDownObject {
    private int count = 0;
    CountDownObject(int count){
        this.setCount(count);
    }
    public void setCount(int count) { this.count = count; }
    public int getCount() { return this.count; }
}

class TimerThread implements Runnable{
    private final static Logger tlogger = Logger.getLogger(TimerThread.class.getName());
    //boolean fool = false;
    //tlogger.setUseParentHandlers(fool);        // disable logging to the console
    private static FileHandler tlog = null;
    
    private volatile boolean stopped;
    public ZMQ.Socket socket;
    private Random rand = new Random();
    private int countDownSecs = rand.nextInt(60) + 1, threadFileID = rand.nextInt(100) + 1;
    private final CountDownObject obj =  new CountDownObject(countDownSecs);
    private String timeToSet = "";
    private int newTime = 0;
    
    TimerThread (ZMQ.Context context, String address){
        this.socket = context.socket(ZMQ.PAIR);
        socket.connect(address);
        this.init();
    } 

    public void init(){
        try{
            String THREADLOGFILENAME = "TimerThread" + this.threadFileID + ".log";
            tlog = new FileHandler(THREADLOGFILENAME, false);
        } catch ( SecurityException | IOException e ){
            e.printStackTrace();
        }
        
        tlog.setFormatter(new SimpleFormatter()); 
        tlogger.addHandler(tlog);
        tlogger.setLevel(Level.INFO);
    }
    
    @Override
    public void run(){
        while(!stopped){
            try{
                this.timeToSet = new String(socket.recv(0));
                if (this.timeToSet != ""){
                    newTime = Integer.parseInt(timeToSet);
                }
                synchronized(this.obj){
                    int step = this.obj.getCount();
                    step = step + newTime;
                    step--;
                    this.obj.setCount(step);
                    if (step == 0){
                        socket.send("TIMEOUT".getBytes(), 0);
                        String str = "TIMEOUT at " + new Date().toString();
                        tlogger.log(Level.INFO, str);
                        int newCountDownSecs = rand.nextInt(countDownSecs) + 1;
                        this.obj.setCount(newCountDownSecs);
                    }
                }
                Thread.sleep(300);
            } catch (InterruptedException ie){
                ie.printStackTrace();
            }
        }

        if (stopped == true){
            socket.close();
        }
    }

    public void stopWorker(){
        stopped = true;
    }
}

public class Peer extends Thread{
    private final Logger flogger = Logger.getLogger(Peer.class.getName());
    //boolean fool = false;
    //flogger.setUseParentHandlers(fool);        // disable logging to the console
    private static FileHandler flog = null;
    private String HOSTNAME = "", SERVER_ADDR = "";
    private Random rand = new Random();
    private int threadFileID = rand.nextInt(100) + 1;
    public boolean isNodeUp = false, stopPingThread = false, sentId = false;
    
    // Sockets 
    public ZMQ.Socket timerSocket,              // socket for the timer thread
                        raftSocket,             // socket for the raft thread
                        managementSocket,       // socket for the management client 
                        clusterRouterSocket,          // socket for sending/receiving messages to nodes in the cluster 
                        clsuterDealerSocket,          // socket for sending/receiving messages to nodes in the cluster 
                        heartbeatSubSocket,     // socket for subscribing to the publisher socket(heartbeats)
                        heartbeatReplySocket,   // socket for replying to heartbeats
                        shutdownClusterSocket;  // socket for shutdown sequence 
    public ZMQ.Context context;
    public ZMQ.Poller socketPoller;
   
    public TimerThread thr;                     // the thread for the timer process
    public RaftThread rft;                      // the thread for the raft process
    public HashMap<String, String> hostMap = new HashMap<>();     // the mapping (hostname to ip address) 
    public Utils utils;
    private String THREADLOGFILENAME = "";
   
    public void init(){
        try{
            this.THREADLOGFILENAME = "ClusterNode" + this.threadFileID + ".log";
            flog = new FileHandler(this.THREADLOGFILENAME, false);
        } catch ( SecurityException | IOException e ){
            e.printStackTrace();
        }
        
        flog.setFormatter(new SimpleFormatter()); 
        flogger.addHandler(flog);
        flogger.setLevel(Level.INFO);
    }
    
    Peer(){
        this.init();
        this.context = ZMQ.context(1);
        this.utils = new Utils();
        this.socketPoller = new ZMQ.Poller(5);
        this.setUpSockets();
    }
    
    
    private void setUpSockets(){
        this.timerSocket = this.context.socket(ZMQ.PAIR);
        this.timerSocket.bind("inproc://timerSocket");
        
        this.raftSocket = this.context.socket(ZMQ.PAIR);
        this.raftSocket.bind("inproc://raftSocket");
        
        this.managementSocket = this.context.socket(ZMQ.DEALER);
        this.HOSTNAME = System.getenv("HOSTNAME");
        this.managementSocket.setIdentity(this.HOSTNAME.getBytes());
        this.managementSocket.connect("tcp://clusterman:5100");
        
        this.clusterRouterSocket = this.context.socket(ZMQ.ROUTER);
        this.SERVER_ADDR = "tcp://" + this.HOSTNAME + ":6000";
        this.clusterRouterSocket.bind(this.SERVER_ADDR);
        
        this.clusterDealerSocket = this.context.socket(ZMQ.DEALER);
        this.clusterDealerSocket.connect(this.SERVER_ADDR);
        
        this.heartbeatSubSocket = this.context.socket(ZMQ.SUB);
        this.heartbeatSubSocket.connect("tcp://clusterman:5002");
        this.heartbeatSubSocket.subscribe("".getBytes());
        
        this.heartbeatReplySocket = this.context.socket(ZMQ.REQ);
        this.heartbeatReplySocket.connect("tcp://clusterman:5001");
        
        this.shutdownClusterSocket = this.context.socket(ZMQ.REQ);
        this.shutdownClusterSocket.bind("tcp://clusterman:5101");
        
        // Register most of the sockets
        this.socketPoller.register(this.heartbeatSubSocket, ZMQ.Poller.POLLIN);
        this.socketPoller.register(this.managementSocket, ZMQ.Poller.POLLIN);
        this.socketPoller.register(this.timerSocket, ZMQ.Poller.POLLIN);
        this.socketPoller.register(this.raftSocket, ZMQ.Poller.POLLIN);
        this.socketPoller.register(this.clusterDealerSocket, ZMQ.Poller.POLLIN);
        
        this.rft = new RaftThread(this.context, "inproc://raftSocket");
        this.thr = new TimerThread(this.context, "inproc://timerSocket");
    }
    
    
    private void tearDownSockets(){
        this.timerSocket.close();
        this.raftSocket.close();
        this.managementSocket.close();
        this.clusterDealerSocket.close();
        this.clusterRouterSocket.close();
        this.heartbeatSubSocket.close();
        this.heartbeatReplySocket.close();
        this.shutdownClusterSocket.close();
        this.context.term();
    }
    
    @Override
    public void run(){
        Thread raftProcess = new Thread(this.rft);
        Thread timerProcess = new Thread(this.thr);
        String nodeIdRecv = "";
        
        try{
            while(!Thread.currentThread().isInterrupted()){
                String message;
                ZMsg messageToSend, messageRecv;                
                
                // Poll for messages
                this.socketPoller.poll();
                
                // Responding to heartbeats
                if (this.socketPoller.pollin(0)){
                    message = new String(this.heartbeatSubSocket.recv(0));
                    String str = "Received: (" + message + ")";
                    flogger.log(Level.INFO, str);
                    this.heartbeatReplySocket.send("I am here".getBytes(), 0);
                    flogger.log(Level.INFO, "Sent: (I am here)");
                    message = new String(this.heartbeatReplySocket.recv(0));
                    str = "Received: (" + message + ")";
                    flogger.log(Level.INFO, str);
                    if ("I have seen you".equalsIgnoreCase(messageRecv) && isNodeUp == false){
                        str = "I am ready," + this.SERVER_ADDR;
                        this.managementSocket.send(str.getBytes(), 0);
                        str = "[Heartbeats]: Just sent(I am ready and " + this.SERVER_ADDR + ")"; 
                        flogger.log(Level.INFO,str);
                        isNodeUp = true;
                    }
                }
                
                // Responding to the commands from server
                if (this.socketPoller.pollin(1)){
                    message = new String(this.managementSocket.recv(0));
                    String [] messageParts = message.trim().split("\\s+");
                    if ("START".equalsIgnoreCase(messageParts[0])){
                        if (isNodeUp == true){
                            this.hostMap = (HashMap<String, String>) this.utils.convertFromBytes(this.managementSocket.recv(0));
                            for (Map.Entry<String, String> entry: hostMap.entrySet()){
                                String nodeId = entry.getKey();
                                String ipAddr = entry.getValue();
                                if (!ipAddr.equalsIgnoreCase(this.SERVER_ADDR)){
                                    this.clusterDealerSocket.connect(ipAddr);
                                }
                            }
                            raftProcess.start();
                            raftProcess.setNumOfVotesReq(this.hostMap.size());
                            timerProcess.start();
                        } else {
                            String str = "[Error]: Cluster has already started";
                            this.managementSocket.send(str.getBytes(), 0);
                            flogger.log(Level.WARNING, str);
                        }
                    }
                    
                    
                    if ("QUIT".equalsIgnoreCase(messageParts[0]) || "STOP".equalsIgnoreCase(messageParts[0])){
                        stopPingThread = true;
                        String str = "Received: " + messageParts[0]; 
                        flogger.log(Level.INFO, str);
                    }
                    
                    if ("QUERY".equalsIgnoreCase(messageParts[0])){
                        int numberToSend = Integer.parseInt(messageParts[1]);
                        ArrayList<String> stringsToSend = this.utils.readFromFile(numberToSend, this.THREADLOGFILENAME);
                        this.managementSocket.send(this.utils.convertToBytes(stringsToSend), 0);
                    }
                }
                
                // Responding to the timer thread 
                if (this.socketPoller.pollin(2)){
                    message = new String(this.timerSocket.recv(0));
                    messageToSend.add(message);
                    messageToSend.send(this.raftSocket);
                    messageToSend.destroy();
                }
                
                // Responding to the raft socket
                if (this.socketPoller.pollin(3)){
                    messageRecv = ZMsg.recvMsg(this.raftSocket);
                    message = new String(messageRecv.peekFirst().getData());
                    if ("VOTE:REQ".equalsIgnoreCase(message) || "SET LEADER".equalsIgnoreCase(message)){
                        for (String nodeId: hostMap.keySet()){
                            this.clusterRouterSocket.send(nodeId, ZMQ.SNDMORE);
                            messageRecv.send(this.clusterRouterSocket);
                        }
                        if ("SET LEADER".equalsIgnoreCase(message)){
                            this.managementSocket.send(message.getBytes(), 0);
                        }
                    } else if ("VOTE:REP".equalsIgnoreCase(message)){
                        this.clusterRouterSocket.send(nodeIdRecv, ZMQ.SNDMORE);
                        messageRecv.send(this.clusterRouterSocket);
                    } else if ("Changing state to FOLLOWER"){    
                        message = new String(messageRecv.popString());
                        flogger.log(Level.INFO, message);
                    } else {
                        // Log for the server/producer
                        byte [] messRecv = messageRecv.pollFirst();
                    }
                    messageRecv.destroy();
                }
                
                if (this.socketPoller.pollin(4)){
                    messageRecv = ZMsg.recvMsg(this.clusterDealerSocket);
                    messageRecv.send(this.raftSocket);
                    messageRecv.destroy();
                }
                
                if (stopPingThread == true){
                    this.shutdownClusterSocket.send("Shutting down!!!".getBytes(), 0);
                    message = new String(this.shutdownClusterSocket.recv(0));
                    System.out.println("[Shutdown]: Received (" + message + ")");
                    if ("GOODBYE".equalsIgnoreCase(message)){
                        raftProcess.stopWorker();
                        timerProcess.stopWorker();
                        Thread.currentThread().interrupt();
                    }
                }
            }
        
            if (Thread.currentThread().isInterrupted()){
                return;
            }
        } catch (InterruptedException ie){
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ie.printStackTrace(pw);
            System.out.println(sw.toString());
        }
        
        this.tearDownSockets();
    }
    
    public static void main(String [] args){                
        Peer peer = new Peer();
        peer.start();
    }
}