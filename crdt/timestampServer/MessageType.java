/**
 *  Copyright 2017 Okusanya Oluwadamilola
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
 
 
/**
 *  MessageSubType class 
 *  Used for determining type of message to be used by entities in the system
 */ 

public class MessageType{
    
    // Messages to be discarded
    public static enum Command{
        START,
        QUIT,
        STOP,
        EJECT,
        ADDSERVER,
        GETALLIDS,
        GETALLIDS_REP,
        GETLEADERID,
        GETLEADERID_REP,
        GETLOGS,
        GETLOGS_REP,
        CRDT,
        ACK,
        NACK,
        HEARTBEAT,
        SETPRIMARY,
        SETPRIMARY_REP,
        SETBACKUP,
        SETBACKUP_REP,
        SETSERVERS,
        GETSERVERS,
    }
    
    // Messages to be replicated
    public static enum Crdt{
        UPDATE,     // This sends out all the relevant updates for all the nodes
        CLIENTGET,
        CLIENTGET_REP,
        CLIENTPOST,
        CLIENTPOST_REP,
        SETPRIMARY,
        SETPRIMARY_REP,
        SETBACKUP,
        SETBACKUP_REP,
        GETSERVERS, 
        GETSERVERS_REP,
        HEARTBEAT       // used to check if the primary is alive
    }
}

