/**
 *  Copyright 2018 Okusanya Oluwadamilola
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
 
/**
 * Crdt node class
 * It uses the binary star pattern.
 * The binary star pattern is a pattern where there are two servers, one backup, one secondary.
 * Each node has five states: 
 * - primary(the main server that is waiting for client connections)
 * - backup (the secondary server that is waiting for client connections)
 * - active ( the main server that is accepting client connections)
 * - passive (the secondary server that is not accepting client connections)
 * - peer( a server that is neither primary, backup, active, passive)
 * Conversion between the various states will be done through a FSM.
 * 
 * The description in the ZMQ source code uses PUB-SUB. This will not do for CRDTs.
 * The algorihtm for this server is this:
 * - when the management client sends a START message, the management server collects all the addresses
 *  of all the servers in the network. All the servers start up as peer.
 * - the management server then designates the backup and primary servers.
 *  - the management server sends a EVENT_PEER_BACKUP message to a server, 
 * -  
*/

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Collection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.io.IOException;
import java.util.concurrent.ThreadLocalRandom;
import org.zeromq.ZMQ; 
import java.sql.TimeStamp;

public class TimeStampServer extends Thread{
    
    // Sockets
    private ZMQ.Socket manServerSocket,         // management client socket
        heartBeatRepSocket,         // socket to respond to heartbeats 
        heartBeatSubSocket,         
        clusterNodeSocket,              // socket to talk to the cluster nodes
        shutDownSocket;             // socket to shutdown the cluster nodes
        
    // Logging
    private final static String HOSTNAME = System.getenv("HOSTNAME");
    private final static String LOGFILENAME = "logs/TimeStampServer.log";
    private final static Logger logger = Logger.getLogger(TimeStampServer.class.getName());
    private static FileHandler handler = null;
        
    private final static String PUBLICNODENAME= "Cluster-" + HOSTNAME;      // used to connect to the managment server
    private final static String PRIVATENODENAME = "Raft-" + HOSTNAME;       // used to connect to the raft nodes
    private static String APPLOGFILENAME;
    
    private SystemState.ClusterState currentClusterState;  // this is mainly for shutdown and startup
    
    private ZMQ.Context context = ZMQ.context(1);
    private ZMQ.Poller poller;
    
    // Utils object
    private final Utils utils = new Utils();
        
    TimeStampServer(){
        this.clusterState = SystemState.ClusterState.DOWN;
        this.setUpSockets();
    }
    
    public static void startLogging(){
        try{
            handler = new FileHandler(LOGFILENAME, true);
        } catch ( SecurityException | IOException e){
            e.printStackTrace();
        }
        
        handler.setFormatter(new SimpleFormatter()); 
        logger.setUseParentHandlers(false);        // disable logging to the console
        logger.addHandler(handler);
        logger.setLevel(Level.INFO);
    }
    
    private void setUpSockets(){
        this.manServerSocket = this.context.socket(ZMQ.DEALER);
        this.manServerSocket.setIdentity(this.PUBLICNODENAME.getBytes());
        this.manServerSocket.connect(Constants.MANAGEMENT_SERVER_COMMAND_PORT);
        
        this.heartBeatSubSocket = this.context.socket(ZMQ.SUB);
        this.heartBeatSubSocket.connect(Constants.MANAGEMENT_SERVER_SUB_PORT);
        this.heartBeatSubSocket.subscribe("".getBytes());
        
        this.heartBeatRepSocket = this.context.socket(ZMQ.REQ);
        this.heartBeatRepSocket.connect(Constants.MANAGEMENT_SERVER_HEARTBEAT_PORT);
        
        this.shutDownSocket = this.context.socket(ZMQ.REQ);
        this.shutDownSocket.connect(Constants.MANAGEMENT_SERVER_SHUTDOWN_PORT);
        
        this.clusterNodeSocket = this.context.socket(ZMQ.ROUTER);
        this.clusterNodeSocket.bind(Constants.TIMESTAMP_SERVER_CLIENT_PORT);
    
        this.poller = new ZMQ.Poller(3);
        this.poller.register(this.manServerSocket, ZMQ.Poller.POLLIN);
        this.poller.register(this.heartBeatSubSocket, ZMQ.Poller.POLLIN);
        this.poller.register(this.clusterNodeSocket, ZMQ.Poller.POLLIN);
    }
    
    private void tearDownSockets(){
        this.shutDownSocket.close();
        this.heartBeatRepSocket.close();
        this.heartBeatSubSocket.close();
        this.clusterNodeSocket.close();
        this.manServerSocket.close();
        this.context.term();
    }
    
    private void printState(String message, String type){
        String strToWrite = "[STATE at ManagementServer ]: " + message;
        System.out.println(strToWrite);
        switch(type){
            case "INFO":
                logger.log(Level.INFO, strToWrite);
                break;
            case "WARNING":
                logger.log(Level.WARNING, strToWrite);
                break;
            case "SEVERE":
                logger.log(Level.SEVERE, strToWrite);
                break;
            default:
                System.out.println("Cannot log!!!!");
                break;
        }
    }
    
    @Override
    public void run(){
        try{
            while (!Thread.currentThread().isInterrupted()){
                Message message;
                String strMessage, strToLog, nodeId;
                
                MessageType.Command type;
                
                this.poller.poll();
                
                // connects to management server
                if (this.poller.pollin(0)){
                    message = (Message) this.utils.convertFromBytes(this.manServerSocket.recv(0));
                    type = message.getMessageType();
                    switch(type){
                        case START:
                            if (this.currentClusterState == SystemState.ClusterState.STARTED){
                                this.currentClusterState = SystemState.ClusterState.RUNNING;
                                this.printState(
                                    "Changing ClusterNodeState from STARTED to UP",
                                    "INFO",
                                    this.details.getPrivateNodeName()
                                );
                            } else {
                                this.printState(
                                    "This node is already started",
                                    "WARNING",
                                    this.details.getPrivateNodeName()
                                );
                            }
                            break;
                        case QUIT:
                            // Quitting 
                            this.shutDownSocket.send(
                                this.utils.convertToBytes(
                                    this.createMessageToSend(MessageType.Command.HEARTBEAT, 
                                        null, PUBLICNODENAME)
                                ), 0
                            );
                            this.printState( 
                                "Sent QUIT message ON [Node Shutdown Endpoint] TO (Management Server)", 
                                "INFO", 
                                this.details.getPrivateNodeName()
                            );
                            message = (Message) this.utils.convertFromBytes(this.shutDownSocket.recv(0));
                            strToLog = "MESSAGE RECEIVED: (" + message.getMessageType() + ") ON [Node Shutdown Endpoint] FROM (Management Server)";
                            this.printState(
                                strToLog, 
                                "INFO", 
                                this.details.getPrivateNodeName()
                            );
                            if ("QUIT".equalsIgnoreCase(message.getMessageType().name())){
                                Thread.currentThread().interrupt();
                            }
                            break;
                        default:
                            break;
                    }
                }
                
                // the socket that subscribes to the management server
                if (this.poller.pollin(1)){
                    
                }
                
                // connects to crdtnode
                if (this.poller.pollin(2)){
                    nodeId = new String(this.clusterNodeSocket.recv(0));
                    message = (Message) this.utils.convertFromBytes(this.clusterNodeSocket.recv(0));
                    strToLog = "Received timeStamp request from node=" + nodeid;
                    this.printState(
                        strToLog,
                        "INFO",
                        this.details.getPrivateNodeName();
                    );
                    NodeDetails details = (NodeDetails) messsage.getPayloadPart();
                    int term = (int) message.getPayloadPart();
                    TimeStamp timestamp = new TimeStamp(System.currentTimeMillis());
                    this.clusterNodeSocket,send(nodeId, ZMQ.SNDMORE);
                    this.shutDownSocket.send(
                        this.utils.convertToBytes(
                            this.createMessageToSend(MessageType.Command.Crdt, 
                                null, timeStamp, term)
                        ), 0
                    );
                }
                
            }
        } catch (Exception ie){
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ie.printStackTrace(pw);
            System.out.println(sw.toString());
        }
    }
}