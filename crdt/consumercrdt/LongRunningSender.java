/**
 *  Copyright 2017 Okusanya Oluwadamilola
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
 
/**
 *  LongRunningSender class
 *  Produces messages to be replicated by the raft node
*/

import java.util.Timer;
import java.util.TimerTask;
import org.zeromq.ZMQ;
import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class LongRunningSender implements Runnable{

    public final Message message = new Message(MessageType.Command.HEARTBEAT);
    private final int timeOut = 10000;
    private volatile boolean stopped;
    public ZMQ.Socket socket;
    public final Utils utils = new Utils();
    private Timer producerTimer;
    
    public final static Logger logger = Logger.getLogger(LongRunningSender.class.getName());
    public static FileHandler handler;
    private static String LOGFILENAME;
    public static String LOGFILENAMEID;
    
    LongRunningSender(ZMQ.Context context, String FILEID){
        this.socket = context.socket(ZMQ.PAIR);
        this.socket.connect(Constants.LONGRUNNING_SENDER_PORT);
        this.LOGFILENAMEID = FILEID;
    }
    
    public static void startLogging(){
        try{
            LOGFILENAME = "logs/LongRunningSender_" + LOGFILENAMEID + ".log";
            handler = new FileHandler(LOGFILENAME, false);
        } catch ( SecurityException | IOException e ){
            e.printStackTrace();
        }
        
        handler.setFormatter(new SimpleFormatter());
        logger.setUseParentHandlers(false);
        logger.addHandler(handler);
        logger.setLevel(Level.INFO);
    }
    
    private void startSendingHeartbeats(){
        if (this.producerTimer == null){
            TimerTask timerTask = new TimerTask() {
                @Override
                public void run(){
                    socket.send(utils.convertToBytes(message), 0);
                }
            };
            this.producerTimer = new Timer();
            this.producerTimer.scheduleAtFixedRate(timerTask, 0, this.timeOut);
        }
    }
    
    private void stopSendingHeartBeats(){
        if (this.producerTimer != null){
            this.producerTimer.cancel();
            this.producerTimer.purge();
            this.producerTimer = null;
        }
    }
    
    @Override
    public void run(){
        startLogging();
        String str = "[LongRunningSender]: Starting Heartbeating";
        System.out.println(str);
        logger.log(Level.INFO, str);
        while(stopped == false){
            Message messageRecv = (Message) this.utils.convertFromBytes(this.socket.recv(0));
            if (messageRecv != null){
                MessageType.Command type = messageRecv.getMessageType();
                str = "Received " + type.name();
                System.out.println(str);
                logger.log(Level.INFO, str);
                switch(type){
                    case QUIT:
                        this.stopSendingHeartBeats();
                        break;
                    case START:
                        this.startSendingHeartbeats();
                        break;
                    default:
                        System.out.println("This is not recognised");
                        break;
                }
            }
        }
    }
    
    public void stopWorker(){
        this.stopped = true;
    }
}