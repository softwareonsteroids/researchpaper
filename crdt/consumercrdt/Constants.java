/**
 *  Copyright 2017 Okusanya Oluwadamilola
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
 
/**
 *  Constants class
 *  Class that contains all the constants used in the system
 */ 

public class Constants{

    static final String MANAGEMENT_SERVER_COMMAND_PORT="tcp://managementservercrdt:7000";
    static final String MANAGEMENT_SERVER_CLUSTER_PORT="tcp://*:7000";
    
    static final String MANAGEMENT_SERVER_SUB_PORT="tcp://managementservercrdt:7100";
    static final String MANAGEMENT_SERVER_PUB_PORT="tcp://*:7100";
    
    static final String MANAGEMENT_SERVER_HEARTBEAT_PORT="tcp://managementservercrdt:7050";
    static final String MANAGEMENT_SERVER_HEARTBEAT_RCV_PORT="tcp://*:7050";
    
    static final String MANAGEMENT_SERVER_SHUTDOWN_PORT="tcp://managementservercrdt:7150";
    static final String MANAGEMENT_SERVER_SHUTDOWN_RCV_PORT="tcp://*:7150";
    
    static final String MANAGEMENT_CLIENT_SERVER_PORT="tcp://managementservercrdt:6900";
    static final String MANAGEMENT_SERVER_CLIENT_PORT="tcp://*:6900";
    
    static final String CRDTCLUSTER_CRDTNODE_RECV_PORT="tcp://*:7200";
    
    static final String FILESERVER_PORT="inproc://filethreadSocket";
    static final String TIMERSERVER_PORT="inproc://timerthreadSocket";
    static final String LONGRUNNING_SENDER_PORT="inproc://longrunningsenderSocket";
}
