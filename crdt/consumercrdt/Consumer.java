/**
 *  Copyright 2017 Okusanya Oluwadamilola
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
 
/**
 *  Producer class
 *  Produces messages to be replicated by the raft node
 *  For every message, it receives an acknowledgement message
 */
 
import java.io.PrintWriter;
import java.io.StringWriter; 
import java.io.BufferedReader;
import java.io.BufferedWriter; 
import java.io.IOException;
import org.zeromq.ZMQ;
import java.util.UUID;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Map;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Collections;
import java.util.ArrayList;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.Formatter;
import java.util.Locale;

class Pair<T, K> {
    
    private T first;
    private K second;
    
    public Pair(T first, K second){
        this.first = first;
        this.second = second;
    }
    
    public T getFirst(){
        return first;
    }
    
    public K getSecond(){
        return second;
    }
}

  
public class Consumer extends Thread{
    
    private final static String HOSTNAME = System.getenv("HOSTNAME");
    private ArrayList<NodeDetails> clusterCrdtNodes;
    public HashMap<String, ZMQ.Socket> negotationSockets;
    private ArrayList<Pair<Long, Pair<Long, Long>>> statsMessages;  
    public NodeDetails details;
    public int counter; 
    private int nextNodeToTry;   
    private SystemState.ClusterState currentClusterState;   // this is mainly for shutdown and startup
    private static String PUBLICNODENAME = "Cluster-" + HOSTNAME;
    private final static String PRIVATENODENAME = "Consumer-" + HOSTNAME;       // used to connect to the crdt nodes
    private static String APPLOGFILENAME;
    private String primaryServerId = "";
    private String backupServerId = "";
    private String oldBackupServerId = "";
    private boolean hasSentNewUpdate = false;
    private boolean hasSentResetToManagementServer = false;
    private boolean resendUpdate = false;
    
    public int waitCount;
    public final int WAITCOUNT = 30;
    public final int OTHERWAITCOUNT = 20;
    
    public int numberOFMessagesReceived;
    public int numberOfMessagesToTest;
    private String tagID;
    
    /** 
     * Sockets that belongs to the thread/node 
     *
     * - A socket that connects to management server. 
     *      - For sending/receiving messages to the management server.
     *      - Socket is async.  
     * - A socket that connects to management server.
     *      - For receiving heartbeat messages
     * - A socket that connects to the management server.
     *      - For sending receipt of the heartbeat messages
     * - A socket that connects to the raft nodes.
     *      - For sending messages to the raft node
     *      - Socket is async
     */
     
    private ZMQ.Socket manServerSocket,
        heartBeatSubSocket,
        heartBeatRepSocket,
        innerThreadSocket,
        shutDownSocket;        
    
    private ZMQ.Context context = ZMQ.context(1);
    private ZMQ.Poller poller;
    
    // Utils object
    public final Utils utils = new Utils();
    
    // Logger
    public final static Logger logger = Logger.getLogger(Consumer.class.getName());
    public static FileHandler handler;
    
    // Sender object
    private LongRunningSender sender;
    
    Consumer(){
        this.currentClusterState = SystemState.ClusterState.DOWN;
        
        this.details = new NodeDetails.NodeDetailsBuilder(false)
                            .setPublicNodeName(PUBLICNODENAME)
                            .setPrivateNodeName(PRIVATENODENAME)
                            .build();
        this.poller = new ZMQ.Poller(4);
        this.counter = 0; 
        this.nextNodeToTry = 0;
        this.tagID = "";
        this.numberOFMessagesReceived = 0;
        this.numberOfMessagesToTest = 0;
        this.statsMessages = new ArrayList<>();
        this.clusterCrdtNodes = new ArrayList<>();
        this.negotationSockets = new HashMap<>();
        this.setUpSockets();
    }
    
    public static void startLogging(){
        try{
            APPLOGFILENAME = "logs/" + PUBLICNODENAME + ".log";
            handler = new FileHandler(APPLOGFILENAME, true);
        } catch ( SecurityException | IOException e ){
            e.printStackTrace();
        }
        
        System.out.println("===============================================");
        String strToWrite = "This is " + PUBLICNODENAME + "'s or the consumer's log";;
        System.out.println(strToWrite);
        System.out.println("===============================================");
        handler.setFormatter(new SimpleFormatter());
        logger.setUseParentHandlers(false);
        logger.addHandler(handler);
        logger.setLevel(Level.INFO);
    }
    
    public void setUpSockets(){
        this.manServerSocket = this.context.socket(ZMQ.DEALER);
        this.manServerSocket.setIdentity(this.details.getPublicNodeName().getBytes());
        this.manServerSocket.connect(Constants.MANAGEMENT_SERVER_COMMAND_PORT);
        
        this.heartBeatSubSocket = this.context.socket(ZMQ.SUB);
        this.heartBeatSubSocket.connect(Constants.MANAGEMENT_SERVER_SUB_PORT);
        this.heartBeatSubSocket.subscribe("".getBytes());
        
        this.heartBeatRepSocket = this.context.socket(ZMQ.REQ);
        this.heartBeatRepSocket.connect(Constants.MANAGEMENT_SERVER_HEARTBEAT_PORT);
        
        this.shutDownSocket = this.context.socket(ZMQ.REQ);
        this.shutDownSocket.connect(Constants.MANAGEMENT_SERVER_SHUTDOWN_PORT);
        
        this.innerThreadSocket = this.context.socket(ZMQ.PAIR);
        this.innerThreadSocket.bind(Constants.LONGRUNNING_SENDER_PORT);
		
        this.poller.register(this.manServerSocket, ZMQ.Poller.POLLIN);
        this.poller.register(this.heartBeatSubSocket, ZMQ.Poller.POLLIN);
        this.poller.register(this.innerThreadSocket, ZMQ.Poller.POLLIN);
        
        this.sender = new LongRunningSender(this.context, HOSTNAME);
    }
    
    private void tearDownSockets(){
        this.manServerSocket.close();
        this.heartBeatRepSocket.close();
        this.heartBeatSubSocket.close();
        this.innerThreadSocket.close();
        this.tearDownNegotationSockets();
        this.context.term();
    }
    
    
    private void printStats(){
        final StringBuilder sb = new StringBuilder();
        
        /** Investigate Formatter class
        //final Formatter formatter = new Formatter(sb, Locale.US);
        
        //formattter.formatt(
        **/
        
        sb.append(System.lineSeparator());
        sb.append("\t\t\t\t\t\tSTATS");
        sb.append(System.lineSeparator());
        sb.append("+======================================================+");
        sb.append(System.lineSeparator());
        //sb.append("Total received messages: ");
        //sb.append(this.numberOFMessagesReceived);
        sb.append(System.lineSeparator());
        sb.append(System.lineSeparator());
        int count = 1;
        sb.append("\t\tTimeMessageWasReplicated TimeMessageWasCreatedAtProducer DifferenceBtwProducerAndReplication TimeReceivedAtConsumer DifferenceBtwProducerAndConsumer");
        sb.append(System.lineSeparator());
        sb.append("\t\t------------------------ ------------------------------- ---------------------- ----------");
        sb.append(System.lineSeparator());
        
        long averageTimeReplicatedAtCrdt = 0L;
        long averageTimeGottenAtConsumer = 0L;
        for (Pair<Long, Pair<Long, Long>> item: this.statsMessages){
            String str = String.valueOf(count) + "\t" + String.valueOf(item.getFirst());
            sb.append(str);
            Pair<Long, Long> pair = item.getSecond();
            averageTimeReplicatedAtCrdt += item.getFirst() - pair.getFirst();
            averageTimeGottenAtConsumer += pair.getSecond() - pair.getFirst();
            str = "\t" + String.valueOf(pair.getFirst()) + " ms\t\t\t" + String.valueOf(item.getFirst() - pair.getFirst()) 
                + " ms\t\t\t" + String.valueOf(pair.getSecond()) 
                + " ms\t\t\t" + String.valueOf(pair.getSecond() - pair.getFirst()) + "ms";
            sb.append(str);
            sb.append(System.lineSeparator());
            ++count;
        }
        
        sb.append(System.lineSeparator());
        sb.append("AverageTimeGottenAtConsumer = " + String.valueOf((long) averageTimeGottenAtConsumer/count) + " ms");
        sb.append("AverageTimeGottenAtCrdt = " + String.valueOf((long)averageTimeReplicatedAtCrdt/count) + " ms");
        this.printState(sb.toString(), "INFO", this.details.getPrivateNodeName());
    }
    
    private void printPeers(){
        String strToLog = "Current total num of raftNodes: " + this.clusterCrdtNodes.size(); 
        this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
        this.printState("Nodes that are part of this cluster: ", "INFO", this.details.getPrivateNodeName());
        for (NodeDetails details: this.clusterCrdtNodes){
            this.printState(details.toString(), "INFO", this.details.getPrivateNodeName());
        }
    }
    
    private void setUpNegotiationSockets(){
        for (NodeDetails item: this.clusterCrdtNodes){
            if (!item.getPrivateNodeName().equalsIgnoreCase(this.details.getPrivateNodeName())){
                ZMQ.Socket socket = this.context.socket(ZMQ.DEALER);
                String socketname = UUID.randomUUID().toString();
                String logStr = "This is the dealer socket identity=" +
                    socketname + " on this node that is bound to the router socket of node=(" +
                    item.getPrivateNodeName() + ")";
                this.printState(logStr, "INFO", this.details.getPrivateNodeName());
                socket.setIdentity(socketname.getBytes());
                socket.connect(item.getIPAddress());
                this.negotationSockets.put(item.getPrivateNodeName(), socket);
            }
        }
    }
    
    private void printState(String message, String type, String privateName){
        String strToWrite = "[STATE at " + privateName +  "]: " + message;
        System.out.println(strToWrite);
        switch(type){
            case "INFO":
                logger.log(Level.INFO, strToWrite);
                break;
            case "WARNING":
                logger.log(Level.WARNING, strToWrite);
                break;
            default:
                System.out.println("Cannot log!!!!");
                break;
        }
    }
    
    public Message createMessageToSend(MessageType.Command messageType, 
        MessageType.Crdt crdtType, Object ... args){
        Message messageToSend = new Message(messageType);
        if (crdtType != null){
            messageToSend.setPayloadType(crdtType);
        }
        if (args != null){
            for (Object arg: args){
                messageToSend.addPayloadPart(arg);
            }
        }
        return messageToSend;
    }
    
    private void tearDownNegotationSockets(){
        for (ZMQ.Socket socket: this.negotationSockets.values()){
            socket.close();
        }
    }
    
    private void processEject(String nodeId){
        Iterator<NodeDetails> it = this.clusterCrdtNodes.iterator();
        while(it.hasNext()){
            NodeDetails item = it.next();
            if (item.getPublicNodeName().equalsIgnoreCase(nodeId)){
                String strToLog = "Found node to eject=" + item.getPrivateNodeName();
                this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                ZMQ.Socket socket = this.negotationSockets.get(item.getPrivateNodeName());
                socket.close();
                this.negotationSockets.remove(item.getPrivateNodeName());
                this.printState("Removing socket", "INFO", this.details.getPrivateNodeName());
                it.remove();
            }
        }
        this.printPeers();
    }
    
    private void checkForNodesToTry(String start){
        NodeDetails node;
        ZMQ.Socket socket;
        String str;
        if (start.isEmpty()) {
            // Unregister that socket
            // Work backwards
            int lastnodeTried = ( ( (this.nextNodeToTry - 1) % this.clusterCrdtNodes.size() )
                + this.clusterCrdtNodes.size() ) % this.clusterCrdtNodes.size();
            str = "Index tried: " + lastnodeTried + " of " + this.clusterCrdtNodes.size();
            this.printState(str, "INFO", this.details.getPrivateNodeName());
            node = this.clusterCrdtNodes.get(lastnodeTried);
            this.printState("This is the node that is being removed: " + node.getPrivateNodeName(), "INFO", this.details.getPrivateNodeName());
            socket = this.negotationSockets.get(node.getPrivateNodeName());
            this.poller.unregister(socket);
        }
            
        str = "Current index to try: " + this.nextNodeToTry + " of " + this.clusterCrdtNodes.size();
        this.printState(str, "INFO", this.details.getPrivateNodeName());
        node = this.clusterCrdtNodes.get(this.nextNodeToTry);
        this.printState("This is the node to try: " + node.getPrivateNodeName(), "INFO", this.details.getPrivateNodeName());
        socket = this.negotationSockets.get(node.getPrivateNodeName());
        this.poller.register(socket, ZMQ.Poller.POLLIN);
        this.nextNodeToTry = (this.nextNodeToTry + 1) % this.clusterCrdtNodes.size();
        
        str = "Next index to try: " + this.nextNodeToTry + " of " + this.clusterCrdtNodes.size();
        this.printState(str, "INFO", this.details.getPrivateNodeName());        
                
        Message message = this.createMessageToSend(MessageType.Command.CRDT,
                MessageType.Crdt.GETSERVERS, "", PeerConnectionState.ServerConnectionEvent.CLIENT_REQUEST);
        
        message.setNodeIDFrom(new String(socket.getIdentity()));
        
        if (socket != null){
                socket.send(this.utils.convertToBytes(message),0);
        }
        
        this.printState("Sending [GETSERVERS] to" + node.getPrivateNodeName() , "INFO", this.details.getPrivateNodeName()); 
    }
    
    
    
    @Override
    public void run(){
        Thread thrd = new Thread(this.sender);
    
        try{
            while(!Thread.currentThread().isInterrupted()){
                try{
                    Message message;
                    String strMessage, strToLog;
                    MessageType.Command type;
                    ZMQ.Socket socket;
                    
                    this.poller.poll();
                    
                    if (this.poller.pollin(0)){
                        message = (Message) this.utils.convertFromBytes(this.manServerSocket.recv(0));
                        type = message.getMessageType();
                        switch(type){
                            case START:
                                if (this.currentClusterState == SystemState.ClusterState.STARTED){
                                    this.currentClusterState = SystemState.ClusterState.RUNNING;
                                    this.printState("Changing ClusterNodeState from STARTED to RUNNING", 
                                        "INFO", 
                                        this.details.getPrivateNodeName()
                                    );
                                    this.clusterCrdtNodes = (ArrayList<NodeDetails>) message.getPayloadPart();
                                    this.printState("Received this clusterOfNodes", 
                                        "INFO", 
                                        this.details.getPrivateNodeName()
                                    );
                                    this.printPeers();
                                    this.setUpNegotiationSockets();
                                    this.checkForNodesToTry("START");
                                    thrd.start();
                                    this.innerThreadSocket.send(this.utils.convertToBytes(new Message(type)), 0);
                                    break;
                                } else {
                                    strToLog = "Received START when node is already started";
                                    this.printState(strToLog, "WARNING", this.details.getPrivateNodeName());
                                }
                                break;
                            case ADDSERVER:
                                NodeDetails dets = (NodeDetails) message.getPayloadPart();
                                this.clusterCrdtNodes.add(dets);
                                this.printPeers();
                                ZMQ.Socket newSocket = this.context.socket(ZMQ.DEALER);
                                String newSocketname = UUID.randomUUID().toString();
                                strToLog = "This is the dealer socket identity=" +
                                    newSocketname + " on this node that is bound to the router socket of node=(" +
                                    dets.getPrivateNodeName() + ")";
                                this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                                newSocket.setIdentity(newSocketname.getBytes());
                                newSocket.connect(dets.getIPAddress());
                                this.negotationSockets.put(dets.getPrivateNodeName(), newSocket);
                                this.printState("Added new server: " + dets.getPrivateNodeName(), "INFO", this.details.getPrivateNodeName());
                                break;    
                            case QUIT:
                            case STOP:
                                // Quitting 
                                this.shutDownSocket.send(
                                    this.utils.convertToBytes(
                                        this.createMessageToSend(MessageType.Command.HEARTBEAT, 
                                            null, PUBLICNODENAME)
                                    ), 0
                                );
                                this.printState( 
                                    "Sent QUIT message ON [Node Shutdown Endpoint] TO (Management Server)", 
                                    "INFO", 
                                    this.details.getPrivateNodeName()
                                );
                                message = (Message) this.utils.convertFromBytes(this.shutDownSocket.recv(0));
                                strToLog = "MESSAGE RECEIVED: (" + message.getMessageType() + ") ON [Node Shutdown Endpoint] FROM (Management Server)";
                                this.printState(
                                    strToLog, 
                                    "INFO", 
                                    this.details.getPrivateNodeName()
                                );
                                if ("QUIT".equalsIgnoreCase(message.getMessageType().name())){
                                    this.innerThreadSocket.send(
                                        this.utils.convertToBytes(
                                            this.createMessageToSend(MessageType.Command.QUIT, null)
                                        ), 0
                                    );
                                    this.printStats();
                                    this.printState(
                                        "Sending QUIT to fileThread", 
                                        "INFO", 
                                        this.details.getPrivateNodeName()
                                    );
                                    Thread.currentThread().interrupt();
                                }
                                break;
                            case SETGROUPID:
                                this.tagID = (String) message.getPayloadPart();
                                this.numberOfMessagesToTest = (int) message.getPayloadPart();
                                strToLog = "Just set the tag id=" + this.tagID + " to test for numOfMessages=" + String.valueOf(this.numberOfMessagesToTest);
                                this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                                break;
                            case EJECT:
                                String nodeIdToEject = (String) message.getPayloadPart();
                                this.processEject(nodeIdToEject);
                                strToLog = "Ejecting nodeId=" + nodeIdToEject + " from list of nodes";
                                this.printState(
                                    strToLog, 
                                    "INFO", 
                                    this.details.getPrivateNodeName()
                                );
                                break;
                            default:
                                this.printState("This command is not recognised", 
                                    "WARNING",
                                    this.details.getPrivateNodeName()
                                );
                                break;
                        }
                        strToLog = "PROCESSING END: (Command Type: " + type.name() + ")";
                        this.printState(strToLog, 
                            "INFO",
                            this.details.getPrivateNodeName()
                        );
                    }
                    
                    if (this.poller.pollin(1)){
                        message = (Message) this.utils.convertFromBytes(this.heartBeatSubSocket.recv(0));
                        strToLog = "MESSAGE RECEIVED: (" + (String) message.getPayloadPart() + ") ON [Subs Socket] FROM (Management server)";
                        this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                        
                        // Communicating with the management server
                        
                        this.heartBeatRepSocket.send(
                            this.utils.convertToBytes(
                                this.createMessageToSend(MessageType.Command.HEARTBEAT, 
                                    null, PUBLICNODENAME)
                            ), 0
                        );
                        this.printState("MESSAGE SENT: (" + PUBLICNODENAME + ") ON [Node Rep Endpoint] TO (Management Server)", 
                                "INFO", this.details.getPrivateNodeName());
                        message = (Message) this.utils.convertFromBytes(this.heartBeatRepSocket.recv(0));
                        strMessage = (String) message.getPayloadPart();
                        strToLog = "MESSAGE RECEIVED: (" + strMessage + ") ON [Node Rep Endpoint] FROM (Management Server)";
                        this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                        
                        if ("I have seen you".equalsIgnoreCase(strMessage) && this.currentClusterState == SystemState.ClusterState.DOWN){
                            this.manServerSocket.send(
                                this.utils.convertToBytes(
                                    this.createMessageToSend(MessageType.Command.HEARTBEAT,
                                        null, this.details)
                                ),0
                            );
                            strToLog = "MESSAGE SENT: (" + 
                                    this.details + 
                                    ") ON [Node Public Endpoint] TO (Management Server)";
                            this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                            this.currentClusterState = SystemState.ClusterState.STARTED;
                            this.printState("Changing ClusterState from DOWN to STARTED", 
                                "INFO", this.details.getPrivateNodeName());
                        } else if ("I have seen you".equalsIgnoreCase(strMessage) && this.currentClusterState == SystemState.ClusterState.STARTED && this.clusterCrdtNodes.isEmpty() == true){
                            // This is on the rare chance the managmenet server receives its heartbeat 
                            // before the management server sends the entire cluster of raftnodes to the 
                            // raftnodes themselves
                            this.printState("Node is at ClusterState STARTED", 
                                "INFO", this.details.getPrivateNodeName());
                            this.manServerSocket.send(
                                this.utils.convertToBytes(
                                    this.createMessageToSend(MessageType.Command.HEARTBEAT,
                                        null, this.details)
                                ),0
                            );
                            strToLog = "MESSAGE SENT: (" + 
                                    this.details + 
                                    ") ON [Node Public Endpoint] TO (Management Server)";
                            this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                        }
                    }
                    
                    if (this.poller.pollin(2)){
                        message = (Message) this.utils.convertFromBytes(this.innerThreadSocket.recv(0));
                        if (this.primaryServerId.trim().length() != 0 && this.hasSentNewUpdate == false){
                            if (this.resendUpdate == false){
                                ++this.counter;
                                this.printState("New message to ask for: (" + this.counter + ")", "INFO", this.details.getPrivateNodeName());
                                if (this.waitCount > 0){
                                    this.waitCount = 0;
                                }
                                System.out.println("hasSentNewUpdate: " + this.hasSentNewUpdate);
                                
                                message = this.createMessageToSend(MessageType.Command.CRDT,
                                        MessageType.Crdt.CLIENTGET, this.counter);
                                socket = this.poller.getSocket(3);
                                message.setNodeIDFrom(new String(socket.getIdentity()));
                                socket.send(this.utils.convertToBytes(message), 0);
                                strToLog =  " Sent new message=(" + this.counter + ") at timestamp=" + System.currentTimeMillis() + " to node=[" + this.primaryServerId + "]";
                                this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                                this.hasSentNewUpdate = true;
                            } else if (this.resendUpdate == true){
                                if (this.counter > 1){
                                    --this.counter;
                                }
                                if (this.waitCount > 0){
                                    this.waitCount = 0;
                                }
                                System.out.println("hasSentNewUpdate with resendUpdate == true: " + this.hasSentNewUpdate);
                                
                                message = this.createMessageToSend(MessageType.Command.CRDT,
                                        MessageType.Crdt.CLIENTGET, this.counter);
                                socket = this.poller.getSocket(3);
                                message.setNodeIDFrom(new String(socket.getIdentity()));
                                socket.send(this.utils.convertToBytes(message), 0);
                                strToLog =  " Sent new message=(" + this.counter + ") at timestamp=" + System.currentTimeMillis() + " to node=[" + this.primaryServerId + "]";
                                this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                                this.resendUpdate = false;
                            }
                        } else if (this.backupServerId.trim().length() != 0 && this.oldBackupServerId.trim().length() == 0) {
                            this.waitCount++;
                            this.printState("Trying to connect to failed primary server... in " + this.waitCount, "INFO", this.details.getPrivateNodeName());
                            if (this.waitCount == this.WAITCOUNT){
                                this.waitCount = 0;
                                socket = this.negotationSockets.get(this.primaryServerId);
                                this.poller.unregister(socket);
                                ZMQ.Socket newSocket = this.negotationSockets.get(this.backupServerId);
                                this.poller.register(newSocket);
                                
                                this.printState("Asking backup server=" + this.backupServerId + " for new servers", "INFO", this.details.getPrivateNodeName());
                                
                                this.processEject(this.primaryServerId);

                                this.primaryServerId = "";
                                this.oldBackupServerId = this.backupServerId;
                                this.backupServerId = "";
                                
                                this.printState("Sending getServers message to backup server", "INFO", this.details.getPrivateNodeName()); 
                                
                                message = this.createMessageToSend(MessageType.Command.CRDT,
                                    MessageType.Crdt.GETSERVERS, "None", PeerConnectionState.ServerConnectionEvent.CLIENT_REQUEST);
                                message.setNodeIDFrom(new String(newSocket.getIdentity()));
                                if (newSocket != null){
                                    newSocket.send(this.utils.convertToBytes(message),0);
                                }
                            }
                        }  else if ("".equalsIgnoreCase(this.backupServerId) && "".equalsIgnoreCase(this.primaryServerId)){
                            this.waitCount++;
                            if (this.hasSentResetToManagementServer == true){
                                this.checkForNodesToTry("");
                            }
                            if (this.waitCount == this.OTHERWAITCOUNT){
                                // this means that the backup server has failed
                                this.waitCount = 0;
                                this.printState("Asking management to select new servers", "INFO", this.details.getPrivateNodeName());
                                socket = this.negotationSockets.get(this.oldBackupServerId);
                                this.poller.unregister(socket);
                                // Connect to the management server and ask it to give you two new sockets
                                this.processEject(this.oldBackupServerId);
                                
                                this.manServerSocket.send(
                                    this.utils.convertToBytes(
                                        this.createMessageToSend(MessageType.Command.GETSERVERS, null)
                                    ),0
                                );
                                this.checkForNodesToTry("");
                                this.hasSentResetToManagementServer = true;
                            } 
                        }
                    }
                    
                    
                    if (this.currentClusterState == SystemState.ClusterState.RUNNING){
                        if (this.poller.pollin(3)){
                            socket = this.poller.getSocket(3);
                            message = (Message) this.utils.convertFromBytes(socket.recv(0));
                            type = message.getMessageType();
                            strToLog = "Received a message of type=[" + type.name() + "] from the node=[" + message.getNodeIDFrom() + "]";
                            this.printState(
                                strToLog,
                                "INFO",
                                this.details.getPrivateNodeName()
                            );
                            switch(type){
                                case SETSERVERS:
                                    MessageType.Command responsetype = (MessageType.Command) message.getPayloadPart();
                                    switch (responsetype){
                                        case ACK:
                                            this.primaryServerId = (String) message.getPayloadPart();
                                            this.backupServerId = (String) message.getPayloadPart();
                                            // Register the socket to send to, to be the orimary server id
                                            this.poller.unregister(socket);
                                            ZMQ.Socket newSocket = this.negotationSockets.get(this.primaryServerId);
                                            this.poller.register(newSocket);
                                            
                                            strToLog = "Found the primary server=(" + this.primaryServerId + "), secondary server=(" + this.backupServerId + ").Connecting now...";
                                            this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                                            this.hasSentNewUpdate = false;
                                            this.hasSentResetToManagementServer = false;
                                            this.oldBackupServerId = "";
                                            break;
                                        case NACK:
                                            if (this.primaryServerId.trim().length() == 0 && this.backupServerId.trim().length() == 0){
                                                // Initial connection
                                                if (this.oldBackupServerId.trim().length() != 0){
                                                    this.printState("Still asking backup server for main servers", "INFO", this.details.getPrivateNodeName());
                                                    message = this.createMessageToSend(MessageType.Command.CRDT,
                                                        MessageType.Crdt.GETSERVERS, "", PeerConnectionState.ServerConnectionEvent.CLIENT_REQUEST);
                                                    message.setNodeIDFrom(new String(socket.getIdentity()));
                                                    if (socket != null){
                                                            socket.send(this.utils.convertToBytes(message),0);
                                                    }
                                                } else {
                                                    this.printState("Did not find main servers.Retrying..", "INFO", this.details.getPrivateNodeName());
                                                    this.checkForNodesToTry("");
                                                }
                                            } else {
                                                strToLog = "Received NACK when main servers are known.Disregarding..";
                                                this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                                            }
                                            break;
                                    }
                                    break;
                                case CRDT:
                                    MessageType.Crdt crdtType = message.getPayloadType();
                                    this.printState("Processing message of crdtype=" + crdtType.name(), "INFO", this.details.getPrivateNodeName());
                                    switch(crdtType){
                                        case CLIENTGET_REP:
                                            MessageType.Command cmdType = (MessageType.Command) message.getPayloadPart();
                                            switch(cmdType){
                                                case ACK:
                                                    if (waitCount > 0){
                                                        waitCount = 0;
                                                    }
                                                    LWWSetValue<Integer> valueReceived = (LWWSetValue<Integer>) message.getPayloadPart();
                                                    long timeReceivedAtConsumer = System.currentTimeMillis();
                                                    strToLog = "Value received is=(" + valueReceived.getElement() + ") at timeStamp=(" + valueReceived.getTimestampInLong() + ")";
                                                    if (valueReceived.getGroupId().trim().length() > 0 && valueReceived.getGroupId().equalsIgnoreCase(this.tagID)){
                                                        // checking for the groupId 
                                                        
                                                        long timeMessageWasCreatedAtProducer = valueReceived.getProducerTimestamp();
                                                        long timeMessageWasReplicated = valueReceived.getTimestampInLong();
                                                        Pair<Long, Long> innerPair = new Pair(timeMessageWasCreatedAtProducer, timeReceivedAtConsumer);
                                                        Pair<Long, Pair<Long,Long>> outerpair = new Pair<>(timeMessageWasReplicated, innerPair);
                                                        this.statsMessages.add(outerpair);
                                                        --this.numberOfMessagesToTest;
                                                        this.printState("Countdown to printStats=" + this.numberOfMessagesToTest, "INFO", this.details.getPrivateNodeName());
                                                        if (this.numberOfMessagesToTest == 0){
                                                            this.tagID = "";
                                                            this.printStats();
                                                        }
                                                    }
                                                    this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                                                    this.hasSentNewUpdate = false;
                                                    break;
                                                case NACK:
                                                    this.printState("Item=("+ this.counter +") was not found on cluster", "INFO", this.details.getPrivateNodeName());
                                                    this.hasSentNewUpdate = false;
                                                    this.resendUpdate = true;
                                                    break;
                                            }
                                            break;
                                        default:
                                            this.printState("This command is not recognised", 
                                                "WARNING",
                                                this.details.getPrivateNodeName()
                                            );
                                            break;
                                    }
                                    break;
                                default:
                                    this.printState("This command is not recognised", 
                                        "WARNING",
                                        this.details.getPrivateNodeName()
                                    );
                                    break;
                            }
                            strToLog = "Processing end of message of type=( " + type.name() + ")";
                            this.printState(strToLog, 
                                "INFO",
                                this.details.getPrivateNodeName()
                            );
                        } 
                        
                    }
                    Thread.sleep(2000);
                } catch (InterruptedException ie){
                    ie.printStackTrace();
                }
            }
                
            if (Thread.currentThread().isInterrupted()){
                return;
            }
            
        } catch (Exception ie){
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ie.printStackTrace(pw);
            System.out.println(sw.toString());
        }
        
        this.tearDownSockets();
    }
    
    public static void main(String [] args){
        startLogging();
        // String type = System.getenv("TYPE");
        // int nodeNum = Integer.parseInt(System.getenv("NODENUM"));
        // String strRT = "TYPE: " + type + " NODENUM: " + nodeNum; 
        // System.out.println(strRT);
        Consumer consumer = new Consumer();
        consumer.start();
    }
}