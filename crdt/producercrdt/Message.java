/**
 *  Copyright 2017 Okusanya Oluwadamilola
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
 
 
/**
 *  Message class 
 *  Used for containing payload to be exchanged between entities in the system
 */
 
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayDeque;
 
public class Message implements Serializable{
    
    private static final long serialVersionUID = 6763126599073315685L;
     
    private final Timestamp timeCreated;
    private final ArrayDeque payload;
    private MessageType.Command type;
    private MessageType.Crdt payloadtype;
    private String nodeIDSentFrom;
    private String groupID;
    
    Message(MessageType.Command type){
        this.type = type;
        this.payload = new ArrayDeque();
        this.timeCreated = new Timestamp(System.currentTimeMillis());
    }
    
    public void setNodeIDFrom(String nodeIDSentFrom){
        this.nodeIDSentFrom = nodeIDSentFrom;
    }
    
    public String getNodeIDFrom(){
        return this.nodeIDSentFrom;
    }
    
    public Timestamp getTimeCreated(){
        return this.timeCreated;
    }
    
    public MessageType.Command getMessageType(){
        return this.type;
    }
    
    public void setPayloadType(MessageType.Crdt payloadtype){
        this.payloadtype = payloadtype;
    }
    
    public MessageType.Crdt getPayloadType(){
        return this.payloadtype;
    }
    
    public void setGroupId(String groupID){
        this.groupID = groupID;
    }   
    
    public String getGroupID(){
        return this.groupID;
    }
    
    public void addPayloadPart(Object payloadPart){
        this.payload.add(payloadPart);
    }
    
    public Object getPayloadPart(){
        return this.payload.poll();
    }
    
    public boolean isEmpty(){
        return this.payload.isEmpty();
    }
    
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("MessageType= ");
        sb.append(this.type);
        sb.append(",MessageSubType= ");
        sb.append(this.payloadtype);
        sb.append(", Payload=");
        sb.append(this.payload.peek().toString());
        return sb.toString();
    }
}
