/**
 *  Copyright 2017 Okusanya Oluwadamilola
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
 
/**
 *  Producer class
 *  Produces messages to be replicated by the raft node
 *  For every message, it receives an acknowledgement message
 */
 
import java.io.PrintWriter;
import java.io.StringWriter; 
import java.io.BufferedReader;
import java.io.BufferedWriter; 
import java.io.IOException;
import org.zeromq.ZMQ;
import java.util.UUID;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Collections;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
  
public class Producer extends Thread{
    
    private final static String HOSTNAME = System.getenv("HOSTNAME");
    private ArrayList<NodeDetails> clusterCrdtNodes;
    private HashMap<String, ZMQ.Socket> negotationSockets;
    private NodeDetails details;
    public static int counter = 0; 
    private int nextNodeToTry;   
    private SystemState.ClusterState currentClusterState;   // this is mainly for shutdown and startup
    private static String PUBLICNODENAME = "Cluster-" + HOSTNAME;
    private final static String PRIVATENODENAME = "Producer-" + HOSTNAME;       // used to connect to the crdt nodes
    private static String APPLOGFILENAME;
    private String primaryServerId = "";
    private String backupServerId = "";
    private String oldBackupServerId = "";
    private boolean hasSentNewUpdate = false;
    private boolean hasSentResetToManagementServer = false;

    
    private int waitCount;
    private final int WAITCOUNT = 10;
    private final int OTHERWAITCOUNT = 20;
    
    private volatile boolean stopped;
    
    // Better way to stop threads
    //private AtomicBoolean running = new AtomicBoolean(false);
    //private AtomicBoolean stopped = new AtomicBoolean(true);
    
    private Thread worker;
    
    
    /** 
     * Sockets that belongs to the thread/node 
     *
     * - A socket that connects to management server. 
     *      - For sending/receiving messages to the management server.
     *      - Socket is async.  
     * - A socket that connects to management server.
     *      - For receiving heartbeat messages
     * - A socket that connects to the management server.
     *      - For sending receipt of the heartbeat messages
     * - A socket that connects to the raft nodes.
     *      - For sending messages to the raft node
     *      - Socket is async
     */
     
    private ZMQ.Socket manServerSocket,
        heartBeatSubSocket,
        heartBeatRepSocket,
        innerThreadSocket,
        shutDownSocket;

    private ZMQ.Context context = ZMQ.context(1);
    private ZMQ.Poller poller;
    
    // Utils object
    private final Utils utils = new Utils();
    
    // Logger
    public final static Logger logger = Logger.getLogger(Producer.class.getName());
    public static FileHandler handler;
	
    // Sender object
    private LongRunningSender sender;
    
    // Details for stats 
    private String tagID;
    private int numberOfMessagesToTest;
    
    Producer(){
        this.currentClusterState = SystemState.ClusterState.DOWN;

        this.details = new NodeDetails.NodeDetailsBuilder(false)
                            .setPublicNodeName(PUBLICNODENAME)
                            .setPrivateNodeName(PRIVATENODENAME)
                            .build();
        this.poller = new ZMQ.Poller(4);
        this.nextNodeToTry = 0;
        this.numberOfMessagesToTest = 0;
        this.tagID = "";
        this.clusterCrdtNodes = new ArrayList<>();
        this.negotationSockets = new HashMap<>();
        this.setUpSockets();
    }
    
    public static void startLogging(){
        try{
            APPLOGFILENAME = "logs/" + PUBLICNODENAME + ".log";
            handler = new FileHandler(APPLOGFILENAME, true);
        } catch ( SecurityException | IOException e ){
            e.printStackTrace();
        }
        
        System.out.println("===============================================");
        String strToWrite = "This is " + PUBLICNODENAME + "'s or the producer log";
        System.out.println(strToWrite);
        System.out.println("===============================================");
        handler.setFormatter(new SimpleFormatter());
        logger.setUseParentHandlers(false);
        logger.addHandler(handler);
        logger.setLevel(Level.INFO);
    }
    
    /**
    public void start(){
        worker = new Thread(this);
        worker.start();
    }
    
    public void stopThread(){
        running.set(false);
    }
    
    public void interrupt(){
        running.set(false);
        worker.interrupt();
    }
    
    public boolean isRunning(){
        return running.get();
    }
    
    public boolean isStopped(){
        return stopped.get();
    }
    **/
    
    public void setUpSockets(){
        this.manServerSocket = this.context.socket(ZMQ.DEALER);
        this.manServerSocket.setIdentity(this.details.getPublicNodeName().getBytes());
        this.manServerSocket.connect(Constants.MANAGEMENT_SERVER_COMMAND_PORT);
        
        this.heartBeatSubSocket = this.context.socket(ZMQ.SUB);
        this.heartBeatSubSocket.connect(Constants.MANAGEMENT_SERVER_SUB_PORT);
        this.heartBeatSubSocket.subscribe("".getBytes());
        
        this.heartBeatRepSocket = this.context.socket(ZMQ.REQ);
        this.heartBeatRepSocket.connect(Constants.MANAGEMENT_SERVER_HEARTBEAT_PORT);
        
        this.shutDownSocket = this.context.socket(ZMQ.REQ);
        this.shutDownSocket.connect(Constants.MANAGEMENT_SERVER_SHUTDOWN_PORT);
		
        this.innerThreadSocket = this.context.socket(ZMQ.PAIR);
        this.innerThreadSocket.bind(Constants.LONGRUNNING_SENDER_PORT);
        
        this.poller.register(this.manServerSocket, ZMQ.Poller.POLLIN);
        this.poller.register(this.heartBeatSubSocket, ZMQ.Poller.POLLIN);
        this.poller.register(this.innerThreadSocket, ZMQ.Poller.POLLIN);
        
        this.sender = new LongRunningSender(this.context, HOSTNAME);
    }
	
    private void setUpNegotiationSockets(){
        for (NodeDetails item: this.clusterCrdtNodes){
            if (!item.getPrivateNodeName().equalsIgnoreCase(this.details.getPrivateNodeName())){
                ZMQ.Socket socket = this.context.socket(ZMQ.DEALER);
                String socketname = UUID.randomUUID().toString();
                String logStr = "This is the dealer socket identity=" +
                    socketname + " on this node that is bound to the router socket of node=" +
                    item.getPrivateNodeName();
                this.printState(logStr, "INFO", this.details.getPrivateNodeName());
                socket.setIdentity(socketname.getBytes());
                socket.connect(item.getIPAddress());
                this.negotationSockets.put(item.getPrivateNodeName(), socket);
            }
        }
    }
    
    private void printPeers(){
        String strToLog = "Current total num of raftNodes: " + this.clusterCrdtNodes.size(); 
        this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
        this.printState("Nodes that are part of this cluster: ", "INFO", this.details.getPrivateNodeName());
        for (NodeDetails details: this.clusterCrdtNodes){
            this.printState(details.toString(), "INFO", this.details.getPrivateNodeName());
        }
    }
    
    private void tearDownSockets(){
        this.manServerSocket.close();
        this.heartBeatRepSocket.close();
        this.heartBeatSubSocket.close();
        this.innerThreadSocket.close();
        this.tearDownNegotationSockets();
        this.context.term();
    }
    
    private void printState(String message, String type, String privateName){
        String strToWrite = "[STATE at " + privateName +  "]: " + message;
        System.out.println(strToWrite);
        switch(type){
            case "INFO":
                logger.log(Level.INFO, strToWrite);
                break;
            case "WARNING":
                logger.log(Level.WARNING, strToWrite);
                break;
            default:
                System.out.println("Cannot log!!!!");
                break;
        }
    }    

    private String getIPAddress(String nodename){
        for (NodeDetails node: this.clusterCrdtNodes){
            if (nodename.equalsIgnoreCase(node.getPrivateNodeName())){
                return node.getIPAddress();
            }
        }
        return "";
    }
	
    public Message createMessageToSend(MessageType.Command messageType, 
        MessageType.Crdt raftype, Object ... args){
        Message messageToSend = new Message(messageType);
        if (raftype != null){
            messageToSend.setPayloadType(raftype);
        }
        if (args != null){
            for (Object arg: args){
                messageToSend.addPayloadPart(arg);
            }
        }
        return messageToSend;
    }
    
    private void tearDownNegotationSockets(){
        for (ZMQ.Socket socket: this.negotationSockets.values()){
            socket.close();
        }
    }
    
    private void processEject(String nodeId){
        Iterator<NodeDetails> it = this.clusterCrdtNodes.iterator();
        while(it.hasNext()){
            NodeDetails item = it.next();
            if (item.getPrivateNodeName().equalsIgnoreCase(nodeId)){
                String strToLog = "Found node to eject=" + item.getPrivateNodeName();
                this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                ZMQ.Socket socket = this.negotationSockets.get(item.getPrivateNodeName());
                socket.close();
                this.negotationSockets.remove(item.getPrivateNodeName());
                this.printState("Removing socket", "INFO", this.details.getPrivateNodeName());
                it.remove();
            }
        }
        this.printPeers();
    }
    
    
    private void checkForNodesToTry(String start){
        NodeDetails node;
        ZMQ.Socket socket;
        String str;
        if (start.isEmpty()) {
            // Unregister that socket
            // Work backwards
            int lastnodeTried = ( ( (this.nextNodeToTry - 1) % this.clusterCrdtNodes.size() )
                + this.clusterCrdtNodes.size() ) % this.clusterCrdtNodes.size();
            str = "Index tried: " + lastnodeTried + " of " + this.clusterCrdtNodes.size();
            this.printState(str, "INFO", this.details.getPrivateNodeName());
            node = this.clusterCrdtNodes.get(lastnodeTried);
            this.printState("This is the node that is being removed: " + node.getPrivateNodeName(), "INFO", this.details.getPrivateNodeName());
            socket = this.negotationSockets.get(node.getPrivateNodeName());
            this.poller.unregister(socket);
        }
            
        str = "Current index to try: " + this.nextNodeToTry + " of " + this.clusterCrdtNodes.size();
        this.printState(str, "INFO", this.details.getPrivateNodeName());
        node = this.clusterCrdtNodes.get(this.nextNodeToTry);
        this.printState("This is the node to try: " + node.getPrivateNodeName(), "INFO", this.details.getPrivateNodeName());
        socket = this.negotationSockets.get(node.getPrivateNodeName());
        this.poller.register(socket, ZMQ.Poller.POLLIN);
        this.nextNodeToTry = (this.nextNodeToTry + 1) % this.clusterCrdtNodes.size();
        
        str = "Next index to try: " + this.nextNodeToTry + " of " + this.clusterCrdtNodes.size();
        this.printState(str, "INFO", this.details.getPrivateNodeName());        
                
        Message message = this.createMessageToSend(MessageType.Command.CRDT,
                MessageType.Crdt.GETSERVERS, "", PeerConnectionState.ServerConnectionEvent.CLIENT_REQUEST);
        
        message.setNodeIDFrom(new String(socket.getIdentity()));
        
        if (socket != null){
                socket.send(this.utils.convertToBytes(message),0);
        }
        
        this.printState("Sending [GETSERVERS] to" + node.getPrivateNodeName() , "INFO", this.details.getPrivateNodeName()); 
    }
    
    public void stopThread(){
        this.stopped = true;
    }
    
    @Override
    public void run(){
        Thread thrd = new Thread(this.sender);
		
	//running.set(true);
	//stopped.set(false);
		
        try{
            while(this.stopped == false){
                try{
                    Message message;
                    String strMessage, strToLog;
                    MessageType.Command type;
                    ZMQ.Socket socket;
                    
                    this.poller.poll();
                    
                    if (this.poller.pollin(0)){
                        message = (Message) this.utils.convertFromBytes(this.manServerSocket.recv(0));
                        type = message.getMessageType();
                        switch(type){
                            case START:
                                if (this.currentClusterState == SystemState.ClusterState.STARTED){
                                    this.currentClusterState = SystemState.ClusterState.RUNNING;
                                    this.printState("Changing ClusterNodeState from STARTED to RUNNING", 
                                        "INFO", 
                                        this.details.getPrivateNodeName()
                                    );
                                    this.clusterCrdtNodes = (ArrayList<NodeDetails>) message.getPayloadPart();
                                    this.printState("Received this clusterOfNodes", 
                                        "INFO", 
                                        this.details.getPrivateNodeName()
                                    );
                                    this.printPeers();
                                    this.setUpNegotiationSockets();
                                    this.checkForNodesToTry("START");
                                    thrd.start();
                                    this.innerThreadSocket.send(this.utils.convertToBytes(new Message(type)), 0);
                                    break;
                                } else {
                                    strToLog = "Received START when node is already started";
                                    this.printState(strToLog, "WARNING", this.details.getPrivateNodeName());
                                }
                                break;
                            case ADDSERVER:
                                NodeDetails dets = (NodeDetails) message.getPayloadPart();
                                this.clusterCrdtNodes.add(dets);
                                this.printPeers();
                                ZMQ.Socket newSocket = this.context.socket(ZMQ.DEALER);
                                String newSocketname = UUID.randomUUID().toString();
                                strToLog = "This is the dealer socket identity=" +
                                    newSocketname + " on this node that is bound to the router socket of node=(" +
                                    dets.getPrivateNodeName() + ")";
                                this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                                newSocket.setIdentity(newSocketname.getBytes());
                                newSocket.connect(dets.getIPAddress());
                                this.negotationSockets.put(dets.getPrivateNodeName(), newSocket);
                                this.printState("Added new server: " + dets.getPrivateNodeName(), "INFO", this.details.getPrivateNodeName());
                                break;
                            case QUIT:
                            case STOP:
                                // Quitting 
                                this.shutDownSocket.send(
                                    this.utils.convertToBytes(
                                        this.createMessageToSend(MessageType.Command.HEARTBEAT, 
                                            null, PUBLICNODENAME)
                                    ), 0
                                );
                                this.printState( 
                                    "Sent QUIT message ON [Node Shutdown Endpoint] TO (Management Server)", 
                                    "INFO", 
                                    this.details.getPrivateNodeName()
                                );
                                message = (Message) this.utils.convertFromBytes(this.shutDownSocket.recv(0));
                                strToLog = "MESSAGE RECEIVED: (" + message.getMessageType() + ") ON [Node Shutdown Endpoint] FROM (Management Server)";
                                this.printState(
                                    strToLog, 
                                    "INFO", 
                                    this.details.getPrivateNodeName()
                                );
                                if ("QUIT".equalsIgnoreCase(message.getMessageType().name())){
                                    this.innerThreadSocket.send(
                                        this.utils.convertToBytes(
                                            this.createMessageToSend(MessageType.Command.QUIT, null)
                                        ), 0
                                    );
                                    this.printState(
                                        "Sending QUIT to fileThread", 
                                        "INFO", 
                                        this.details.getPrivateNodeName()
                                    );
                                    //Thread.currentThread().interrupt();
                                    this.stopThread();
                                }
                                break;
                            case SETGROUPID:
                                this.tagID = (String) message.getPayloadPart();
                                this.numberOfMessagesToTest = (int) message.getPayloadPart();
                                strToLog = "Just set the tag id=" + this.tagID + " to test for numOfMessages=" + String.valueOf(this.numberOfMessagesToTest);
                                this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                                break;
                            case EJECT:
                                String nodeIdToEject = (String) message.getPayloadPart();
                                this.processEject(nodeIdToEject);
                                strToLog = "Ejecting nodeId=" + nodeIdToEject + " from list of nodes";
                                this.printState(
                                    strToLog, 
                                    "INFO", 
                                    this.details.getPrivateNodeName()
                                );
                                break;
                            default:
                                this.printState("This command is not recognised", 
                                    "WARNING",
                                    this.details.getPrivateNodeName()
                                );
                                break;
                        }
                        strToLog = "PROCESSING END: (Command Type: " + type.name() + ")";
                        this.printState(strToLog, 
                            "INFO",
                            this.details.getPrivateNodeName()
                        );
                    }
                    
                    if (this.poller.pollin(1)){
                        message = (Message) this.utils.convertFromBytes(this.heartBeatSubSocket.recv(0));
                        strToLog = "MESSAGE RECEIVED: (" + (String) message.getPayloadPart() + ") ON [Subs Socket] FROM (Management server)";
                        this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                        
                        // Communicating with the management server
                        
                        this.heartBeatRepSocket.send(
                            this.utils.convertToBytes(
                                this.createMessageToSend(MessageType.Command.HEARTBEAT, 
                                    null, PUBLICNODENAME)
                            ), 0
                        );
                        this.printState("MESSAGE SENT: (" + PUBLICNODENAME + ") ON [Node Rep Endpoint] TO (Management Server)", 
                                "INFO", this.details.getPrivateNodeName());
                        message = (Message) this.utils.convertFromBytes(this.heartBeatRepSocket.recv(0));
                        strMessage = (String) message.getPayloadPart();
                        strToLog = "MESSAGE RECEIVED: (" + strMessage + ") ON [Node Rep Endpoint] FROM (Management Server)";
                        this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                        
                        if ("I have seen you".equalsIgnoreCase(strMessage) && this.currentClusterState == SystemState.ClusterState.DOWN){
                            this.manServerSocket.send(
                                this.utils.convertToBytes(
                                    this.createMessageToSend(MessageType.Command.HEARTBEAT,
                                        null, this.details)
                                ),0
                            );
                            strToLog = "MESSAGE SENT: (" + 
                                    this.details + 
                                    ") ON [Node Public Endpoint] TO (Management Server)";
                            this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                            this.currentClusterState = SystemState.ClusterState.STARTED;
                            this.printState("Changing ClusterState from DOWN to STARTED", 
                                "INFO", this.details.getPrivateNodeName());
                        } else if ("I have seen you".equalsIgnoreCase(strMessage) && this.currentClusterState == SystemState.ClusterState.STARTED && this.clusterCrdtNodes.isEmpty() == true){
                            // This is on the rare chance the managmenet server receives its heartbeat 
                            // before the management server sends the entire cluster of raftnodes to the 
                            // raftnodes themselves
                            this.printState("Node is at ClusterState STARTED", 
                                "INFO", this.details.getPrivateNodeName());
                            this.manServerSocket.send(
                                this.utils.convertToBytes(
                                    this.createMessageToSend(MessageType.Command.HEARTBEAT,
                                        null, this.details)
                                ),0
                            );
                            strToLog = "MESSAGE SENT: (" + 
                                    this.details + 
                                    ") ON [Node Public Endpoint] TO (Management Server)";
                            this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                        }
                    }
                                    
                    if (this.poller.pollin(2)){
                        message = (Message) this.utils.convertFromBytes(this.innerThreadSocket.recv(0));
                        if (this.primaryServerId.trim().length() != 0 && this.hasSentNewUpdate == false){
                            ++counter;
                            if (this.waitCount > 0){
                                this.waitCount = 0;
                            }
                            this.printState("Sending new message","INFO", this.details.getPrivateNodeName());
                            if (this.tagID.trim().length() == 0 && this.numberOfMessagesToTest == 0){
                                message = this.createMessageToSend(MessageType.Command.CRDT,
                                        MessageType.Crdt.CLIENTPOST, counter);
                            } else {
                                this.printState("Sending tagged message..." + this.numberOfMessagesToTest, "INFO", this.details.getPrivateNodeName());
                                message = this.createMessageToSend(MessageType.Command.CRDT,
                                        MessageType.Crdt.CLIENTPOST, counter, this.tagID);
                            }
                            socket = this.poller.getSocket(3);
                            message.setNodeIDFrom(new String(socket.getIdentity()));
                            socket.send(this.utils.convertToBytes(message), 0);
                            strToLog =  " Sent new message=(" + counter + ") at timestamp=" + System.currentTimeMillis();
                            this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                            this.hasSentNewUpdate = true;
                        } else if (this.backupServerId.trim().length() != 0 && this.oldBackupServerId.trim().length() == 0) {
                            this.waitCount++;
                            this.printState("Trying to connect to failed primary server... in " + this.waitCount, "INFO", this.details.getPrivateNodeName());
                            if (this.waitCount == this.WAITCOUNT){
                                this.waitCount = 0;
                                socket = this.negotationSockets.get(this.primaryServerId);
                                this.poller.unregister(socket);
                                ZMQ.Socket newSocket = this.negotationSockets.get(this.backupServerId);
                                this.poller.register(newSocket);
                                
                                this.printState("Asking backup server=" + this.backupServerId + " for new servers", "INFO", this.details.getPrivateNodeName());
                                
                                this.processEject(this.primaryServerId);

                                this.primaryServerId = "";
                                this.oldBackupServerId = this.backupServerId;
                                this.backupServerId = "";
                                
                                this.printState("Sending getServers message to backup server", "INFO", this.details.getPrivateNodeName()); 
                                
                                message = this.createMessageToSend(MessageType.Command.CRDT,
                                    MessageType.Crdt.GETSERVERS, "None", PeerConnectionState.ServerConnectionEvent.CLIENT_REQUEST);
                                message.setNodeIDFrom(new String(newSocket.getIdentity()));
                                if (newSocket != null){
                                    newSocket.send(this.utils.convertToBytes(message),0);
                                }
                            }
                        }  else if ("".equalsIgnoreCase(this.backupServerId) && "".equalsIgnoreCase(this.primaryServerId)){
                            this.waitCount++;
                            if (this.hasSentResetToManagementServer == true){
                                this.checkForNodesToTry("");
                            }
                            if (this.waitCount == this.OTHERWAITCOUNT){
                                // this means that the backup server has failed
                                this.waitCount = 0;
                                this.printState("Asking management to select new servers", "INFO", this.details.getPrivateNodeName());
                                socket = this.negotationSockets.get(this.oldBackupServerId);
                                this.poller.unregister(socket);
                                // Connect to the management server and ask it to give you two new sockets
                                this.processEject(this.oldBackupServerId);
                                
                                this.manServerSocket.send(
                                    this.utils.convertToBytes(
                                        this.createMessageToSend(MessageType.Command.GETSERVERS, null)
                                    ),0
                                );
                                this.checkForNodesToTry("");
                                this.hasSentResetToManagementServer = true;
                            } 
                        }
                    }
                                    
                    if (this.currentClusterState == SystemState.ClusterState.RUNNING){
                        if (this.poller.pollin(3)){
                            socket = this.poller.getSocket(3);
                            message = (Message) this.utils.convertFromBytes(socket.recv(0));
                            type = message.getMessageType();
                            strToLog = "Received a message of type=[" + type + "] from the node=[" + message.getNodeIDFrom() + "]";
                            this.printState(
                                strToLog,
                                "INFO",
                                this.details.getPrivateNodeName()
                            );
                            switch(type){
                                case SETSERVERS:
                                    MessageType.Command responsetype = (MessageType.Command) message.getPayloadPart();
                                    switch (responsetype){
                                        case ACK:
                                            this.primaryServerId = (String) message.getPayloadPart();
                                            this.backupServerId = (String) message.getPayloadPart();
                                            // Register the socket to send to, to be the orimary server id
                                            this.poller.unregister(socket);
                                            ZMQ.Socket newSocket = this.negotationSockets.get(this.primaryServerId);
                                            this.poller.register(newSocket);
                                            
                                            strToLog = "Found the primary server=(" + this.primaryServerId + "), secondary server=(" + this.backupServerId + ").Connecting now...";
                                            this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                                            this.hasSentNewUpdate = false;
                                            this.hasSentResetToManagementServer = false;
                                            this.oldBackupServerId = "";
                                            break;
                                        case NACK:
                                            if (this.primaryServerId.trim().length() == 0 && this.backupServerId.trim().length() == 0){
                                                // Initial connection
                                                if (this.oldBackupServerId.trim().length() != 0){
                                                    this.printState("Still asking backup server for main servers", "INFO", this.details.getPrivateNodeName());
                                                    message = this.createMessageToSend(MessageType.Command.CRDT,
                                                        MessageType.Crdt.GETSERVERS, "", PeerConnectionState.ServerConnectionEvent.CLIENT_REQUEST);
                                                    message.setNodeIDFrom(new String(socket.getIdentity()));
                                                    if (socket != null){
                                                            socket.send(this.utils.convertToBytes(message),0);
                                                    }
                                                } else {
                                                    this.printState("Did not find main servers.Retrying..", "INFO", this.details.getPrivateNodeName());
                                                    this.checkForNodesToTry("");
                                                }
                                            } else {
                                                strToLog = "Received NACK when main servers are known.Disregarding..";
                                                this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                                            }
                                            break;
                                    }
                                    break;
                                case CRDT:
                                    MessageType.Crdt raftype = message.getPayloadType();
                                    switch(raftype){
                                        case CLIENTPOST_REP:
                                            StringBuilder sb = new StringBuilder();
                                            sb.append("Received ( ");
                                            sb.append(raftype.name());
                                            sb.append(" ) from crdtnode: ");
                                            sb.append((String)message.getPayloadPart());
                                            System.out.println(sb.toString());
                                            this.printState(sb.toString(), 
                                                "INFO",
                                                this.details.getPrivateNodeName()
                                            );
                                            if (this.numberOfMessagesToTest > 0 && this.tagID.trim().length() > 0){
                                                --this.numberOfMessagesToTest;
                                                if (this.numberOfMessagesToTest == 0){
                                                    this.tagID = "";
                                                }
                                            } 
                                            
                                            this.hasSentNewUpdate = false;
                                    }
                                    break;
                                default:
                                    strToLog = "PROCESSING END: (Command Type: " + type.name() + ")";
                                    this.printState(strToLog, 
                                        "INFO",
                                        this.details.getPrivateNodeName()
                                    );
                                    break;
                            }
                            strToLog = "Processing end of message of type=[" + type.name() + "]";
                            this.printState(
                                strToLog,
                                "INFO",
                                this.details.getPrivateNodeName()
                            );
                        }
                    }
                    Thread.sleep(2000);
                } catch (InterruptedException ie){
                    ie.printStackTrace();
                }
            }
            
            if (this.stopped == true){
                this.printState("Thread is interrupted", "INFO", this.details.getPrivateNodeName());
                this.tearDownSockets();
                return;
            }
            
        } catch (Exception ie){
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ie.printStackTrace(pw);
            System.out.println(sw.toString());
        }
    }
    
    public static void main(String [] args){
        startLogging();
        // String type = System.getenv("TYPE");
        // int nodeNum = Integer.parseInt(System.getenv("NODENUM"));
        // String strRT = "TYPE: " + type + " NODENUM: " + nodeNum; 
        // System.out.println(strRT);
        Producer producer = new Producer();
        producer.start();
    }
}
