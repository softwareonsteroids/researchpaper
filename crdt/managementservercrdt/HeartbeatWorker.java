/**
 *  Copyright 2017 Okusanya Oluwadamilola
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */


/**
 *  This sends heartbeats
 */

import org.zeromq.ZMQ;
import java.io.IOException; 
import java.util.ArrayDeque;

public class HeartbeatWorker implements Runnable{
    private volatile boolean stopped;
    private Message message;
    public ZMQ.Socket socket;
    private final Utils utils = new Utils();
    private int time;

    HeartbeatWorker(ZMQ.Context context){
        this.socket = context.socket(ZMQ.PUB);
        socket.setLinger(5000);
        socket.setSndHWM(0);
        this.socket.bind(Constants.MANAGEMENT_SERVER_PUB_PORT);
        this.setSleepTime(9000);
        this.setMessage(); 
    }
    
    private void setMessage(){
        this.message = new Message(MessageType.Command.HEARTBEAT);
        this.message.addPayloadPart("Are you there");
    }

    public void setSleepTime(int time){
        this.time = time;
    }

    @Override
    public void run(){
        while(stopped == false){
        // If there is no stop, then there would be a Context terminated exception.
            socket.send(this.utils.convertToBytes(message), 0);
            try{
                System.out.println("[Heartbeats]: Just sent a heartbeat ") ;	
                Thread.sleep(time);
            } catch (InterruptedException ie){
                ie.printStackTrace();
            }
        }
        if (stopped == true){
            socket.close();
            Thread.currentThread().interrupt();
        }
    }

    public void stopHeartBeats(){
        stopped = true;
    }
}