/**
 *  Copyright 2017 Okusanya Oluwadamilola
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
 
/**
 *  This is the Utility class
 */

import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.Files;
import java.util.ArrayList;
import java.io.IOException;

public class Utils{

    public void writeToFile(String content, String filename){
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(filename))){
            bw.write(content);
        } catch (IOException e){
            e.printStackTrace();
        }
    }
    
    public ArrayList<String> readFromFile(int noOfLines, String filename){
        ArrayList<String> tnr = new ArrayList<String>();
        try( BufferedReader br = new BufferedReader(new FileReader(filename))){
            String line = br.readLine();
            int count = 0;
            final Path path = Paths.get(filename);
            long lineCount = Files.lines(path).count();
            if (noOfLines >= lineCount){
                noOfLines = (int) lineCount;
            }
            while (line != null && count < noOfLines){
                tnr.add(line);
                line = br.readLine();
                count++;
            }
        } catch (IOException ie){
            ie.printStackTrace();
        }
        return tnr;
    }
    
    public byte [] convertToBytes(Object object){
        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutput out = new ObjectOutputStream(bos)){
            out.writeObject(object);
            return bos.toByteArray();
        } catch (IOException ie) {
            ie.printStackTrace();
        }
        return null;
    }
    
    public Object convertFromBytes(byte[] bytes) {
        try (ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
            ObjectInput in = new ObjectInputStream(bis)){
            return in.readObject();
        } catch (IOException | ClassNotFoundException ie){
            ie.printStackTrace();
        }
        return null;
    }
    
    /**
    public String getNodeAddress(){
        String line;
        try{
            ProcessBuilder builder = new ProcessBuilder(
                "hostname", "-i", "|", "awk", "{print $1}");
            builder.redirectErrorStream(true);
            Process p = builder.start();
            BufferedReader reader = new BufferedReader(new 
                InputStreamReader(p.getInputStream()));
            while (true){
                line = reader.readLine();
                if (line == null) { break; }
                System.out.println(line);
            }
        } catch (IOException ie){
            ie.printStackTrace();
        }
        return line;
    }
    **/
}