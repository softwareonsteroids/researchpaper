/**
 *  Copyright 2018 Okusanya Oluwadamilola
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
 
/**
 * GSet class
 */
 
import java.util.HashSet;
import java.io.Serializable;
import java.util.Set;
import java.util.Collections;
import java.util.Objects;

// this is a state based implementation (CmRDT)
public class GSet<T> implements Serializable {

    private static final long serialVersionUID = 5108200790348334384L;

    private HashSet<T> innerSet;
    
    GSet(){
        this.innerSet = new HashSet<T>();
    }
    
    GSet(HashSet<T> set){
        this.innerSet = new HashSet<T>(set);
    }
    
    // this is the update method
    public void add(T newVal){
        this.innerSet.add(newVal);
    }
    
    public boolean lookup(T e){
        return Collections.unmodifiableSet(this.innerSet).contains(e);
    }
    
    public HashSet<T> query(){
        return this.innerSet;
    }
    
    public int getSize(){
        return this.innerSet.size();
    }
    
    // merge does not modify local state
    public GSet<T> merge(GSet<T> set){
        final HashSet<T> mergeSet = new HashSet<T>(this.innerSet);
        mergeSet.addAll(set.query());
        return new GSet<T> (new HashSet<T>(mergeSet));
    }
    
    public boolean isSuperSetOf(GSet<T> set){
        return Collections.unmodifiableSet(this.innerSet).containsAll(set.query());
    }
    
    public GSet<T> diff(GSet<T> set){
        HashSet<T> diffSet = new HashSet<>();
        Set<T> compareSet = Collections.unmodifiableSet(this.innerSet);
        for (T t: set.query()){
            if (compareSet.contains(t) == false){
                diffSet.add(t);
            }
        }
        return new GSet<T>(diffSet);
    }
    
    @Override
    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        
        GSet<T> that = (GSet<T>) o;
        
        return Objects.equals(this.innerSet, that.innerSet);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(this.innerSet);
    }
    
    @Override
    public String toString(){
        return this.innerSet.toString();
    }

}
