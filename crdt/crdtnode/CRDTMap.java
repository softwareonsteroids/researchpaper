/**
 *  Copyright 2018 Okusanya Oluwadamilola
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
 
/**
 * CRDT map
 */ 

import java.util.HashMap;
import java.util.Set;
import java.util.Collections;
 
public class CRDTMap<K, V extends Comparable<V>>{
    private HashMap<K, LWWSet<V>> log;
    
    CRDTMap(){
        this.log = new HashMap<K, LWWSet<V>>();
    }
    
    public LWWSet<V> getValuesFor(K key){
        return this.log.get(key);
    }
    
    public void setNewLog(HashMap<K, LWWSet<V>> log){
        this.log = log;
    }
    
    public HashMap<K, LWWSet<V>> getEntireState(){
        return this.log;
    }

    // this is from server to node
    public void add(K key, V value, long timeStamp, long timeCreated){
        System.out.println("Adding value=" + value + " at timestamp=" + timeStamp);
        LWWSet<V> values = this.getValuesFor(key);
        if (values == null){
            values = new LWWSet<V>();
            LWWSetValue<V> newEntry = values.add(value, timeStamp, timeCreated);
            System.out.println("Entry added: " + newEntry);
            this.log.put(key, values);
        } else {
            LWWSetValue<V> entry = values.add(value, timeStamp, timeCreated);
            this.log.put(key, values);
            System.out.println("Entry added: " + entry);
        }
    }
    
    // For generating stats
    public void add(K key, V value, long timeStamp, long timeCreated, String tagID){
        System.out.println("Adding value=" + value + " at timestamp=" + timeStamp);
        LWWSet<V> values = this.getValuesFor(key);
        if (values == null){
            values = new LWWSet<V>();
            LWWSetValue<V> newEntry = values.add(value, timeStamp, timeCreated, tagID);
            System.out.println("Entry added: " + newEntry);
            this.log.put(key, values);
        } else {
            LWWSetValue<V> entry = values.add(value, timeStamp, timeCreated, tagID);
            this.log.put(key, values);
            System.out.println("Entry added: " + entry);
        }
    }
    
    public void addWithoutMerge(K key, LWWSet<V> mergedState){
        this.log.put(key, mergedState);
    }
    
    public void remove(K key, V value, long timeStamp, long timeCreated){
        LWWSet<V> values = this.getValuesFor(key);
        values.remove(value, timeStamp, timeCreated);
        this.log.put(key, values);
    }
    
    public void get(K key){
        
    }
    
    // this is for node to node
    public void addState(K key, LWWSet<V> anotherState){
        final LWWSet<V> otherState = new LWWSet<V>();
        for (LWWSetValue<V> item: anotherState.getAddSet().query()){
            otherState.add(item.getElement(), System.currentTimeMillis(), item.getProducerTimestamp());
        }
        
        for (LWWSetValue<V> item: anotherState.getRemoveSet().query()){
            otherState.add(item.getElement(), System.currentTimeMillis(), item.getProducerTimestamp());
        }
        
        LWWSet<V> values = this.getValuesFor(key);
        if (values == null){
            values = new LWWSet<V>();
            LWWSet<V> newState = values.merge(otherState);
            this.log.put(key, newState);
        } else {
            LWWSet<V> newerState = values.merge(otherState);
            this.log.put(key, newerState);
        }
    }
    
    public boolean find(K key, V val){
        LWWSet<V> values = this.getValuesFor(key);
        return values.lookup(val);
    }
    
    public LWWSetValue<V> getLatest(K key, V val){
        LWWSet<V> values = this.getValuesFor(key);
        return values.value(val).getFirst();
    }
    
    public Set<K> getKeys(){
        return Collections.unmodifiableSet(this.log.keySet());
    }
    
    public GSet<LWWSetValue<V>> getRemoveSet(K key){
        LWWSet<V> values = this.getValuesFor(key);
        return values.getRemoveSet();
    }
    
    public void removeElements(K key, GSet<LWWSetValue<V>> setToRemove){
        final LWWSet<V> values = this.getValuesFor(key);
        LWWSet<V> othervalues = values.purge(setToRemove);
        this.log.put(key, othervalues);
    }
    
    public int length(K key){
        LWWSet<V> values = this.getValuesFor(key);
        return values.getAddSize();
    }
    
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        for (K key: this.getKeys()){
            sb.append(System.lineSeparator());
            sb.append("Key: ");
            sb.append(key.toString());
            sb.append(System.lineSeparator());
            sb.append(this.getValuesFor(key).toString());
            sb.append(System.lineSeparator());
        }
        sb.append(System.lineSeparator());
        return sb.toString();
    }
    
    public static void main(String [] args){
        //CRDTMap<String, Integer> map = new CRDTMap<>();
        /**
        map.add("Test", 1, System.currentTimeMillis());
        map.add("Test", 3, System.currentTimeMillis());
        map.add("Test", 5, System.currentTimeMillis());
        map.remove("Test", 1, System.currentTimeMillis());
        
        System.out.println("Map:" + map);
    
        //map.removeElements("Test");
        
        System.out.println("Map:" + map);
        **/
    }
}
 
