/**
 *  Copyright 2018 Okusanya Oluwadamilola
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
 
/**
 * LWWSetValue class
 */ 

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ArrayList;
import java.io.Serializable;
import java.util.Objects;

public class LWWSetValue<T extends Comparable<T>> implements Serializable{
    
    private static final long serialVersionUID = 8383319433470940185L;

    private long timeInLong, timeProducerCreated;
    private String groupID="";
    
    private T element;
    
    LWWSetValue(T element, long timeInLog, long timeProducerCreated){
        this.element = element;
        this.timeInLong = timeInLog;
        this.timeProducerCreated = timeProducerCreated;
    }
    
    public long getTimestampInLong(){
        return this.timeInLong;
    }
    
    public String getGroupId(){
        return this.groupID;
    }
    
    public void setGroupId(String groupID){
        this.groupID = groupID;
    }
    
    public T getElement(){
        return this.element;
    }
    
    public long getProducerTimestamp(){
        return this.timeProducerCreated;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LWWSetValue<T> that = (LWWSetValue<T>) o;

        //return timeInLong == that.timeInLong && !(element != null ? !element.equals(that.element) : that.element != null);
        return Objects.equals(this.timeInLong, that.timeInLong) && Objects.equals(this.element, that.element);
    }

    @Override
    public int hashCode() {
        /**
        int result = (int) (timeInLong ^ (timeInLong >>> 32));
        result = 31 * result + (element != null ? element.hashCode() : 0);
        return result;
        **/
        return Objects.hash(this.timeInLong, this.element);
    }
    
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("(Timestamp: ");
        sb.append(String.valueOf(this.timeInLong));
        sb.append(" Element: ");
        sb.append(this.element.toString());
        sb.append(")");
        sb.append(System.lineSeparator());
        return sb.toString();
    }
}  
