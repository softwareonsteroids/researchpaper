/**
 *  Copyright 2018 Okusanya Oluwadamilola
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
 
/**
 * Crdt node class
 * It uses the binary star pattern.
 * The binary star pattern is a pattern where there are two servers, one backup, one secondary.
 * Each node has five states: 
 * - primary(the main server that is waiting for client connections)
 * - backup (the secondary server that is waiting for client connections)
 * - a primary server that knows both the primary and secondary id is an active server
 * - a backup server that knows both the primary and secondary is a passive server
 * - peer( a server that is neither primary, backup, active, passive)
 * The client only connects if it knows boh primary and active nodes.
 * Conversion between the various states will be done through a FSM.
 * 
 * The description in the ZMQ source code uses PUB-SUB. This will not do for CRDTs.
 * The algorihtm for this server is this:
 * - when the management client sends a START message, the management server collects all the addresses
 *  of all the servers in the network. All the servers start up as peer.
 * - the management server then designates the backup and primary servers.
 *  - the management server sends a EVENT_PEER_BACKUP message to a server, 
 * - 
 */
 
import java.io.PrintWriter;
import java.io.StringWriter; 
import java.io.BufferedReader;
import java.io.BufferedWriter; 
import java.io.IOException;
import java.util.concurrent.ThreadLocalRandom;
import org.zeromq.ZMQ;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.Iterator;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Collections;
import java.util.ArrayList;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.sql.Timestamp;

public class CrdtNode extends Thread{
    
    private final static String HOSTNAME = System.getenv("HOSTNAME");
    private final static String PUBLICNODENAME= "Cluster-" + HOSTNAME;      // used to connect to the managment server
    private final static String PRIVATENODENAME = "Crdt-" + HOSTNAME;       // used to connect to the crdt nodes
    private static String APPLOGFILENAME;
    private ArrayList<NodeDetails> clusterCrdtNodes;
    private HashMap<String, ZMQ.Socket> dealerSockets;
    private NodeDetails details;
    
    private String primaryServerId="";
    private String backupServerId="";
    
    private int indexOfNextPeerToSendUpdateTo;
    private int numOfReceivedMergeStates;
    
    private boolean hasReceivedEntireState;
    private String consumerIDToSendTo;
    
    // Sockets
    private ZMQ.Socket manServerSocket, // connects to the management server
            heartBeatSubSocket,         // subscribes to the heartbeats from management server
            heartBeatRepSocket,         // replies to the heeartbeats from management server
            shutDownSocket,             // socket that responds to shutdown socket
            crdtNodeRcvSocket;          // socket through which all crdts node communicate on
    
    private ZMQ.Context context = ZMQ.context(1);
    private ZMQ.Poller poller;
    
    // Logger
    public final static Logger logger = Logger.getLogger(CrdtNode.class.getName());
    public static FileHandler handler;
    
    // Utils object
    private final Utils utils = new Utils();
    
    // Timestamp object
    private Timestamp initialElectionTimestamp;
    
    private SystemState.ClusterState currentClusterState;  // this is mainly for shutdown and startup
    public PeerConnectionState.ServerState state;   // Primary, backup, peer
    
    private CRDTMap<String, Integer> eventlog;
    
    CrdtNode(String nodename){
        this.currentClusterState = SystemState.ClusterState.DOWN;
        this.state = PeerConnectionState.ServerState.PEER;
        
        String address = "tcp://" + nodename + ":7200";
        this.details = new NodeDetails.NodeDetailsBuilder(false)
                            .setPublicNodeName(PUBLICNODENAME)
                            .setPrivateNodeName(PRIVATENODENAME)
                            .setIPAddress(address)
                            .build();
        System.out.println("My details are : " + this.details);
        this.dealerSockets = new HashMap<String, ZMQ.Socket>();
        this.clusterCrdtNodes = new ArrayList<>();
        this.poller = new ZMQ.Poller(3);
        this.hasReceivedEntireState = false;
        this.indexOfNextPeerToSendUpdateTo = 0;
        this.numOfReceivedMergeStates = 0;
        this.consumerIDToSendTo = "";
        this.eventlog = new CRDTMap<String, Integer>();
        this.setUpSockets();
    }
    
     private void setUpSockets(){
        this.manServerSocket = this.context.socket(ZMQ.DEALER);
        this.manServerSocket.setIdentity(this.PUBLICNODENAME.getBytes());
        this.manServerSocket.connect(Constants.MANAGEMENT_SERVER_COMMAND_PORT);
        
        this.heartBeatSubSocket = this.context.socket(ZMQ.SUB);
        this.heartBeatSubSocket.connect(Constants.MANAGEMENT_SERVER_SUB_PORT);
        this.heartBeatSubSocket.subscribe("".getBytes());
        
        this.heartBeatRepSocket = this.context.socket(ZMQ.REQ);
        this.heartBeatRepSocket.connect(Constants.MANAGEMENT_SERVER_HEARTBEAT_PORT);
        
        this.crdtNodeRcvSocket = this.context.socket(ZMQ.ROUTER);
        this.crdtNodeRcvSocket.bind(Constants.CRDTCLUSTER_CRDTNODE_RECV_PORT);
        
        this.shutDownSocket = this.context.socket(ZMQ.REQ);
        this.shutDownSocket.connect(Constants.MANAGEMENT_SERVER_SHUTDOWN_PORT);

        this.poller.register(this.manServerSocket, ZMQ.Poller.POLLIN);
        this.poller.register(this.heartBeatSubSocket, ZMQ.Poller.POLLIN);
        this.poller.register(this.crdtNodeRcvSocket, ZMQ.Poller.POLLIN);
    }
    
    private void setUpDealerSockets(){
        for (NodeDetails item: this.clusterCrdtNodes){
            if (!item.getPrivateNodeName().equalsIgnoreCase(this.details.getPrivateNodeName())){
                ZMQ.Socket socket = this.context.socket(ZMQ.DEALER);
                String socketname = UUID.randomUUID().toString();
                String logStr = "This is the dealer socket identity=" +
                    socketname + " on this node that is bound to the router socket of node=(" +
                    item.getPrivateNodeName() + ")";
                this.printState(logStr, "INFO", this.details.getPrivateNodeName());
                socket.setIdentity(socketname.getBytes());
                socket.connect(item.getIPAddress());
                this.dealerSockets.put(item.getPrivateNodeName(), socket);
            }
        }
    }
    
    private void tearDownSockets(){
        this.shutDownSocket.close();
        this.heartBeatRepSocket.close();
        this.heartBeatSubSocket.close();
        this.crdtNodeRcvSocket.close();
        this.tearDownDealerSockets();
        this.manServerSocket.close();
        this.context.term();
    }
    
    private void processEject(String nodeId){
        Iterator<NodeDetails> it = this.clusterCrdtNodes.iterator();
        while(it.hasNext()){
            NodeDetails item = it.next();
            if (item.getPublicNodeName().equalsIgnoreCase(nodeId)){
                String strToLog = "Found node to eject=" + item.getPrivateNodeName();
                this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                ZMQ.Socket socket = this.dealerSockets.get(item.getPrivateNodeName());
                socket.close();
                this.dealerSockets.remove(item.getPrivateNodeName());
                this.printState("Removing socket", "INFO", this.details.getPrivateNodeName());
                it.remove();
            }
        }
        this.printPeers();
    }
    
        
    private void printPeers(){
        String strToLog = "Current total num of raftNodes: " + this.clusterCrdtNodes.size(); 
        this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
        this.printState("Nodes that are part of this cluster: ", "INFO", this.details.getPrivateNodeName());
        for (NodeDetails details: this.clusterCrdtNodes){
            this.printState(details.toString(), "INFO", this.details.getPrivateNodeName());
        }
    }
    
    public static void startLogging(){
        try{
            APPLOGFILENAME = "logs/ClusterNode_" + HOSTNAME + ".log";
            handler = new FileHandler(APPLOGFILENAME, true);
        } catch ( SecurityException | IOException e ){
            e.printStackTrace();
        }
        
        System.out.println("===============================================");
        String strToWrite = "This is " + PRIVATENODENAME + " or " + "ClusterNode_" + HOSTNAME + "'s log";
        System.out.println(strToWrite);
        System.out.println("===============================================");
        handler.setFormatter(new SimpleFormatter());
        logger.setUseParentHandlers(false);
        logger.addHandler(handler);
        logger.setLevel(Level.INFO);
    }
    
    
    private void tearDownDealerSockets(){
        for (ZMQ.Socket socket: this.dealerSockets.values()){
            socket.close();
        }
    }
     
    public Message createMessageToSend(MessageType.Command messageType, 
            MessageType.Crdt crdtType, Object ... args){
        Message messageToSend = new Message(messageType);
        if (crdtType != null){
            messageToSend.setPayloadType(crdtType);
        }
        if (args != null){
            for (Object arg: args){
                messageToSend.addPayloadPart(arg);
            }
        }
        return messageToSend;
    }
    
    private void unicastMessage(String idToSendTo, Message message){
        String logStr = "Connecting to node=(" + idToSendTo + ")";
        this.printState(logStr, "INFO", this.details.getPrivateNodeName());
        message.setNodeIDFrom(this.details.getPrivateNodeName());
        ZMQ.Socket socket = this.dealerSockets.get(idToSendTo);
        socket.send(this.utils.convertToBytes(message), 0);
        logStr = "MESSAGE SENT: type=" + message.getMessageType() + ",subtype=" + 
            message.getPayloadType() + " ON [Node Private Endpoint] TO (" + idToSendTo + ")"; 
        this.printState(logStr, "INFO", this.details.getPrivateNodeName());
    }
    
        
    private void sendMessageToExternalClient(Message message, String clientId){
        this.crdtNodeRcvSocket.send(clientId, ZMQ.SNDMORE);
        this.crdtNodeRcvSocket.send(this.utils.convertToBytes(message), 0);
    }
    
    private void printState(String message, String type, String appname){
        String strToWrite = "[STATE at (" + appname +  ")]: " + message;
        System.out.println(strToWrite);
        switch(type){
            case "INFO":
                logger.log(Level.INFO, strToWrite);
                break;
            case "WARNING":
                logger.log(Level.WARNING, strToWrite);
                break;
            case "SEVERE":
                logger.log(Level.SEVERE, strToWrite);
                break;
            default:
                System.out.println("Cannot log!!!!");
                break;
        }
    }
    
    /** 
     * Methods for processing Crdt message 
     **/
    
    // This is when the crdt node receives a message update from the primary server
    // Trying a state-based crdt
    private void processServerPost(Message message, boolean delta){
        if (this.state == PeerConnectionState.ServerState.PEER){
            int counter = (int) message.getPayloadPart();
            long timeCreated = (long) message.getPayloadPart();
            
            String str = "Message payload from primary server=" + counter;
            this.printState(str, "INFO", this.details.getPrivateNodeName());

            
            if (delta == true){
                if (message.isEmpty() == false){
                    String tagId = (String) message.getPayloadPart();
                    this.eventlog.add("Test", counter, System.currentTimeMillis(), timeCreated, tagId);
                    this.printState("State to send: " + counter, "INFO", this.details.getPrivateNodeName());
                    message = this.createMessageToSend(MessageType.Command.CRDT, 
                        MessageType.Crdt.UPDATE, counter, timeCreated, tagId);
                } else {
                    this.eventlog.add("Test", counter, System.currentTimeMillis(), timeCreated);
                    this.printState("State to send: " + counter, "INFO", this.details.getPrivateNodeName());
                    message = this.createMessageToSend(MessageType.Command.CRDT, 
                        MessageType.Crdt.UPDATE, counter, timeCreated);
                }
                    
            } else {
                if (message.isEmpty() == false){
                    String tagId = (String) message.getPayloadPart();
                    this.eventlog.add("Test", counter, System.currentTimeMillis(), timeCreated, tagId);
                    LWWSet<Integer> state = this.eventlog.getValuesFor("Test");
                    this.printState("State to send: " + state.toString(), "INFO", this.details.getPrivateNodeName());
                    message = this.createMessageToSend(MessageType.Command.CRDT, 
                        MessageType.Crdt.UPDATE, state);
                } else {
                    this.eventlog.add("Test", counter, System.currentTimeMillis(), timeCreated);
                    LWWSet<Integer> state = this.eventlog.getValuesFor("Test");
                    this.printState("State to send: " + state.toString(), "INFO", this.details.getPrivateNodeName());
                    message = this.createMessageToSend(MessageType.Command.CRDT, 
                        MessageType.Crdt.UPDATE, state);
                }
            }
            
            String strToWrite = "[Message Sent by " + this.PUBLICNODENAME + "]: Broa.getTime()dcasting updates";
            this.printState(strToWrite, "INFO", this.details.getPrivateNodeName());
            //ArrayList<NodeDetails> serversToSendUpdateTo = new ArrayList<NodeDetails>();

            for (NodeDetails item: this.clusterCrdtNodes){
                if (
                    (item.getPrivateNodeName().equalsIgnoreCase(this.details.getPrivateNodeName()) == false) 
                    && (item.getPrivateNodeName().equalsIgnoreCase(this.backupServerId) == false )
                    && (item.getPrivateNodeName().equalsIgnoreCase(this.primaryServerId) == false)
                ){
                    strToWrite = "Server to send update to: " + item.getPrivateNodeName();
                    this.printState(strToWrite, "INFO", this.details.getPrivateNodeName());
                    this.unicastMessage(item.getPrivateNodeName(), message);
                }
            }

        }
    }
    
    
    // This is when the crdt node receives a message update from another crdt
    private void processUpdate(Message message, String nodeIdFrom, boolean delta){
        // the peer adds to its crdt map and sends the updates to the every other crdt node
        if (this.state == PeerConnectionState.ServerState.PEER){
            String str = "Message payload from node="+ nodeIdFrom; 
            if (delta == true){
                int counter = (int) message.getPayloadPart();
                long timeCreated = (long) message.getPayloadPart();
                
                str = str + " is message=" + String.valueOf(counter);
                this.printState(str, "INFO", this.details.getPrivateNodeName());
                
                if (message.isEmpty() == false){
                    String tagId = (String) message.getPayloadPart();
                    this.eventlog.add("Test", counter, System.currentTimeMillis(), timeCreated, tagId);
                } else {
                    this.eventlog.add("Test", counter, System.currentTimeMillis(), timeCreated);
                }

                String strToLog= "Current log (" + this.eventlog.toString() + ")";
                this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                
            } else {
                LWWSet<Integer> state = (LWWSet<Integer>) message.getPayloadPart();
                str = str + " is message=" + state.toString();
                this.printState(str, "INFO", this.details.getPrivateNodeName());
                
                // This is for the normal operation pf state crdts
                this.eventlog.addState("Test", state);
                String strToLog= "Current log (" + this.eventlog.toString() + ")";
                this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
            }
        
            message = this.createMessageToSend(MessageType.Command.CRDT, 
                MessageType.Crdt.UPDATE_REP);
            this.unicastMessage(nodeIdFrom, message);
        }
    }
    
    // This is for the primary server receiving a message from the producer
    private void processClientPost(Message message, String nodeIdFrom){
        if (this.state == PeerConnectionState.ServerState.PRIMARY){
            int counter = (int) message.getPayloadPart();
            Timestamp timeCreated = message.getTimeCreated();
            String str = "Message payload from producer=" + counter;
            this.printState(str, "INFO", this.details.getPrivateNodeName());
            
            if (message.isEmpty() == false){
                String tagId = (String) message.getPayloadPart();
                this.eventlog.add("Test", counter, System.currentTimeMillis(), timeCreated.getTime(), tagId);
                message = this.createMessageToSend(MessageType.Command.CRDT, 
                    MessageType.Crdt.SERVERPOST, counter, timeCreated.getTime(), tagId);
            } else {
                this.eventlog.add("Test", counter, System.currentTimeMillis(), timeCreated.getTime());
                message = this.createMessageToSend(MessageType.Command.CRDT, 
                    MessageType.Crdt.SERVERPOST, counter, timeCreated.getTime());
            }
            
            this.printState("State on my node: " + this.eventlog.toString(), "INFO", this.details.getPrivateNodeName());
            
            // Get the next server to try: round-robin
            str = "Current index to try: " + this.indexOfNextPeerToSendUpdateTo + " of " + this.clusterCrdtNodes.size();
            this.printState(str, "INFO", this.details.getPrivateNodeName());
            NodeDetails nodeToSendTo = this.clusterCrdtNodes.get(this.indexOfNextPeerToSendUpdateTo);
            while ( 
                (nodeToSendTo.getPrivateNodeName().equalsIgnoreCase(this.details.getPrivateNodeName()) == true) ||
                (nodeToSendTo.getPrivateNodeName().equalsIgnoreCase(this.backupServerId) == true) 
                ){
                this.indexOfNextPeerToSendUpdateTo = (this.indexOfNextPeerToSendUpdateTo + 1) % this.clusterCrdtNodes.size();
                nodeToSendTo = this.clusterCrdtNodes.get(this.indexOfNextPeerToSendUpdateTo);
            }
            
            
            str = "Server to send producer message to: " + nodeToSendTo.getPrivateNodeName();
            this.printState(str, "INFO", this.details.getPrivateNodeName());
            this.indexOfNextPeerToSendUpdateTo = (this.indexOfNextPeerToSendUpdateTo + 1) % this.clusterCrdtNodes.size();
            this.unicastMessage(nodeToSendTo.getPrivateNodeName(), message);
            

            this.printState("State on my node: " + this.eventlog.toString(), "INFO", this.details.getPrivateNodeName());
            
            // Send back to response to the producer
            message = this.createMessageToSend(MessageType.Command.CRDT, MessageType.Crdt.CLIENTPOST_REP, 
                this.details.getPrivateNodeName()); 
            this.sendMessageToExternalClient(message, nodeIdFrom);
        } 
    }
    
    
    // This is for added server, receiving the entire state from a peer node
    public void processAddServerUpdate(Message message, String nodeIdSent){
        if (this.state == PeerConnectionState.ServerState.PEER){
            if (this.hasReceivedEntireState == false){
                this.eventlog.setNewLog((HashMap<String, LWWSet<Integer>>) message.getPayloadPart());
                message = this.createMessageToSend(MessageType.Command.CRDT, 
                    MessageType.Crdt.ADDSERVERUPDATE_REP, MessageType.Command.ACK);
                this.unicastMessage(nodeIdSent, message);
                this.hasReceivedEntireState = true;
            } else {
                message = this.createMessageToSend(MessageType.Command.CRDT, 
                    MessageType.Crdt.ADDSERVERUPDATE_REP, MessageType.Command.NACK);
                this.unicastMessage(nodeIdSent, message);
            }
        }
    }
    
    // This is for added server, receiving the entire state from a peer node
    public void processAddServerUpdateRep(Message message){
        MessageType.Command ackType = (MessageType.Command) message.getPayloadPart();
        if ( ackType == MessageType.Command.ACK){
            this.printState("New Server has updated its entire state", "INFO", this.details.getPrivateNodeName());
        } else {
            this.printState("New Server has already updated its entire state", "INFO", this.details.getPrivateNodeName());
        }
    }
    
    private void broadCastMessage(Message message){
        for (NodeDetails item: this.clusterCrdtNodes){
            if(!item.getIPAddress().equalsIgnoreCase(this.details.getIPAddress())){
                this.unicastMessage(item.getPrivateNodeName(), message);
            }
        }
    }
    
    // Responds to CLIENT GET from the consumer
    private void processClientGet(Message message, String nodeIDFrom){
        if (this.state == PeerConnectionState.ServerState.PRIMARY){
            int elementToFind = (int) message.getPayloadPart();
        
            message = this.createMessageToSend(MessageType.Command.CRDT, 
                MessageType.Crdt.SERVERGET, elementToFind);
            
            this.printState("Consumer to send replies to is:" + nodeIDFrom, "INFO", this.details.getPrivateNodeName());
            this.consumerIDToSendTo = nodeIDFrom;
            
            for (NodeDetails item: this.clusterCrdtNodes){
                if ( (item.getPrivateNodeName().equalsIgnoreCase(this.primaryServerId) == false) &&
                (item.getPrivateNodeName().equalsIgnoreCase(this.backupServerId) == false) ){
                    String str = "Server to send consumer message to: " + item.getPrivateNodeName();
                    this.printState(str, "INFO", this.details.getPrivateNodeName());
                    this.unicastMessage(item.getPrivateNodeName(), message);
                }
            }  
        }
    }
    
    // Responds to the SERVERGET from the primary server
    private void processServerGet(Message message, String nodeIDFrom){
        if (this.state == PeerConnectionState.ServerState.PEER){
            int elementToFind = (int) message.getPayloadPart();
            
            LWWSet<Integer> retrievedState = this.eventlog.getValuesFor("Test");
            message = this.createMessageToSend(MessageType.Command.CRDT, 
                MessageType.Crdt.MERGESTATE, retrievedState, elementToFind);
                
            String strToWrite = "[Message Sent by " + this.PUBLICNODENAME + "]: Broadcasting gets";
            this.printState(strToWrite, "INFO", this.details.getPrivateNodeName());
            
            for (NodeDetails item: this.clusterCrdtNodes){
                if (
                    (item.getPrivateNodeName().equalsIgnoreCase(this.details.getPrivateNodeName()) == false) 
                    && (item.getPrivateNodeName().equalsIgnoreCase(this.backupServerId) == false )
                    && (item.getPrivateNodeName().equalsIgnoreCase(this.primaryServerId) == false)
                ){
                    strToWrite = "Peer Server to send get message to: " + item.getPrivateNodeName();
                    this.printState(strToWrite, "INFO", this.details.getPrivateNodeName());
                    this.unicastMessage(item.getPrivateNodeName(), message);
                }
            }
        }
    }
    
    
    private void processMergeStateRep(Message message){
        if ( this.state == PeerConnectionState.ServerState.PEER){
            this.numOfReceivedMergeStates++;
            if (numOfReceivedMergeStates == (this.clusterCrdtNodes.size() - 1) - 2){
                int elementToFind = (int) message.getPayloadPart();
                LWWSet<Integer> retrievedState = this.eventlog.getValuesFor("Test");
                message = this.createMessageToSend(MessageType.Command.CRDT,
                    MessageType.Crdt.SERVERGET_REP, retrievedState, elementToFind);
                this.printState("Sending back a mergestate_rep to the primary server", "INFO", this.details.getPrivateNodeName());
                this.unicastMessage(this.primaryServerId, message);
                this.numOfReceivedMergeStates = 0;
            }
        }
    }
    
    private void processMergeState(Message message, String nodeIdFrom){
        // process MERGESTATE
        if( this.state == PeerConnectionState.ServerState.PEER){
            LWWSet<Integer> retrievedPeerState = (LWWSet<Integer>) message.getPayloadPart();
            int elementToFind = (int) message.getPayloadPart();
            LWWSet<Integer> myState = this.eventlog.getValuesFor("Test");
            LWWSet<Integer> merged = retrievedPeerState.merge(myState);
            
            // add merged state
            this.eventlog.addWithoutMerge("Test", merged);
            
            message = this.createMessageToSend(MessageType.Command.CRDT,
                MessageType.Crdt.MERGESTATE_REP, elementToFind);
            this.unicastMessage(nodeIdFrom, message);
        }
    }
    
    private void processServerGetRep(Message message, String nodeIDFrom){
        if (this.state == PeerConnectionState.ServerState.PRIMARY){
            this.numOfReceivedMergeStates++;
            if (numOfReceivedMergeStates == (this.clusterCrdtNodes.size() - 2)){
                LWWSet<Integer> allMergedPeerState = (LWWSet<Integer>) message.getPayloadPart();
                int elementToFind = (int) message.getPayloadPart();
                this.eventlog.addState("Test", allMergedPeerState);
                if (this.eventlog.find("Test", elementToFind)){
                    LWWSetValue<Integer> valueToSend = this.eventlog.getLatest("Test", elementToFind);
                    message = this.createMessageToSend(MessageType.Command.CRDT,
                        MessageType.Crdt.CLIENTGET_REP, MessageType.Command.ACK, valueToSend);
                    message.setNodeIDFrom(this.details.getPrivateNodeName());
                    
                    this.sendMessageToExternalClient(message, this.consumerIDToSendTo);
                    this.printState("Sent message of type=[CLIENTGET_REP] to node=" + this.consumerIDToSendTo, "INFO", this.details.getPrivateNodeName()); 
                    
                    this.eventlog.remove("Test", valueToSend.getElement(), System.currentTimeMillis(), valueToSend.getProducerTimestamp());
                    GSet<LWWSetValue<Integer>> setToRemove = this.eventlog.getRemoveSet("Test");
                    
                    message = this.createMessageToSend(MessageType.Command.CRDT,
                        MessageType.Crdt.REMOVEVALUE, setToRemove);
                    
                    for (NodeDetails item: this.clusterCrdtNodes){
                        if (
                            (item.getPrivateNodeName().equalsIgnoreCase(this.details.getPrivateNodeName()) == false) 
                            && (item.getPrivateNodeName().equalsIgnoreCase(this.backupServerId) == false )
                            && (item.getPrivateNodeName().equalsIgnoreCase(this.primaryServerId) == false)
                            ){
                            String strToWrite = "Peer Server to send remove message to: " + item.getPrivateNodeName();
                            this.printState(strToWrite, "INFO", this.details.getPrivateNodeName());
                            this.unicastMessage(item.getPrivateNodeName(), message);
                        }
                    }
                    
                    // Remove my own addset
                    this.eventlog.removeElements("Test", setToRemove);
                    
                    this.numOfReceivedMergeStates = 0;
                } else {
                    message = this.createMessageToSend(MessageType.Command.CRDT,
                        MessageType.Crdt.CLIENTGET_REP, MessageType.Command.NACK);
                    message.setNodeIDFrom(this.details.getPrivateNodeName());
                    this.sendMessageToExternalClient(message, this.consumerIDToSendTo);
                    this.printState("Sent message of type=[CLIENTGET_REP] to node=" + this.consumerIDToSendTo, "INFO", this.details.getPrivateNodeName()); 
                }
            }
        }
    }
    
    private void processRemoveValue(Message message, String nodeIDFrom){
        // Send out removeElement operation
        if (this.state == PeerConnectionState.ServerState.PEER){
            GSet<LWWSetValue<Integer>> setToRemove = (GSet<LWWSetValue<Integer>>) message.getPayloadPart();
            this.eventlog.removeElements("Test", setToRemove);
            this.printState("Read repair finished !!", "INFO", this.details.getPrivateNodeName());
        }
    }

    
    // This method processes messsage from a crdt node: SETPRIMARYSERVER
    private void processSetPrimaryServer(Message mess, String nodeIdFrom){
        this.primaryServerId = nodeIdFrom;
        String str = "My primary server is : " + this.primaryServerId;
        this.printState(str, "INFO", this.details.getPrivateNodeName());
        Message message = this.createMessageToSend(MessageType.Command.CRDT, 
            MessageType.Crdt.SETPRIMARY_REP);
        if (mess.isEmpty() == false){
            this.backupServerId = "";
            str = "Removing current backup server : " + this.backupServerId;
        }
        message.setNodeIDFrom(this.details.getPrivateNodeName());
        this.unicastMessage(nodeIdFrom, message);
    }
 
    // This method processes messsage from a crdt node: SETBACKUPSERVER
    private void processSetBackupServer(Message mess, String nodeIdFrom){
        this.backupServerId = nodeIdFrom;
        String str = "My backup server is : " + this.backupServerId;
        this.printState(str, "INFO", this.details.getPrivateNodeName());
        Message message = this.createMessageToSend(MessageType.Command.CRDT, 
            MessageType.Crdt.SETBACKUP_REP);
        if (mess.isEmpty() == false){
            this.primaryServerId = "";
            str = "Removing current primary server : " + this.backupServerId;
        }
        message.setNodeIDFrom(this.details.getPrivateNodeName());
        this.unicastMessage(nodeIdFrom, message);
    }
    
    private void processGetServersFromClient(Message message, String nodeIdFrom){
        Message mess = new Message(MessageType.Command.SETSERVERS);
        String delimiter = (String) message.getPayloadPart();
    
        // the server has both items set
        if (this.primaryServerId.isEmpty() == false && this.backupServerId.isEmpty() == false){
            if (delimiter.trim().length() == 0){
                // the server is notified that the primary server has failed
                this.printState("Sending both the primary and backup server ids....", "INFO", this.details.getPrivateNodeName());
                mess.addPayloadPart(MessageType.Command.ACK);
                mess.addPayloadPart(this.primaryServerId);
                mess.addPayloadPart(this.backupServerId);
                mess.setNodeIDFrom(this.details.getPrivateNodeName());
                this.sendMessageToExternalClient(mess, nodeIdFrom);
            } else {
                this.stateMachine(message, nodeIdFrom);
            }
        } else {
            // if the server does not have both items set
            this.stateMachine(message, nodeIdFrom);            
        }
    }    
    
    // This decides the various states of our CRDTNODE
    public void stateMachine(Message mess, String clientNode){
        PeerConnectionState.ServerConnectionEvent event = (PeerConnectionState.ServerConnectionEvent) mess.getPayloadPart();
        String strToLog = "";
        Message message = new Message(MessageType.Command.CRDT);
        
        if(this.state == PeerConnectionState.ServerState.PEER){
            if (event == PeerConnectionState.ServerConnectionEvent.PEER_IS_PRIMARY){
                this.state = PeerConnectionState.ServerState.PRIMARY;
                message.setPayloadType(MessageType.Crdt.SETPRIMARY);
                
                this.primaryServerId = this.details.getPrivateNodeName();
                
                this.printState(
                    "Setting myself as the primary server",
                    "INFO",
                    this.details.getPrivateNodeName()
                );
                
                // Tell manangement server that we have seen the set primary message
                this.manServerSocket.send(
                    this.utils.convertToBytes(
                        this.createMessageToSend(MessageType.Command.SETPRIMARY_REP,
                            null, this.details.getPrivateNodeName())
                    ),0
                );
                
                this.broadCastMessage(message);
                
            } else if ( event == PeerConnectionState.ServerConnectionEvent.PEER_IS_BACKUP){
                this.state = PeerConnectionState.ServerState.BACKUP;
                message.setPayloadType(MessageType.Crdt.SETBACKUP);
                
                this.backupServerId = this.details.getPrivateNodeName();
                
                this.printState(
                    "Setting myself as the backup server",
                    "INFO",
                    this.details.getPrivateNodeName()
                );
                
                // Tell manangement server that we have seen the set backup message
                this.manServerSocket.send(
                    this.utils.convertToBytes(
                        this.createMessageToSend(MessageType.Command.SETBACKUP_REP,
                            null, this.details.getPrivateNodeName())
                    ),0
                );
                
                this.broadCastMessage(message);
            } else if (event == PeerConnectionState.ServerConnectionEvent.CLIENT_REQUEST){
                // the server does not know the primary and backup servers
                message = new Message(MessageType.Command.SETSERVERS);
                message.addPayloadPart(MessageType.Command.NACK);
                message.setNodeIDFrom(this.details.getPrivateNodeName());
                this.sendMessageToExternalClient(message, clientNode);
            }
        } else if (this.state == PeerConnectionState.ServerState.BACKUP){
            if (event == PeerConnectionState.ServerConnectionEvent.CLIENT_REQUEST){
                strToLog = "Removing original primaryServer=" + this.primaryServerId;
                this.printState(
                    strToLog,
                    "INFO",
                    this.details.getPrivateNodeName()
                );
                message.setPayloadType(MessageType.Crdt.SETPRIMARY);
                message.addPayloadPart("");
                
                for (NodeDetails item: this.clusterCrdtNodes){
                    // exclude the failed primary server
                    if(!item.getIPAddress().equalsIgnoreCase(this.details.getIPAddress()) && !item.getPrivateNodeName().equalsIgnoreCase(this.primaryServerId)){
                        this.unicastMessage(item.getPrivateNodeName(), message);
                    }
                }
                
                this.state = PeerConnectionState.ServerState.PRIMARY;
                String oldPrimaryServerId = this.primaryServerId;
                this.primaryServerId = this.details.getPrivateNodeName();
                this.backupServerId = "";
                
                this.printState("Setting me as the new primary server", "INFO", this.details.getPrivateNodeName());
                
                // tell management server to eject the failed primary server
                this.manServerSocket.send(
                    this.utils.convertToBytes(
                        this.createMessageToSend(MessageType.Command.SETPRIMARY_REP,
                            null, this.primaryServerId, oldPrimaryServerId)     
                    ),0
                );
                
                message = new Message(MessageType.Command.SETSERVERS);
                message.setNodeIDFrom(this.details.getPrivateNodeName());
                message.addPayloadPart(MessageType.Command.NACK);
                this.sendMessageToExternalClient(message, clientNode);
                
            } else {
                strToLog = event + " received by backup server. Disregarding...";
                this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
            }
        } else if (this.state == PeerConnectionState.ServerState.PRIMARY){
            // the server does not know the primary and backup servers
            if (event == PeerConnectionState.ServerConnectionEvent.CLIENT_REQUEST){
                message = new Message(MessageType.Command.SETSERVERS);
                message.addPayloadPart(MessageType.Command.NACK);
                this.sendMessageToExternalClient(message, clientNode);
            } else {
                strToLog = event + " received by primary server. Disregarding...";
                this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
            }
        }
    }
    
    
    @Override
    public void run(){
        try{
            while (!Thread.currentThread().isInterrupted()){
                Message message;
                String strMessage, strToLog;
                
                MessageType.Command type;
                
                this.poller.poll();
                
                // connection to the management server
                if (this.poller.pollin(0)){
                   message = (Message) this.utils.convertFromBytes(this.manServerSocket.recv(0));
                   type = message.getMessageType();
                   switch(type){
                        case START:
                            if (this.currentClusterState == SystemState.ClusterState.STARTED){
                                this.clusterCrdtNodes = (ArrayList<NodeDetails>) message.getPayloadPart();
                                this.setUpDealerSockets();
                                this.printPeers();
                                this.currentClusterState = SystemState.ClusterState.RUNNING;
                                this.printState(
                                    "Changing ClusterNodeState from STARTED to UP",
                                    "INFO",
                                    this.details.getPrivateNodeName()
                                );
                            } else {
                                this.printState(
                                    "This node is already started",
                                    "WARNING",
                                    this.details.getPrivateNodeName()
                                );
                            }
                            break;
                        case ADDSERVER:
                            NodeDetails dets = (NodeDetails) message.getPayloadPart();
                            this.clusterCrdtNodes.add(dets);
                            this.printPeers();
                            ZMQ.Socket newSocket = this.context.socket(ZMQ.DEALER);
                            String newSocketname = UUID.randomUUID().toString();
                            strToLog = "This is the dealer socket identity=" +
                                newSocketname + " on this node that is bound to the router socket of node=(" +
                                dets.getPrivateNodeName() + ")";
                            this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                            newSocket.setIdentity(newSocketname.getBytes());
                            newSocket.connect(dets.getIPAddress());
                            this.dealerSockets.put(dets.getPrivateNodeName(), newSocket);
                            this.printState("Added new server: " + dets.getPrivateNodeName(), "INFO", this.details.getPrivateNodeName());
                            
                            // Send state to the new server
                            this.printState("Sending entire state to new server", "INFO", this.details.getPrivateNodeName());
                            HashMap<String, LWWSet<Integer>> peerEventLog = this.eventlog.getEntireState();
                            message = this.createMessageToSend(MessageType.Command.CRDT, MessageType.Crdt.ADDSERVERUPDATE, 
                                peerEventLog);
                            this.unicastMessage(dets.getPrivateNodeName(), message);
                            break;
                        case QUIT:
                        case STOP:
                            // Quitting 
                            this.shutDownSocket.send(
                                this.utils.convertToBytes(
                                    this.createMessageToSend(MessageType.Command.HEARTBEAT, 
                                        null, PUBLICNODENAME)
                                ), 0
                            );
                            this.printState( 
                                "Sent QUIT message ON [Node Shutdown Endpoint] TO (Management Server)", 
                                "INFO", 
                                this.details.getPrivateNodeName()
                            );
                            message = (Message) this.utils.convertFromBytes(this.shutDownSocket.recv(0));
                            strToLog = "MESSAGE RECEIVED: (" + message.getMessageType() + ") ON [Node Shutdown Endpoint] FROM (Management Server)";
                            this.printState(
                                strToLog, 
                                "INFO", 
                                this.details.getPrivateNodeName()
                            );
                            if ("QUIT".equalsIgnoreCase(message.getMessageType().name())){
                                Thread.currentThread().interrupt();
                            }
                            break;
                        case EJECT:
                            String nodeIdToEject = (String) message.getPayloadPart();
                            this.processEject(nodeIdToEject);
                            strToLog = "Ejecting nodeId=" + nodeIdToEject + " from list of nodes";
                            this.printState(
                                strToLog, 
                                "INFO", 
                                this.details.getPrivateNodeName()
                            );
                            break;
                        case GETLOGS:
                            break;
                        case SETPRIMARY:
                        case SETBACKUP:
                            strMessage = "Received from " + type + " from management server";
                            this.printState(strMessage, "INFO", this.details.getPrivateNodeName());
                            this.stateMachine(message, "");
                            break;
                        default:
                            this.printState(
                                "This command is not recognised", 
                                "WARNING",
                                this.details.getPrivateNodeName()
                            );
                            break;
                    }
                } 
                
                // the socket that subscribes to the management server
                if (this.poller.pollin(1)){
                    message = (Message) this.utils.convertFromBytes(this.heartBeatSubSocket.recv(0));
                    strToLog = "MESSAGE RECEIVED: (" + (String) message.getPayloadPart() + ") ON [Subs Socket] FROM (Management server)";
                    this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                    
                    // Communicating with the management server
                    
                    this.heartBeatRepSocket.send(
                        this.utils.convertToBytes(
                             this.createMessageToSend(MessageType.Command.HEARTBEAT, 
                                null, PUBLICNODENAME)
                        ), 0
                    );
                    this.printState("MESSAGE SENT: (" + PUBLICNODENAME + ") ON [Node Rep Endpoint] TO (Management Server)", 
                            "INFO", this.details.getPrivateNodeName());
                    message = (Message) this.utils.convertFromBytes(this.heartBeatRepSocket.recv(0));
                    strMessage = (String) message.getPayloadPart();
                    strToLog = "MESSAGE RECEIVED: (" + strMessage + ") ON [Node Rep Endpoint] FROM (Management Server)";
                    this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                    
                    if ("I have seen you".equalsIgnoreCase(strMessage) && this.currentClusterState == SystemState.ClusterState.DOWN){
                        this.manServerSocket.send(
                            this.utils.convertToBytes(
                                this.createMessageToSend(MessageType.Command.HEARTBEAT,
                                    null, this.details)
                            ),0
                        );
                        strToLog = "MESSAGE SENT: (" + 
                                this.details + 
                                ") ON [Node Public Endpoint] TO (Management Server)";
                        this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                        this.currentClusterState = SystemState.ClusterState.STARTED;
                        this.printState("Changing ClusterState from DOWN to STARTED", 
                            "INFO", this.details.getPrivateNodeName());
                    }
                }
                
                
                // connecting to the other crdt nodes
                if (this.poller.pollin(2)){
                    String socketId = new String(this.crdtNodeRcvSocket.recv(0));
                    strToLog = "This is the socket id=" + socketId;
                    this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                    message = (Message) this.utils.convertFromBytes(this.crdtNodeRcvSocket.recv(0));
                    //String nodeIdSent = (String) message.getLastPayloadPart();   // the raftnodeId of the sending node
                    String nodeIdSent = message.getNodeIDFrom();
                    MessageType.Crdt crdtType = message.getPayloadType();
                    strToLog = "Received a CRDT message of type = [" + crdtType + "] from " + nodeIdSent;
                    this.printState(
                        strToLog,
                        "INFO",
                        this.details.getPrivateNodeName()
                    );
                    switch(crdtType){
                        case SERVERPOST:
                            this.processServerPost(message, true);
                            break;
                        case SETPRIMARY:
                            this.processSetPrimaryServer(message, nodeIdSent);
                            break;
                        case SETBACKUP:
                            this.processSetBackupServer(message, nodeIdSent);
                            break;
                        case UPDATE:
                            this.processUpdate(message, nodeIdSent, true);
                            break;
                        case SETPRIMARY_REP:
                        case SETBACKUP_REP:
                        case UPDATE_REP:
                        case SERVERPOST_REP:
                            strToLog = "Received [" + crdtType + "] from CrdtNode=" + nodeIdSent;
                            this.printState(strToLog, "INFO", this.details.getPrivateNodeName());
                            break;
                        case CLIENTPOST:
                            this.processClientPost(message, nodeIdSent);
                            break;
                        case GETSERVERS:
                            this.processGetServersFromClient(message, nodeIdSent);
                            break;
                        case ADDSERVERUPDATE:
                            this.processAddServerUpdate(message, nodeIdSent);
                            break;
                        case ADDSERVERUPDATE_REP:
                            this.processAddServerUpdateRep(message);
                            break;
                        case SERVERGET:
                            this.processServerGet(message, nodeIdSent);
                            break;
                        case SERVERGET_REP:
                            this.processServerGetRep(message, nodeIdSent);
                            break;
                        case CLIENTGET:
                            this.processClientGet(message, nodeIdSent);
                            break;
                        case MERGESTATE:
                            this.processMergeState(message, nodeIdSent);
                            break;
                        case MERGESTATE_REP:
                            this.processMergeStateRep(message);
                            break;
                        case REMOVEVALUE:
                            this.processRemoveValue(message, nodeIdSent);
                            break;
                        default:
                            this.printState(
                                "This command is not recognised", 
                                "WARNING",
                                this.details.getPrivateNodeName()
                            );
                            break;
                    }
                    strToLog = "Processing end of a CRDT message of type=[" + crdtType + "]";
                    this.printState(
                        strToLog,
                        "INFO",
                        this.details.getPrivateNodeName()
                    );
                }
                
            }
            
            if (Thread.currentThread().isInterrupted()){
                this.printState("Thread is interrupted", "INFO", this.details.getPrivateNodeName());
                this.tearDownSockets();
                return;
            }
            
        } catch (Exception ie){
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ie.printStackTrace(pw);
            System.out.println(sw.toString());
        }
    }
    
    public static void main(String [] args){
        startLogging();
        String docker_net_alias = System.getenv("NETALIAS");
        CrdtNode peer = new CrdtNode(docker_net_alias);
        peer.start();
    }
}