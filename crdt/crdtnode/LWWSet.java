/**
 *  Copyright 2018 Okusanya Oluwadamilola
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
 
/**
 * LWWSet class
 */ 

import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Objects;
import java.sql.Timestamp;
import java.io.Serializable;
 
public class LWWSet<T extends Comparable<T>> implements Serializable {
    
    private static final long serialVersionUID = 3294840922964631172L;
    
    private GSet<LWWSetValue<T>> addSet;
    private GSet<LWWSetValue<T>> removeSet;
    
    LWWSet(){
        this.addSet = new GSet<LWWSetValue<T>>();
        this.removeSet = new GSet<LWWSetValue<T>>();
    }
    
    LWWSet(GSet<LWWSetValue<T>> addSet, GSet<LWWSetValue<T>> removeSet){
        this.addSet = addSet;
        this.removeSet = removeSet;
    }
    
    // val is actually a set
    public LWWSetValue<T> add(T val, long timeStamp, long timeCreated){
        LWWSetValue<T> entry = new LWWSetValue<>(val, timeStamp, timeCreated);
        this.addSet.add(entry);
        return entry;
    }
    
    public LWWSetValue<T> add(T val, long timeStamp, long timeCreated, String tagId){
        LWWSetValue<T> entry = new LWWSetValue<>(val, timeStamp, timeCreated);
        entry.setGroupId(tagId);
        this.addSet.add(entry);
        return entry;
    }
    
    public LWWSetValue<T> remove(T val, long timeStamp, long timeCreated){
        LWWSetValue<T> entry = new LWWSetValue<>(val, timeStamp, timeCreated);
        this.removeSet.add(entry);
        return entry;
    }
    
    public GSet<LWWSetValue<T>> getAddSet(){
        return this.addSet;
    }
    
    public GSet<LWWSetValue<T>> getRemoveSet(){
        return this.removeSet;
    }
    
    public GSet<LWWSetValue<T>> mergeWithSelf(){
        return this.addSet.merge(this.removeSet);
    }
    
    public int getAddSize(){
        return this.addSet.getSize();
    }
    
    public LWWSet<T> merge(LWWSet<T> set){
        return new LWWSet<T>(this.addSet.merge(set.getAddSet()), this.removeSet.merge(set.getRemoveSet())); 
    }
    
    /**
    public LWWSet<T> diff(LWWSet<T> set){
        //final LWWSet<T> mergedSet = this.merge(set);
        //System.out.println("Merged set: " + mergedSet.toString());
        //return new LWWSet<T>( mergedSet.getAddSet().diff(set.getAddSet()), mergedSet.getRemoveSet().diff(set.getRemoveSet()));
        return new LWWSet<T>( this.addSet.diff(set.getAddSet()), this.removeSet.diff(set.getRemoveSet()));
    }**/
    
    private Comparator<LWWSetValue<T>> timeStampOrder(){
        return new Comparator<LWWSetValue<T>>(){
            @Override
            public int compare(LWWSetValue<T> firstVal, LWWSetValue<T> secondVal){
                return Long.compare(firstVal.getTimestampInLong(), secondVal.getTimestampInLong());
            }
        };
    }
    
    public LWWSet<T> purge(GSet<LWWSetValue<T>> setToRemove){
        System.out.println("Number of items before removal:" + String.valueOf(this.getAddSize()));
        // Remove items from the eventlog
        final HashSet<LWWSetValue<T>> elementSetToRemoveFrom = this.addSet.query();
        for (LWWSetValue<T> val: setToRemove.query()){
            Iterator<LWWSetValue<T>> setIterator = elementSetToRemoveFrom.iterator();
            while(setIterator.hasNext()){
                LWWSetValue<T> item = setIterator.next();
                T element = item.getElement();
                Timestamp timestampOfAddItem = new Timestamp(item.getTimestampInLong());
                Timestamp timestampOfRemoveItem = new Timestamp(val.getTimestampInLong());
                if ( element.compareTo(val.getElement()) == 0 && timestampOfAddItem.before(timestampOfRemoveItem)){       
                    // Comparing two Integers is guaranteed to be false without unboxing
                    setIterator.remove();
                }
            }
        }
        setToRemove.query().clear();
        System.out.println("Number of items after removal:" + String.valueOf(this.getAddSize()));
        GSet <LWWSetValue<T>> newAddSet = new GSet<>(elementSetToRemoveFrom);
        LWWSet<T> newSet = new LWWSet<T>(newAddSet, setToRemove);
        return newSet;
    }
    
    public void setRemoveSet(GSet<LWWSetValue<T>> values){
        this.removeSet = values;
    }
    
    public void setAddSet(GSet<LWWSetValue<T>> values){
        this.addSet = values;
    }
    
    public Pair<LWWSetValue<T>, LWWSetValue<T>> value(T val){
        // An element is in the set if the element is the add set with a higher timestamp than its duplicate in the removeSet
        final GSet<LWWSetValue<T>> compareAddSet = new GSet<LWWSetValue<T>>(this.addSet.query());
        final GSet<LWWSetValue<T>> compareRemoveSet = new GSet<LWWSetValue<T>>(this.removeSet.query());
        GSet<LWWSetValue<T>> filteredAddSet = new GSet<LWWSetValue<T>>();
        GSet<LWWSetValue<T>> filteredRemoveSet = new GSet<LWWSetValue<T>>();
        for ( LWWSetValue<T> item: compareAddSet.query()){
            if (item.getElement() == val){
                filteredAddSet.add(item);
            }   
        }
        for ( LWWSetValue<T> item: compareRemoveSet.query()){
            if (item.getElement() == val){
                filteredRemoveSet.add(item);
            }   
        }
        
        Comparator<LWWSetValue<T>>  setComparator = this.timeStampOrder();
        LWWSetValue<T> lastItemAddSet = null;
        LWWSetValue<T> lastItemRemoveSet = null;
            
        if (filteredAddSet.getSize() > 0){
            List<LWWSetValue<T>> addSetList = new ArrayList(filteredAddSet.query());
            Collections.sort(addSetList, setComparator);
            lastItemAddSet = addSetList.get(addSetList.size() - 1);
        }
        
        if (filteredRemoveSet.getSize() > 0){
            List<LWWSetValue<T>> removeSetList = new ArrayList(filteredRemoveSet.query());
            Collections.sort(removeSetList, setComparator);
            lastItemRemoveSet = removeSetList.get(removeSetList.size() - 1);
        }        
        
        return new Pair<LWWSetValue<T>, LWWSetValue<T>>(lastItemAddSet, lastItemRemoveSet);
    }
    
    public boolean lookup(T val){
        Pair<LWWSetValue<T>, LWWSetValue<T>> pair = this.value(val);
        
        // An element is in the set if the element is the add set with a higher timestamp than its duplicate in the removeSet
        if ( (pair.getFirst() != null && pair.getSecond() == null) || (pair.getFirst() != null && pair.getSecond() != null && pair.getFirst().getTimestampInLong() >=     pair.getSecond().getTimestampInLong())){
            return true;
        }
        return false;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LWWSet<T> that = (LWWSet<T>) o;

        return Objects.equals(this.addSet, that.addSet) && Objects.equals(this.removeSet, that.removeSet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.addSet, this.removeSet);
    }
    
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("\tAddSet: ");
        sb.append(this.addSet.toString());
        sb.append(System.lineSeparator());
        sb.append("\tRemoveSet: ");
        sb.append(this.removeSet.toString());
        sb.append(System.lineSeparator());
        return sb.toString();
    }
    
    public static void main(String [] args){
        
        /**
        LWWSet<Integer> set1 = new LWWSet<Integer>();
        LWWSet<Integer> set2 = new LWWSet<Integer>();
        
        set1.add(1, 1523936002690L);
        set1.add(2, 1523936022745L);
        set1.add(3, 1523936032783L);
        
        set2.add(1, 1523936012666L);
        set2.add(3, 1523936002620L);
        
        System.out.println("Set1: " + set1.toString());
        System.out.println("Set1: " + set2.toString());
        
        System.out.println("Diff: " + set1.diff(set2)); 
        **/
    }
}