/**
 *  Copyright 2018 Okusanya Oluwadamilola
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
 
/**
 * Crdt node class
 * It uses the binary star pattern.
 * The binary star pattern is a pattern where there are two servers, one backup, one secondary.
 * Each node has five states: 
 * - primary(the main server that is waiting for client connections)
 * - backup (the secondary server that is waiting for client connections)
 * - active ( the main server that is accepting client connections)
 * - passive (the secondary server that is not accepting client connections)
 * - peer( a server that is neither primary, backup, active, passive)
 * Conversion between the various states will be done through a FSM.
 * 
 * The description in the ZMQ source code uses PUB-SUB. This will not do for CRDTs.
 * The algorihtm for this server is this:
 * - when the management client sends a START message, the management server collects all the addresses
 *  of all the servers in the network. All the servers start up as peer.
 * - the management server then designates the backup and primary servers.
 *  - the management server sends a EVENT_PEER_BACKUP message to a server, 
 * - 
 */
 
import java.util.ArrayDeque;
import java.util.Iterator;

class CacheItems<T, V>{
    private T firstPayload;
    private V secondPayload;
    private int term;
    
    CacheItem(T firstPayload, V secondPayload, int term){
        this.firstPayload = firstPayload;
        this.secondPayload = secondPayload;
        this.term = term;
    }
    
    public T getFirstPayload(){
        return this.firstPayload;
    }
    
    public V getSecondPayload(){
        return this.secondPayload;
    }
    
    public int getTerm(){
        return this.term;
    }
    
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("FirstPayload: ");
        sb.append(this.firstPayload.toString());
        sb.append(" secondPayload: ");
        sb.append(this.secondPayload.toString());
        sb.append(" term: ");
        sb.append(this.term.toString());
        return sb.toString();
    }
} 

public class Cache{
    
    private ArrayDeque<CacheItems<String, Integer>> items;
    
    Cache(){
        this.items = new ArrayDeque<CacheItems<String,Integer>>();
    }
    
    public ArrayList<CacheItems<String, Integer>> getItems(int term){
        ArrayList<CacheItems<T, V>> returned = new ArrayList<CacheItems<String, Integer>>();
        Iterator itr = this.items.iterator();
        int countToPop = 0;
        while(itr.hasNext()){
            CacheItem<String, Integer> item = itr.next();
            if (item.getTerm() == term){
                returned.push(item);
                countToPop++;
            }
        }
        while(countToPop > 0){
            this.items.pop();
        }
    }
    
    public void append(String first, int second, int term){
        if (first != null && first.trim().length() != 0){
            items.add(new CacheItem<T, V>(first, null, term));
        } else if (second != null){
            items.add(new CacheItem<T, V>(null, second, term));
        }
    }
    
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("[ ");
        for (CacheItem<String, Integer> item: this.items){
            sb.append(item.toString());
        }
        sb.append(" ]");
        return sb.toString();
    }
}