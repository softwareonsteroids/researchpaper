/**
 *  Copyright 2017 Okusanya Oluwadamilola
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Scanner;
import org.zeromq.ZMQ;
 
/**
 *  Client object for the management substrate of the 
 *  FittChart application
 *  
 *  @author Okusanya Oluwadamilola
 *  @version 1.0
 */

public class ManagementClient{

     // Socket initialisation
    private ZMQ.Context context = ZMQ.context(1);
    private ZMQ.Socket manServerSocket = this.context.socket(ZMQ.REQ);
    
    // Reads user input 
    private Scanner scanner;
    
    // Utils object 
    private Utils utils;
    
    // Message object
    private Message message;
    
    ManagementClient(){
        this.scanner = new Scanner(System.in);
        this.manServerSocket.connect(Constants.MANAGEMENT_CLIENT_SERVER_PORT);
        this.utils = new Utils();
        this.message = null;
    }
    
    public ZMQ.Socket getSocket(){
        return this.manServerSocket;
    }
    
    public ZMQ.Context getContext(){
        return this.context;
    }
    
    public Utils getUtilsObj(){
        return this.utils;
    }
    
    public void prompt(){
        System.out.println("Menu:\n" + 
                        "START\n " +
                        "GETLOGS\n " +  
                        "GETALLIDS\n" + 
                        "GETPRIMARYLEADERID\n" +
                        "GETBACKUPSERVERID\n" +
                        "SETGROUPID\n" + 
                        "STOP\n" +
                        "QUIT\n" + 
                        "HELP\n");
    }
    
    public String mainloop(){
        System.out.println("Enter your command: ");
        String commandString = this.scanner.nextLine();
        String nodeId = "";
        int numberOfNodesToTag = 0;
        switch(commandString){
            case "START":
            case "GETALLIDS":
            case "GETBACKUPSERVERID":
            case "GETPRIMARYLEADERID":
            case "QUIT":
                this.message = new Message(MessageType.Command.valueOf(commandString));
                break;
            case "SETGROUPID":
                System.out.print("\nEnter the tag you want to attach to the group of nodes: ");
                nodeId = this.scanner.nextLine();
                System.out.print("\nEnter the number of nodes you want tagged: ");
                numberOfNodesToTag = Integer.parseInt(this.scanner.nextLine());
                this.message = new Message(MessageType.Command.valueOf(commandString));
                this.message.addPayloadPart(nodeId);
                this.message.addPayloadPart(numberOfNodesToTag);
                break;
            case "GETLOGS":
                System.out.print("\nEnter the node(NodeId) you want to query: ");
                nodeId = this.scanner.nextLine();
                System.out.print("\nEnter the number of messages you want to retrieve: ");
                int numOfMessages = Integer.parseInt(this.scanner.nextLine());
                this.message = new Message(MessageType.Command.valueOf(commandString));
                this.message.addPayloadPart(nodeId);
                this.message.addPayloadPart(numOfMessages);
                break;
            case "STOP":
                System.out.print("\nEnter the node(NodeId) you want to stop: ");
                nodeId = this.scanner.nextLine();
                this.message = new Message(MessageType.Command.valueOf(commandString));
                this.message.addPayloadPart(nodeId);
                break;
            case "HELP":
                this.help();
                break;
            default:
                System.out.println("This command is not recognized\n");
                break; 
        }
        return commandString;
    }
    
    private void help(){
        System.out.print("Enter your command character" +
                    "to see the help associated with that command: ");
        String commandString = this.scanner.nextLine();
        switch(commandString){
            case "START": 
                System.out.println("START\n"+
                    "Info: Starts the entire application.\n " +
                    "Err: This command is designed to be used once.\n");
                break;
            case "GETLOGS":
                System.out.println("GETLOGS [nodeID] [numOfMessages] \n" +
                    "Info: Get [numOfMessages] logs from a specific node [nodeID].\n" +
                    "Err: [nodeId] is wrong or the [numOfMessages] is > than the logs on the [nodeId] \n");
                break;
            case "GETALLIDS":
                System.out.println("GETIDS \n" +
                    "Info: Get all the nodeIds of all the nodes in the application\n" +
                    "Err: None\n");
                break;
            case "SETGROUPID":
                System.out.println("SETGROUID [tagId] [messageCount]\n" +
                    "Info: Set a [messageCount] of message with [tagId] to measure times processed\n" +
                    "Err: This command is designed to be used once.\n");
                break;
            case "GETBACKUPSERVERID":
                System.out.println("GETBACKUPSERVERID \n" +
                    "Info: Get the node id of the backup server node\n" +
                    "Err: If the leader is not selected\n");
                break;
            case "GETPRIMARYLEADERID":
                System.out.println("GETPRIMARYLEADERID \n" +
                    "Info: Get the leader id of the primary server node\n" +
                    "Err: If the leader is not selected\n");
                break;
            case "STOP":
                System.out.println("STOP [nodeId]\n"+
                    "Info: Shutdown a particular node.\n " +
                    "Err: [nodeId] is wrong.\n");
                break;
            case "QUIT":
                System.out.println("QUIT \n" +
                    "Info: Shutdown the application\n"+
                    "Err: None\n");
                break;
            default:
                System.out.println("This command is not recognized\n");
                break;
        }
    }
    
    public static void main(String [] args){
        ManagementClient client = new ManagementClient();
        
        Message message;
        try{
            while(!Thread.currentThread().isInterrupted()){
                try{
                    client.prompt();
                    String commandString = client.mainloop();
                    if (commandString != null && !commandString.isEmpty()){
                        client.getSocket().send(
                            client.getUtilsObj()
                                  .convertToBytes(client.message)
                                  , 0
                        );
                        if ("QUIT".equalsIgnoreCase(commandString)){
                            break;
                        }
                        System.out.println("[Input]: Just sent (" + commandString +")");
                        message = (Message) client.getUtilsObj().convertFromBytes(
                                    client.getSocket().recv(0));
                        String reply = "[Input]: Received (" + message + ")";
                        System.out.println(reply);
                    }
                    Thread.sleep(12000);
                } catch (InterruptedException ie){
                    ie.printStackTrace();
                }
            }
        } catch (Exception e){
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            System.out.println(sw.toString());
        }
        
        client.getSocket().close();
        client.getContext().term();
    }
}
